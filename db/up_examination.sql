-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 09, 2016 at 06:52 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `acme`
--

-- --------------------------------------------------------

--
-- Table structure for table `up_examination`
--

CREATE TABLE IF NOT EXISTS `up_examination` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `department_id` enum('1','2') NOT NULL DEFAULT '1',
  `attachment` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `published_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ent_date` datetime NOT NULL,
  `upd_date` datetime NOT NULL,
  `slug` varchar(255) NOT NULL,
  `status` enum('draft','live') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `up_examination`
--

INSERT INTO `up_examination` (`id`, `name`, `department_id`, `attachment`, `description`, `published_date`, `ent_date`, `upd_date`, `slug`, `status`) VALUES
(6, 'test', '1', '6.docx', 'dsafasdfdsfdsafas\r\n', '2016-05-09', '2016-05-09 00:00:00', '0000-00-00 00:00:00', 'test', 'live'),
(7, 'name2', '2', '7.pdf', 'fdsfsdf\r\n', '2016-05-09', '2016-05-09 00:00:00', '0000-00-00 00:00:00', 'name2', 'live');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
