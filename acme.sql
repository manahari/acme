-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 22, 2015 at 07:47 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `acme`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('07a3afeddd3e97c992e32a7ac3a06eb4', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441737990, 'a:6:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441772383;s:10:"permission";N;s:9:"logged_in";b:1;s:7:"user_id";s:1:"1";s:8:"group_id";s:1:"1";}'),
('082575c4aff7de15a5e89d3d4ca675c0', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441737990, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441824390;}'),
('08ed030bc20e65f75572c0f9c11f9a1e', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441737992, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441824392;}'),
('091e0348931d515f0a92205414de2187', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.4 Safari/537.36', 1440731550, 'a:7:{s:9:"user_data";s:0:"";s:7:"timeout";i:1440817329;s:9:"logged_in";b:1;s:7:"user_id";s:1:"1";s:8:"group_id";s:1:"1";s:10:"permission";N;s:10:"student_id";s:1:"1";}'),
('0c0d2d542abb5ba06379997d2f415180', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.4 Safari/537.36', 1440614916, 'a:7:{s:9:"user_data";s:0:"";s:7:"timeout";i:1440700303;s:7:"user_id";s:1:"1";s:8:"group_id";s:1:"1";s:10:"permission";N;s:9:"logged_in";b:1;s:10:"student_id";s:1:"1";}'),
('219c862e52415635ee04e27833c5603e', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441304665, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441391065;}'),
('2bf0a19b16fd8ac840cecb046e5071fa', '192.168.10.20', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2508.0 Safari/537.36', 1442575631, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1442662031;}'),
('3afbbd276477b50c510d0eef09746b48', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441304665, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441391065;}'),
('40c26f8ee46535852a5605b86400ee15', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441304665, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441391065;}'),
('48a640d1d13a4c58022e8f3da57be9ce', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441737992, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441824392;}'),
('4b664bfa688710275eb02fd59aad0950', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36', 1440597287, 'a:6:{s:9:"user_data";s:0:"";s:7:"timeout";i:1440683354;s:9:"logged_in";b:1;s:7:"user_id";s:1:"1";s:8:"group_id";s:1:"1";s:10:"permission";N;}'),
('5693e854456ce7f056a1ba99d5fbf2dc', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2503.0 Safari/537.36', 1442314955, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1442401355;}'),
('63cb816f19850157634b5bf8e13afac5', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2503.0 Safari/537.36', 1442314955, 'a:6:{s:9:"user_data";s:0:"";s:7:"timeout";i:1442391283;s:10:"permission";N;s:7:"user_id";s:1:"1";s:8:"group_id";s:1:"1";s:9:"logged_in";b:1;}'),
('641a201a8e0143b4fe06324a5488c67f', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2503.0 Safari/537.36', 1442314955, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1442401355;}'),
('64b38b8379ee4392353c4c392532f294', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2503.0 Safari/537.36', 1442397561, 'a:6:{s:9:"user_data";s:0:"";s:7:"timeout";i:1442401356;s:9:"logged_in";b:1;s:7:"user_id";s:1:"1";s:8:"group_id";s:1:"1";s:10:"permission";N;}'),
('65de49274bec387c32994bf6a732b72e', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.155 Safari/537.36', 1440084508, 'a:6:{s:9:"user_data";s:0:"";s:7:"timeout";i:1440170909;s:9:"logged_in";b:1;s:7:"user_id";s:1:"1";s:8:"group_id";s:1:"1";s:10:"permission";N;}'),
('7003a6d717a0081b9196680d7ca6f52e', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441799396, 'a:7:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441831515;s:10:"permission";N;s:9:"logged_in";b:1;s:7:"user_id";s:1:"1";s:8:"group_id";s:1:"1";s:10:"student_id";s:1:"1";}'),
('70b2da879beeaf09cdc9aca44aa49bb7', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441304665, 'a:6:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441382818;s:7:"user_id";s:1:"1";s:8:"group_id";s:1:"1";s:10:"permission";N;s:9:"logged_in";b:1;}'),
('83c4bf71294427e5500c1f526f3fcf50', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2503.0 Safari/537.36', 1442314955, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1442401355;}'),
('8560465f3e0d30c09bd847ca8ce1f0fd', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2503.0 Safari/537.36', 1442314955, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1442401356;}'),
('85db5db9226d190188bb3eea81f24f46', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36', 1440603855, 'a:6:{s:9:"user_data";s:0:"";s:7:"timeout";i:1440685994;s:9:"logged_in";b:1;s:7:"user_id";s:1:"1";s:8:"group_id";s:1:"1";s:10:"permission";N;}'),
('8997976f29d61b80061fdf4554e60e74', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441744618, 'a:7:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441824392;s:9:"logged_in";b:1;s:7:"user_id";s:1:"1";s:8:"group_id";s:1:"1";s:10:"permission";N;s:10:"student_id";s:1:"1";}'),
('932a237aa843b4883a8da826b061f140', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2503.0 Safari/537.36', 1442314955, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1442401355;}'),
('977f80ffbc9f86e17e17ea443f77e8a5', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2503.0 Safari/537.36', 1442314956, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1442401356;}'),
('a930a60866ac108728f8b8c7b740b145', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441737990, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441824390;}'),
('ac8030d4a553f6cca3334008952dceba', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2503.0 Safari/537.36', 1442314955, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1442401355;}'),
('ae143a84a9c7bb56f789f9d245306eff', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441737991, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441824391;}'),
('b4c76151a8ab56c34a2c59fb495ff7ed', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36', 1440599249, 'a:6:{s:9:"user_data";s:0:"";s:7:"timeout";i:1440684860;s:9:"logged_in";b:1;s:7:"user_id";s:1:"1";s:8:"group_id";s:1:"1";s:10:"permission";N;}'),
('bc4ac85bd6fc5fe249bff9c55bcc0aa0', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441737991, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441824391;}'),
('c9007cc170502d6a8b0707efa7a20827', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.4 Safari/537.36', 1440614916, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1440701316;}'),
('c99283a2beabdbc226781319492887ae', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441737990, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441824390;}'),
('cfeced77dc511aa72f7a7956e9348f09', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441737990, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441824390;}'),
('e4d1f28c85e75a8cbe62a249cd259180', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2503.0 Safari/537.36', 1442314956, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1442401356;}'),
('e5d3824104c72adbc40ba6c7424a5c3d', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2508.0 Safari/537.36', 1442575400, 'a:6:{s:9:"user_data";s:0:"";s:7:"timeout";i:1442653794;s:9:"logged_in";b:1;s:7:"user_id";s:1:"1";s:8:"group_id";s:1:"1";s:10:"permission";N;}'),
('ea53c79fbc63868f0a6df7b71dfafb09', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.4 Safari/537.36', 1440614916, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1440701316;}'),
('eab2518c0e22628601fd93d027c7c4b3', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.4 Safari/537.36', 1440614916, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1440701316;}'),
('ee6cd73f76b2d8a5147ce37735bf73c1', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36', 1440597432, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1440683832;}'),
('f18462f487a7cae8af6873d5bfe3da44', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2503.0 Safari/537.36', 1442314955, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1442401355;}'),
('f231e31be406f3ba6c80954f1bf9464c', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441737991, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441824391;}'),
('f5b794a04ae87c3c87041bff28c9396f', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441737990, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441824390;}'),
('f7b8fd726b43cf7d3f32842d761fb51b', '::1', 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36', 1440612901, 'a:6:{s:9:"user_data";s:0:"";s:7:"timeout";i:1440690708;s:9:"logged_in";b:1;s:7:"user_id";s:1:"1";s:8:"group_id";s:1:"1";s:10:"permission";N;}'),
('f8483e726734ef9720c4a33a5b53b9e2', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441737990, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441824390;}'),
('f945531fc4630fc23a773b441914ec50', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441737991, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441824392;}'),
('ffe8f76858e56490409abb27285202b9', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2498.0 Safari/537.36', 1441737991, 'a:2:{s:9:"user_data";s:0:"";s:7:"timeout";i:1441824391;}');

-- --------------------------------------------------------

--
-- Table structure for table `up_banner`
--

CREATE TABLE IF NOT EXISTS `up_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('draft','live') NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `up_banner`
--

INSERT INTO `up_banner` (`id`, `title`, `sub_title`, `attachment`, `ent_date`, `upd_date`, `status`) VALUES
(1, '1st', 'NA', '1.jpg', '2014-03-06', '2015-09-06', 'live'),
(2, '2nd Image', 'NA', '2.jpg', '2014-03-06', '2015-09-06', 'live'),
(3, '3rd', 'NA', '3.jpg', '2014-03-06', '2015-09-06', 'live'),
(4, '4th image', '', '4.jpg', '2015-09-06', NULL, 'live'),
(5, '5th image', '', '5.jpg', '2015-09-06', NULL, 'live'),
(6, '6th Image', '', '6.jpg', '2015-09-06', NULL, 'live'),
(7, '7th Image', '', '7.jpg', '2015-09-06', NULL, 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_contactus`
--

CREATE TABLE IF NOT EXISTS `up_contactus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE ucs2_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE ucs2_unicode_ci NOT NULL,
  `mailsubject` text COLLATE ucs2_unicode_ci NOT NULL,
  `mailbody` text COLLATE ucs2_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE ucs2_unicode_ci NOT NULL,
  `ent_date` datetime NOT NULL,
  `upd_date` datetime DEFAULT NULL,
  `status` enum('live','draft') COLLATE ucs2_unicode_ci NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=ucs2 COLLATE=ucs2_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `up_contactus`
--

INSERT INTO `up_contactus` (`id`, `name`, `email`, `mailsubject`, `mailbody`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'bibek', 'bibek.munikar@gmail.com', 'testing', 'This is testing', 'bibek', '2015-02-02 00:00:00', NULL, 'draft'),
(2, 'kjkj', 'jkj', 'kj', 'kj', 'kjkj', '2015-09-08 00:00:00', NULL, 'draft'),
(3, 'manahari sijapati', 'manahari.sijapati@gmail.com', 'test email', 'this is test email for contact us module.', 'manahari-sijapati', '2015-09-08 00:00:00', NULL, 'draft');

-- --------------------------------------------------------

--
-- Table structure for table `up_counter`
--

CREATE TABLE IF NOT EXISTS `up_counter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `counter` int(11) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `up_counter`
--

INSERT INTO `up_counter` (`id`, `counter`, `ent_date`, `upd_date`) VALUES
(1, 87, '0000-00-00', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `up_department`
--

CREATE TABLE IF NOT EXISTS `up_department` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('draft','live') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `up_department`
--

INSERT INTO `up_department` (`id`, `name`, `description`, `attachment`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'Computer Electronics and Communication', '<p><strong>Welcome</strong></p>\r\n<p>Welcome to the web site of the Department of Computer and Electronics &amp; Communication Engineering (DoCEE) at the Acme Engineering College. The Department&rsquo;s overarching vision is to lead in the creation and development of intellectual and human capital in electronics, communication and computer engineering and their applications, in order to foster the technological, economic, and social enrichment of the nation and the world.</p>\r\n<div class="entry-content">\r\n<p>The Department has earned the recognition in several areas, with undergraduate curriculum encompassing Electronics &amp; Communication Engineering and Computer Engineering. Both undergraduate (Bachelors in Engineering) programs include 8 semesters of full time study.</p>\r\n<p>Department feels proud to have highly qualified and talented teaching professionals with whom students feel comfortable in shaping their career to cope with new challenges. The Department houses excellent laboratories, classrooms, and computer facilities for teaching and research. The Department of Computer and Electronics &amp; Communication Engineering has a Robotics Research Lab. The lab aims to work on research problems and innovative projects that extend the state of the art in robotics.</p>\r\n<p>Our goals in support of this vision are to:</p>\r\n<ul>\r\n<li>be recognized as one of the very best programs of electronics and computer engineering education, research, and its transfer to the community at large;</li>\r\n<li>be recognized as a place that encourages excellence and diversity in thought and endeavor, while providing a nurturing environment for continued professional advancement of our staff and faculty;</li>\r\n<li>provide degree and continuing education programs that produce graduates who are well-prepared to enter and assume leadership roles in the profession; and</li>\r\n<li>provide research and intellectual resources to address problems facing the industry and the world, while advancing the boundaries of disciplinary and multidisciplinary research and its applications.</li>\r\n</ul>\r\n<p>It is in this context that DoCEE works to advance Acme&rsquo;s strategic mission to define the technological research institution of the 21st century. As you navigate through this site, I hope you will see how our world-class faculty and staff, gifted students, and educational and research programs enable us to achieve these goals.</p>\r\n<p><strong>Sarad Poudel<br />Head of the Department&nbsp;</strong><br />sarad.poudel@<span class="skimlinks-unlinked">acme.edu.np</span></p>\r\n</div>', '1.jpg', 'computer-electronics-and-communication', '2015-08-12', '2015-09-09', 'live'),
(2, 'Civil Engineering', '<p>This page is under construction.</p>', NULL, 'civil-engineering', '2015-08-16', '2015-09-16', 'live'),
(3, 'Architecture', '<p>The Department of Architecture at AEC has gained a reputed academic environment and inspires productive teaching and learning process. We seek for creative minds and committed students from all over the country and make them socially competent and technically sound architects which with no doubt can contribute to the nation building process. For this the Department holds an experienced faculties and necessary physical infrastructure which include effective studio classes, workshop labs and required books at library.</p>\r\n<p>Architectural education in AEC based on updated syllabus of Purbanchal University. Curriculum maintains the Design Studio through which will be project based learning in semester system. This prepares the aptitude for innovation, effective problem solver, teamwork, and communication and presentation skill among the students through the courses. Theory knowledge will be backed by practical projects and supported by allied subjects like sociology, economics, interior design and professional practice which make them competent to the current requirement of technocrats for the nation. Furthermore students get chance to practice with the practicing architects at their consulting firm and organization as an intern student and get to acquainted with the professional practice in the field and give the platform for the exposure of their creating skills. Ultimately the students get the chance to prepare and submit a thesis project which makes them explore to the design ideas with contemporary context.</p>\r\n<p>I hereby welcome and invite all the creative minds who have aptitude to endeavor sustainable habitable spaces both in urban as well rural setting. We assure you to make an architect capable enough to compete in professional level.</p>\r\n<p>Ar. Binaya R. Shrestha<br />S. Lecturer<br />H.O.D, Architecture</p>', '3.jpg', 'architecture', '2015-08-16', '2015-09-09', 'live'),
(4, 'Postgraduate Studies', '<p>The era of twenty first century is the era of innovation through research, effective and efficient management through potential managers and dissemination of information through information &amp; communication technology. To meet the need for growing demand of information and communication technology as well as managing new opportunities and threats in the global context, both at technical and management level, Acme Engineering College has offered M.Sc. in Information System Engineering and M. Sc. in Engineering Management since a decade.</p>\r\n<p>I feel immense pleasure to introduce the Leading Engineering College founded with a sole aim to produce technically skilled, competent and managerial leaders through its dedicated and qualified faculty members with dynamic and visionary management team.</p>\r\n<p><strong>Information System Engineering</strong> program address the major technical development in information and communication technology and provides a sound foundation in digital computing, software engineering, telecommunication engineering, informatics and control. Graduates of this course will be able to pursue career paths as system analysts, information architects and roles as IT manager.</p>\r\n<p><strong> Engineering Management</strong> course provides the opportunity for potential managers to develop the managerial skills on engineers/architects and produces required level of human resource for the management of engineering in the future.</p>\r\n<p>Specialization on management and technical skills possessed by engineers/architects are no longer sufficient for today&rsquo;s increasingly diverse and rapid changing environment. In order to make a better future, engineers/architects must attain a breadth of outlook to complement their existing skills and to establish their rightful place as industrial leaders, managers, communicators and researchers.</p>\r\n<p>The College has already signed MoU with foreign Universities, namely Polotsk State University, Belarus, Kurtsk State University, Russia, Melbourne Institute of Technology, Australia and Moscow State University of Economics, Statistics and Informatics (MESI).</p>\r\n<p>College has also established research unit in order to conduct research in the related fields of Management and Information-communication.</p>\r\n<p>We are looking forward to welcome you.</p>\r\n<p><strong>Parishwar Acharya</strong><br />HOD<br />Department of Postgraduate Studies</p>', '4.jpg', 'postgraduate-studies', '2015-09-15', '2015-09-18', 'live'),
(5, 'Examination', '<p>This page is under construction</p>', NULL, 'examination', '2015-09-15', NULL, 'live'),
(6, 'Science and Humanities', '<p>This page is under construction</p>', NULL, 'science-and-humanities', '2015-09-15', NULL, 'live'),
(7, 'Library', '', NULL, 'library', '2015-09-16', NULL, 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_distribution`
--

CREATE TABLE IF NOT EXISTS `up_distribution` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `distributed_obtained` enum('obtained','distributed') NOT NULL,
  `district_id` int(11) NOT NULL,
  `agency` varchar(255) NOT NULL,
  `rice` float NOT NULL,
  `sugar` float NOT NULL,
  `salt` float NOT NULL,
  `chiura` float NOT NULL,
  `noodle` float NOT NULL,
  `biscuit` float NOT NULL,
  `dry_food` float NOT NULL,
  `water` float NOT NULL,
  `other_feedable` float NOT NULL,
  `tent` float NOT NULL,
  `tripal` float NOT NULL,
  `kambal` float NOT NULL,
  `metress` float NOT NULL,
  `other_nonfeedable` float NOT NULL,
  `airway` int(11) NOT NULL,
  `roadway` int(11) NOT NULL,
  `kaifiyet` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `up_distribution`
--


-- --------------------------------------------------------

--
-- Table structure for table `up_faculty_members`
--

CREATE TABLE IF NOT EXISTS `up_faculty_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `post` varchar(255) NOT NULL,
  `post_order` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('live','draft') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `up_faculty_members`
--

INSERT INTO `up_faculty_members` (`id`, `name`, `post`, `post_order`, `department_id`, `description`, `attachment`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'Manahari Sijapati', 'Asst.Lecturer', 6, 1, '<table border="1" width="100%" cellpadding="10">\r\n<tbody>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Name </strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Manahari Sijapati</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Designation</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Asst. Lecturer</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Department</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Computer and Electronics &amp; Communication Engineering</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Qualification</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>M.Sc. in Information System Engineering (running)</p>\r\n<p>B.E in Computer Engineering, Purbanchal University</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Area of Interest</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Research and Innovation</p>\r\n<p>Software Development</p>\r\n<p>Database Administration and IT security&nbsp;</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Professional Membership</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Nepal Engineering Council</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Email</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>manahari.sijapati@gmail.com</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Contact Number</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>9841837016</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', '1.jpg', 'manahari-sijapati', '2015-08-16', '2015-09-16', 'live'),
(2, 'Sarad Paudel', 'Senior Lecturer, Department Head.', 1, 1, '<table  border="1" width="100%" cellpadding="10">\r\n<tbody>\r\n<tr>\r\n<td><strong>Name</strong>&nbsp;</td>\r\n<td>Sarad Poudel&nbsp;</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Designation</strong></td>\r\n<td>Senior Lecturer</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Department</strong></td>\r\n<td>Computer and Electronics &amp; Communication Engineering</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Qualification</strong></td>\r\n<td>\r\n<p>BE (Electronics &amp; Communication),</p>\r\n<p>MSc (Information System Engineering),</p>\r\n<p>Teachers Training (University of Bradford, UK)&nbsp;</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Area of Interest</strong></td>\r\n<td>\r\n<p>Filter Design,</p>\r\n<p>Numerical Computing,</p>\r\n<p>Communication System Engineering</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Professional Membership</strong></td>\r\n<td>Nepal Engineering Council: 509 A ''Elex &amp; Communication''</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Email</strong></td>\r\n<td>sard.poudel@acme.edu.np</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Contact Number</strong></td>\r\n<td>01-4670925, Ext: 301</td>\r\n</tr>\r\n</tbody>\r\n</table>', '2.jpg', 'sarad-paudel', '2015-08-16', '2015-09-16', 'live'),
(3, 'Roshan Karki', 'Asst. Lecturer', 40, 2, '<p>page under construction</p>', '3.jpg', 'roshan-karki', '2015-08-26', '2015-09-16', 'live'),
(4, 'Prajwal Acharya', 'Lecturer', 4, 1, '<table  border="1" width="100%" cellpadding="10">\r\n<tbody>\r\n<tr>\r\n<td><strong>Name&nbsp;</strong></td>\r\n<td>&nbsp;Prajwal Acharya</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Designation</strong></td>\r\n<td>&nbsp;Lecturer</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Department</strong></td>\r\n<td>&nbsp;Computer and Electronics &amp; Communication Engineering</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Qualification</strong></td>\r\n<td>\r\n<p class="MsoNormal">&nbsp;Master&acute;s in Intelligent Embedded System</p>\r\n<p class="MsoNormal">&nbsp;Bachelor in Electronics and Communication</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Area of Interest</strong></td>\r\n<td>&nbsp;Research and innovation</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Professional Membership</strong></td>\r\n<td>&nbsp;Nepal Engineer Council</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Email</strong></td>\r\n<td>&nbsp;acharyapra@gmail.com</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Contact Number</strong></td>\r\n<td>&nbsp;9843545370</td>\r\n</tr>\r\n</tbody>\r\n</table>', '4.jpg', 'prajwal-acharya', '2015-09-08', '2015-09-16', 'live'),
(5, 'Subarna Bijay Khadka', 'Lecturer', 2, 1, '<table  border="1" width="100%" cellpadding="10">\r\n<tbody>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Name </strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Subarna Bijay Khadka</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Designation</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Lecturer, Deputy Head (Examination)</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Department</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Computer and Electronics &amp; Communication Engineering</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Qualification</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>M. Sc. Renewable Energy Engineering, IOE</p>\r\n<p>BE Electronics and Communication, Purbanchal University</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Area of Interest</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Photovoltaics</p>\r\n<p>Enbedded System Programming</p>\r\n<p>Energy Economics</p>\r\n<p>&nbsp;</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Professional Membership</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Nepal Engineerng Council</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Email</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>subarna.khadka@acme.edu.np</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Contact Number</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>9841702573</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', '5.jpg', 'subarna-bijay-khadka', '2015-09-08', '2015-09-16', 'live'),
(6, 'Bishow Shakya', 'Lecturer', 2, 3, '<table  border="1" width="100%" cellpadding="10">\r\n<tbody>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Name </strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Bishow Shakya</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Designation</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Lecturer</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Department</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Architecture</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Qualification</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>B. Arch. 2004 (Pokhara University)</p>\r\n<p>M.A. Buddhist Studies 2014 (TU)</p>\r\n<p>M.Sc. Running (Purwanchal University)</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Area of Interest</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Traditional Architecture</p>\r\n<p>Vernacular Architecture</p>\r\n<p>Design</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Professional Membership</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Society of Nepalese Architecture, Nepal Engineers'' Association</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Email</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>bishow.shakya@acme.edu.np</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Contact Number</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>9841397500</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', '6.jpg', 'bishow-shakya', '2015-09-08', '2015-09-16', 'live'),
(7, 'Debendra Bahadur Raut', 'Lecturer', 3, 1, '<table  border="1" width="100%" cellpadding="10">\r\n<tbody>\r\n<tr>\r\n <td ><strong>Name</strong></td>\r\n<td>Debendra Bahadur Raut</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Designation</strong></td>\r\n<td>Lecturer</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Department</strong></td>\r\n<td>Computer and Electronics &amp; Communication Engineering</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Qualification</strong></td>\r\n<td>\r\n<p>M. Sc. Renewable Energy Engineering, IOE</p>\r\n<p>BE Electronics and Communication, Purbanchal University</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Area of interest</strong></td>\r\n<td>\r\n<p>Renewable Energy &amp; Energy Integration</p>\r\n<p>Signal Processing and Communication</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Professional Memnership</strong></td>\r\n<td>Nepal Engineerng Council</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Email</strong></td>\r\n<td>debendra.raut@acme.edu.np</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Contact Number</strong></td>\r\n<td>98510099008; Skype: debendra.raut</td>\r\n</tr>\r\n</tbody>\r\n</table>', '7.jpg', 'debendra-bahadur-raut', '2015-09-09', '2015-09-16', 'live'),
(8, 'Roshani Ghimire', 'Lecturer', 5, 1, '<table  border="1" width="100%" cellpadding="10">\r\n<tbody>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Name </strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Roshani Ghimire</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Designation</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Lecturer</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Department</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Computer and Electronics &amp; Communication Engineering</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Qualification</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>M.Sc. in Information System Engineering, Purbanchal University</p>\r\n<p>B.E in computer engineering, Purbanchal University</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Area of Interest</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Data mining and data warehouse</p>\r\n<p>&nbsp;</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Professional Membership</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>Nepal Engineering Council</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Email</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>roshanigd@gmail.com</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width="193">\r\n<p><strong>Contact Number</strong></p>\r\n</td>\r\n<td width="447">\r\n<p>9840070932</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', '8.jpg', 'roshani-ghimire', '2015-09-09', '2015-09-16', 'live'),
(9, 'Dileep Sah', 'Lab Assistant', 9, 1, '<table  border="1" width="100%" cellpadding="10">\r\n<tbody>\r\n<tr>\r\n<td><strong>Name</strong></td>\r\n<td>Dileep Sah</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Designation</strong></td>\r\n<td>Lab Assistant</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Department</strong></td>\r\n<td>Computer and Electronics &amp; Communication Engineering</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Qualification</strong></td>\r\n<td>\r\n<p>Diploma in computer Engineering (CTEVT</p>\r\n<p>SLC (Nepal government)</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Area of Interest</strong></td>\r\n<td>&nbsp;-</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Professional Membership</strong></td>\r\n<td>&nbsp;-</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Email</strong></td>\r\n<td>sahdileep@yahoo.com</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Contact Number</strong></td>\r\n<td>9861017123</td>\r\n</tr>\r\n</tbody>\r\n</table>', '9.jpg', 'dileep-sah', '2015-09-10', '2015-09-16', 'live'),
(10, 'Sunita Subedi', 'Senior Lecturer', 3, 3, '<table  border="1" width="100%" cellpadding="10">\r\n<tbody>\r\n<tr>\r\n<td><strong>Name</strong></td>\r\n<td>Sunita Subedi</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Designation</strong></td>\r\n<td>Senior Lecturer</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Department</strong></td>\r\n<td>Architecture</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Qualification</strong></td>\r\n<td>\r\n<p>Bachelors in Architecture</p>\r\n<p>Msc. Disaster Risk Management</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Area of Interest</strong></td>\r\n<td>\r\n<p>Urban Risk Assessment</p>\r\n<p>Climate Change</p>\r\n<p>Environment and Disaster Risk Management</p>\r\n<p>Architecture History</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Professional Membership</strong></td>\r\n<td>\r\n<p>Society of Nepalese Architecture</p>\r\n<p>Nepal Engineering Council</p>\r\n<p>Nepal Engineering Association</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Email</strong></td>\r\n<td>sunita.subedi@acme.edu.np</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Contact Number</strong></td>\r\n<td>&nbsp;</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, 'sunita-subedi', '2015-09-10', '2015-09-16', 'live'),
(11, 'Samita Shrestha', 'Lecturer', 4, 3, '<table  border="1" width="100%" cellpadding="10">\r\n<tbody>\r\n<tr>\r\n<td><strong>Name</strong></td>\r\n<td>Samita Shrestha</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Designation</strong></td>\r\n<td>Lecturer</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Department</strong></td>\r\n<td>Architecture</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Qualification</strong></td>\r\n<td>Msc in Urban Planning</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Area of Interest</strong></td>\r\n<td>Urban Planning and Building Design</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Professional Membership</strong></td>\r\n<td>\r\n<p>Society of Nepalese Architects,</p>\r\n<p>Nepal Engineering Association,</p>\r\n<p>Madhypur Thimi Municipality Engineering Society, Balkot Baudik Samaj</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Email</strong></td>\r\n<td>ar.samita2010@gmail.com</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Contact Number</strong></td>\r\n<td>9841413768</td>\r\n</tr>\r\n</tbody>\r\n</table>', '11.jpg', 'samita-shrestha', '2015-09-10', '2015-09-16', 'live'),
(12, 'Babita Thapa', 'Lecturer', 5, 3, '<table  border="1" width="100%" cellpadding="10">\r\n<tbody>\r\n<tr>\r\n<td><strong>Name</strong></td>\r\n<td>Babita Thapa</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Designation</strong></td>\r\n<td>Lecturer</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Department</strong></td>\r\n<td>Architecture</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Qualification</strong></td>\r\n<td>\r\n<p>Bachelor in Architecture</p>\r\n<p>Master in Urban Design and Conservation</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Area of Interest</strong></td>\r\n<td>work in own professional field and Teaching</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Professional Membership</strong></td>\r\n<td>NEA(Nepal Engineering Association)</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Email</strong></td>\r\n<td>babittathapa@gmail.com</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Contact Number</strong></td>\r\n<td>4283976</td>\r\n</tr>\r\n</tbody>\r\n</table>', '12.jpg', 'babita-thapa', '2015-09-10', '2015-09-16', 'live'),
(13, 'Sudeep Shrestha', 'Lecturer', 6, 3, '<table  border="1" width="100%" cellpadding="10">\r\n<tbody>\r\n<tr>\r\n<td><strong>Name</strong></td>\r\n<td>Sudeep Shrestha</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Designation</strong></td>\r\n<td>Lecturer</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Department</strong></td>\r\n<td>Architecture</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Qualification</strong></td>\r\n<td>M. Sc. in Construction Management, thesis running</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Area of Interest</strong></td>\r\n<td>\r\n<p>Architecture, design,</p>\r\n<p>construction</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Professional Membership</strong></td>\r\n<td>\r\n<p>Membership Nepal Engineers'' Association</p>\r\n<p>Society of Nepalese Architects</p>\r\n<p>Madhapur-thimi Engineering Society</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Email</strong></td>\r\n<td>ar.sudeepshrestha@gmail.com</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Contact Number</strong></td>\r\n<td>9841585378</td>\r\n</tr>\r\n</tbody>\r\n</table>', '13.jpg', 'sudeep-shrestha', '2015-09-10', '2015-09-16', 'live'),
(14, 'Situ Manahdhar', 'Lecturer', 10, 3, '<p>This page is under construction.</p>', '14.jpg', 'situ-manahdhar', '2015-09-17', NULL, 'live'),
(15, 'Roma Amatya', 'Lecturer', 20, 3, '<p>This page is under construction.</p>', '15.jpg', 'roma-amatya', '2015-09-17', NULL, 'live'),
(16, 'Prahlad Budhathoki', 'Lab Assistant', 8, 1, '<table  border="1" cellpadding="10" width="100%">\r\n<tbody>\r\n<tr>\r\n<td><strong>Name</strong></td>\r\n<td>Prahlad Budhathoki</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Designation</strong></td>\r\n<td>Lab Assistant</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Department</strong></td>\r\n<td>Computer Electronics and Communication</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Qualification</strong></td>\r\n<td>+2 (HSEB)</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Area of Interest</strong></td>\r\n<td>Hardware and Networking</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Professional Membership</strong></td>\r\n<td>-</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Email</strong></td>\r\n<td>prahlad_budhathoki@yahoo.com</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Contact Number</strong></td>\r\n<td>9841685905</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, 'prahlad-budhathoki', '2015-09-17', '2015-09-17', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_flash_news`
--

CREATE TABLE IF NOT EXISTS `up_flash_news` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `published_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ent_date` datetime NOT NULL,
  `upd_date` datetime NOT NULL,
  `status` enum('draft','live') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `up_flash_news`
--

INSERT INTO `up_flash_news` (`id`, `name`, `attachment`, `description`, `published_date`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'Test flash_news', '1.jpg', '<p >\r\nAcme Engineering College, established in 2000 A.D., aims to provide quality education and has grown to become leading centre for engineering education in Nepal within a short span of its history.\r\n</p>\r\n <p >\r\nThe College has many distinguished scholors on its faculty honored by their peers for important contribution and excellent performance to the field they study. Contact with these hard-working educators offers students the best possible entry point to the world of today where ideas and technology mesh.\r\n</p>\r\n <p >\r\nThe College is situated at very easy location and is very much a part of the locality Sitapaila.\r\n</p>\r\n', '2015-08-10', '2015-08-10 00:00:00', '2015-08-26 00:00:00', 'draft'),
(2, 'Flash News 2', '2.gif', '<span >Acme Engineering College, established in 2000 A.D., aims to provide quality education and has grown to become leading centre for engineering education in Nepal within a short span of its history.</span>\r\n', '2015-08-10', '2015-08-10 00:00:00', '2015-08-26 00:00:00', 'draft'),
(3, 'Admission Open', '3.jpg', '', '2015-09-02', '2015-09-02 00:00:00', '2015-09-09 00:00:00', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_forum`
--

CREATE TABLE IF NOT EXISTS `up_forum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `published_date` date NOT NULL,
  `ent_date` int(11) NOT NULL,
  `upd_date` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('live','draft') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `up_forum`
--

INSERT INTO `up_forum` (`id`, `title`, `description`, `published_date`, `ent_date`, `upd_date`, `slug`, `status`) VALUES
(9, 'Acme sports festival', 'This is the testing done for Acme Engineering College forum.\r\n', '2015-01-19', 1421020800, 1421020800, 'acme-sports-festival', 'live'),
(10, 'Acme sports festival 2', 'This is second forum for the Acme Engineering College.\r\n', '2015-01-04', 1421020800, NULL, 'acme-sports-festival-2', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_forum_comment`
--

CREATE TABLE IF NOT EXISTS `up_forum_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `forum_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ent_date` int(11) NOT NULL,
  `upd_date` int(11) DEFAULT NULL,
  `status` enum('live','draft') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'live',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=31 ;

--
-- Dumping data for table `up_forum_comment`
--

INSERT INTO `up_forum_comment` (`id`, `forum_id`, `name`, `comment`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(24, 9, 'Bibek Munikar', 'This is good.', 'bibek-munikar', 1421020800, 1421020800, 'live'),
(25, 10, 'Bibek ', 'This is second testing.', 'bibek', 1422835200, 1422835200, 'live'),
(26, 10, 'Jay', 'This is testing done by jay. This is testing done by jay. This is testing done by jay. This is testing done by jay. This is testing done by jay. This is testing done by jay. This is testing done by jay. This is testing done by jay. This is testing done by jay. This is testing done by jay. This is testing done by jay.', 'jay', 1422835200, 1422835200, 'live'),
(27, 10, 'Test', 'hello', 'test', 1434837600, 1434837600, 'live'),
(28, 10, 'Test3', 'this is illegal comment.', 'test3', 1435615200, NULL, 'draft'),
(29, 10, 'ffsfs', 'ccb', 'ffsfs', 1440547200, NULL, 'draft'),
(30, 10, 'Test student', 'xvxvxvxvxvxvcxvxvcxvcxvxv', 'test-student', 1440547200, NULL, 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_message`
--

CREATE TABLE IF NOT EXISTS `up_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from_user` int(11) NOT NULL,
  `to_user` int(11) NOT NULL,
  `published_date` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `ent_date` int(11) NOT NULL,
  `upd_date` int(11) DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `up_message`
--

INSERT INTO `up_message` (`id`, `title`, `from_user`, `to_user`, `published_date`, `message`, `ent_date`, `upd_date`, `slug`, `status`) VALUES
(1, 'sachit', 2, 1, '1990-01-01', 'hey sachit whats up', 1413676800, NULL, 'sachit', '1');

-- --------------------------------------------------------

--
-- Table structure for table `up_messages`
--

CREATE TABLE IF NOT EXISTS `up_messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `program_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  `assignedby_id` int(11) NOT NULL,
  `assignedto_id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('draft','live') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `up_messages`
--

INSERT INTO `up_messages` (`id`, `program_id`, `project_id`, `activity_id`, `assignment_id`, `assignedby_id`, `assignedto_id`, `message`, `ent_date`, `upd_date`, `status`) VALUES
(2, 1, 1, 1, 2, 1, 1, 'hey boy', '2014-10-21', '2014-10-21', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_message_comment`
--

CREATE TABLE IF NOT EXISTS `up_message_comment` (
  `msg_id` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reply_user_id` int(11) NOT NULL,
  `comment` text COLLATE utf32_unicode_ci NOT NULL,
  `published_date` varchar(255) COLLATE utf32_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf32_unicode_ci NOT NULL,
  `status` enum('0','1') COLLATE utf32_unicode_ci NOT NULL DEFAULT '0',
  `ent_date` int(11) NOT NULL,
  `upd_date` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `up_message_comment`
--

INSERT INTO `up_message_comment` (`msg_id`, `id`, `reply_user_id`, `comment`, `published_date`, `slug`, `status`, `ent_date`, `upd_date`) VALUES
(1, 1, 2, 'I am fine harry and wbtu', '', '', '0', 1413676800, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `up_ministry`
--

CREATE TABLE IF NOT EXISTS `up_ministry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('draft','live') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `up_ministry`
--


-- --------------------------------------------------------

--
-- Table structure for table `up_modules`
--

CREATE TABLE IF NOT EXISTS `up_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `up_modules`
--

INSERT INTO `up_modules` (`id`, `title`, `slug`, `ent_date`, `upd_date`) VALUES
(1, 'Banner', 'banner', '2014-02-28', NULL),
(2, 'Modules', 'modules', '2014-02-28', NULL),
(3, 'Navigation', 'navigation', '2014-02-28', NULL),
(4, 'Pages', 'pages', '2014-02-28', NULL),
(5, 'Groups', 'groups', '2014-02-28', NULL),
(6, 'Permissions', 'permissions', '2014-02-28', NULL),
(7, 'Settings', 'settings', '2014-02-28', NULL),
(8, 'Users', 'users', '2014-04-04', NULL),
(9, 'Student', 'student', '2014-10-16', NULL),
(10, 'message', 'message', '2014-10-19', NULL),
(11, 'message_comment', 'message_comment', '2014-10-19', NULL),
(12, 'messages', 'messages', '2014-10-21', NULL),
(13, 'forum', 'forum', '2015-01-12', '2015-01-12'),
(14, 'contactus', 'contactus', '2015-01-13', '2015-01-14'),
(15, 'news', 'news', '2015-01-29', NULL),
(16, 'photos', 'photos', '2015-02-03', NULL),
(17, 'ministry', 'ministry', '2015-04-30', NULL),
(18, 'testimonial', 'testimonial', '2015-06-22', NULL),
(19, 'forum_comment', 'forum_comment', '2015-06-30', NULL),
(20, 'notice', 'notice', '2015-06-30', NULL),
(21, 'flash_news', 'flash_news', '2015-08-10', NULL),
(22, 'department', 'department', '2015-08-12', NULL),
(23, 'faculty_members', 'faculty_members', '2015-08-16', NULL),
(24, 'routine', 'routine', '2015-08-26', NULL),
(25, 'notes', 'notes', '2015-08-28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `up_navigation`
--

CREATE TABLE IF NOT EXISTS `up_navigation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `navtype` varchar(255) NOT NULL,
  `module_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `site_uri` varchar(255) DEFAULT NULL,
  `link_url` text,
  `position` varchar(255) DEFAULT NULL,
  `status` enum('draft','live') NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=86 ;

--
-- Dumping data for table `up_navigation`
--

INSERT INTO `up_navigation` (`id`, `group_id`, `title`, `slug`, `parent_id`, `navtype`, `module_id`, `page_id`, `site_uri`, `link_url`, `position`, `status`) VALUES
(1, 1, 'Home', 'home', 0, 'Page', 0, 1, 'NULL', 'NULL', '1', 'live'),
(4, 2, 'Home', 'home', 0, 'Page', 0, 1, '', 'NULL', '1', 'live'),
(5, 2, 'About Us', 'about-us', 0, 'Page', 0, 2, '', 'NULL', '11', 'live'),
(6, 2, 'Contact Us', 'contact-us', 0, 'Page', 0, 3, '', 'NULL', '5', 'live'),
(39, 1, 'About Us', 'about-us', 0, 'Page', 0, 2, '', 'NULL', '2', 'draft'),
(41, 1, 'Department', 'department', 0, 'Module', 22, 0, '', 'NULL', '9', 'live'),
(76, 1, 'Forum', 'forum', 0, 'Module', 13, 0, '', 'NULL', '25', 'live'),
(80, 1, 'Contact Us', 'contactus', 0, 'Module', 14, 0, '', 'NULL', '27', 'live'),
(82, 1, 'News and Event', 'news', 0, 'Module', 15, 0, '', 'NULL', '28', 'live'),
(84, 1, 'Routine', 'routine', 0, 'Module', 24, 0, '', 'NULL', '3', 'live'),
(85, 1, 'Entrance', 'entrance', 0, 'Page', 0, 39, '', 'NULL', '29', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_navigation_group`
--

CREATE TABLE IF NOT EXISTS `up_navigation_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `up_navigation_group`
--

INSERT INTO `up_navigation_group` (`id`, `title`, `slug`) VALUES
(1, 'Header', 'header'),
(2, 'Footer', 'footer'),
(3, 'Sidebar', 'sidebar'),
(5, 'test', 'test');

-- --------------------------------------------------------

--
-- Table structure for table `up_news`
--

CREATE TABLE IF NOT EXISTS `up_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `published_date` int(11) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ent_date` int(11) NOT NULL,
  `upd_date` int(11) DEFAULT NULL,
  `status` enum('draft','live') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `up_news`
--

INSERT INTO `up_news` (`id`, `title`, `published_date`, `description`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(4, 'Final Design of Ayurveda Health Home', 2015, 'This is testing for&nbsp;Final Design of Ayurveda Health Home.\r\n', 'final-design-of-ayurveda-health-home', 2015, 2015, 'live'),
(5, 'First Terminal Exam', 2015, 'This is to notify that the routene of first terminal exam has published..\r\n', 'first-terminal-exam', 2015, NULL, 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_notes`
--

CREATE TABLE IF NOT EXISTS `up_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `semester` varchar(255) NOT NULL,
  `faculty` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('live','draft') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `up_notes`
--

INSERT INTO `up_notes` (`id`, `semester`, `faculty`, `subject`, `attachment`, `description`, `ent_date`, `upd_date`, `status`) VALUES
(2, '1st Semester', 'Computer', 'Advance computer architecture', '2.pdf', 'asdfd\r\n', '2015-08-28', NULL, 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_notice`
--

CREATE TABLE IF NOT EXISTS `up_notice` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) NOT NULL,
  `department_id` int(11) NOT NULL,
  `attachment` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `published_date` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ent_date` datetime NOT NULL,
  `upd_date` datetime NOT NULL,
  `status` enum('draft','live') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `up_notice`
--

INSERT INTO `up_notice` (`id`, `name`, `slug`, `department_id`, `attachment`, `description`, `published_date`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'Test Notice', 'test-notice', 1, '1.docx', 'This is test document\r\n', '2015-08-10', '2015-08-10 00:00:00', '2015-09-16 00:00:00', 'live'),
(2, 'Examination Test notice', 'examination-test-notice', 5, '2.pdf', 'this is test notice from examination department\r\n', '2015-09-16', '2015-09-16 00:00:00', '0000-00-00 00:00:00', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_pages`
--

CREATE TABLE IF NOT EXISTS `up_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('draft','live') NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `up_pages`
--

INSERT INTO `up_pages` (`id`, `title`, `slug`, `description`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'Home', 'home', '', '2014-02-28', NULL, 'live'),
(2, 'About Us', 'about-us', '<p>\r\nWe are acmeains.\r\n</p>\r\n<p>\r\n Proud to be a member of Acme family.\r\n</p>\r\n', '2014-03-03', '2015-06-27', 'live'),
(4, 'Civil Engineering', 'civil-engineering', 'This page is for <strong><em>civil engineering</em></strong>.\r\n', '2014-10-16', '2015-06-23', 'live'),
(5, 'Architecture', 'architecture', '<div>\r\nThe Department of Architecture at AEC has gained a reputed academic environment and inspires productive teaching and learning process. We seek for creative minds and committed students from all over the country and make them socially competent and technically sound architects which with no doubt can contribute to the nation building process. For this the Department holds an experienced faculties and necessary physical infrastructure which include effective studio classes, workshop labs and required books at library.\r\n</div>\r\n<div>\r\nArchitectural education in AEC based on updated syllabus of Purbanchal University. Curriculum maintains the Design Studio through which will be project based learning in semester system. This prepares the aptitude for innovation, effective problem solver, teamwork, and communication and presentation skill among the students through the courses. Theory knowledge will be backed by practical projects and supported by allied subjects like sociology, economics, interior design and professional practice which make them competent to the current requirement of technocrats for the nation. Furthermore students get chance to practice with the practicing architects at their consulting firm and organization as an intern student and get to acquainted with the professional practice in the field and give the platform for the exposure of their creating skills. Ultimately the students get the chance to prepare and submit a thesis project which makes them explore to the design ideas with contemporary context.\r\n</div>\r\n<div>\r\nI hereby welcome and invite all the creative minds who have aptitude to endeavor sustainable habitable spaces both in urban as well rural setting. We assure you to make an architect capable enough to compete in professional level.\r\n</div>\r\n<div>\r\n<br />\r\n</div>\r\n<div>\r\nAr. Binaya R. Shrestha\r\n</div>\r\n<div>\r\nS. Lecturer\r\n</div>\r\n<div>\r\nH.O.D, Architecture\r\n</div>\r\n<div>\r\n<br />\r\n</div>\r\n', '2014-12-10', '2015-09-08', 'live'),
(16, 'forum', 'forum', '', '2014-12-23', '2014-12-29', 'live'),
(38, 'Computer Electronics and Communication', 'computer-electronics-and-communication', '<p>\r\n&nbsp;This page is for computer electronics and communication department.\r\n</p>\r\n', '2015-06-23', NULL, 'live'),
(39, 'Entrance', 'entrance', '<p class="MsoNormal">\r\n<strong><span><font size="4">Entry\r\nRequirement</font></span></strong>\r\n</p>\r\n<p class="MsoNormal">\r\n<span><font size="2">Prospective\r\nstudents with 10+2 or P.C.L (Physical or biological group with at least 100\r\nmarks Math) who have secured 45% in the aggregate or Diploma in Engineering or Architecture\r\nor an equivalent courses from Higher Secondary Board or any Board or University\r\nrecognized by Purbanchal University are eligible to apply. However, all the\r\nprospective students are required to pass entrance examinations and the aptitude\r\ntest conducted by the University. </font></span>\r\n</p>\r\n<p class="MsoNormal">\r\n<strong><span><font size="4">Financial\r\nAssistance/Scholarships</font></span></strong>\r\n</p>\r\n<p class="MsoNormal">\r\n<span><font size="2">Out\r\nof total admitted students in each faculty, top 5% students get intelligent\r\nscholarship on the basis of entrance examination and 5% students get classified\r\nscholarship.</font></span>\r\n</p>\r\n<p class="MsoNormal">\r\n<strong><span><font size="4">Fee\r\nand Other charges </font></span></strong>\r\n</p>\r\n<p class="MsoNormal">\r\n<span><font size="2">The\r\nproposed fees and other charges are as follows:</font></span>\r\n</p>\r\n<p class="MsoNormal">\r\n<strong><font color="#ff0000">B E. (CIVIL)</font>&nbsp;</strong><span>&nbsp;</span>\r\n</p>\r\n<table border="1" cellpadding="0" width="431" class="MsoNormalTable">\r\n <tbody>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <strong><span>Particulars</span></strong>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <strong><span>Amount</span></strong><span>&nbsp;</span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>Admission\r\n   Fee&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>1&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>36,000&nbsp;</span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>Deposit\r\n   (Refundable)&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>10000&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>&nbsp;</span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>Semester\r\n   Fee (Ann. Fee)&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>80000\r\n   x 8&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>6,40,000&nbsp;</span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>Monthly\r\n   Fee&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>4000\r\n   x 48&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>1,92,000&nbsp;</span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <strong><span>Total\r\n   (4 Years)&nbsp;</span>\r\n   </strong>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <strong><span>-&nbsp;</span>\r\n   </strong>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span><strong>8,68,000/-&nbsp;</strong></span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n<p class="MsoNormal">\r\n<span>&nbsp;</span>\r\n</p>\r\n<p class="MsoNormal">\r\n<strong><span><font color="#ff0000">B E. (Computer and Electronics)&nbsp;</font></span></strong>\r\n</p>\r\n<table border="1" cellpadding="0" width="422" class="MsoNormalTable">\r\n <tbody>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <strong><span>Particulars</span></strong>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <strong><span>Amount</span></strong><span>&nbsp;</span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>Admission\r\n   Fee&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>1&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>36,000&nbsp;</span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>Deposit\r\n   (Refundable)&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>10000&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>&nbsp;</span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>Semester\r\n   Fee (Ann. Fee)&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>59000\r\n   x 8&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>4,72,000&nbsp;</span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>Monthly\r\n   Fee&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>4000\r\n   x 48&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>1,92,000&nbsp;</span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <strong><span>Total\r\n   (4 Years)&nbsp;</span>\r\n   </strong>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <strong><span>-&nbsp;</span>\r\n   </strong>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span><strong>7,00,000/-&nbsp;</strong></span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n<p class="MsoNormal">\r\n<span>&nbsp;</span>\r\n</p>\r\n<p class="MsoNormal">\r\n<strong><font color="#ff0000">B  Architecture</font>&nbsp;</strong>\r\n</p>\r\n<table border="1" cellpadding="0" width="430" class="MsoNormalTable">\r\n <tbody>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <strong><span>Particulars</span></strong>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <strong><span>Amount</span></strong><span>&nbsp;</span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>Admission\r\n   Fee&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>1&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>36,000&nbsp;</span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>Deposit\r\n   (Refundable)&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>10000&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>&nbsp;</span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>Semester\r\n   Fee (Ann. Fee)&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>72000\r\n   x 10&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>7,20,000&nbsp;</span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>Monthly\r\n   Fee&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>4000\r\n   x 60&nbsp;</span>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span>2,40,000&nbsp;</span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n  <tr>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <strong><span>Total\r\n   (4 Years)&nbsp;</span>\r\n   </strong>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <strong><span>-&nbsp;</span>\r\n   </strong>\r\n   </p>\r\n   </td>\r\n   <td>\r\n   <p class="MsoNormal">\r\n   <span><strong>9,96,000/-&nbsp;</strong></span>\r\n   </p>\r\n   </td>\r\n  </tr>\r\n </tbody>\r\n</table>\r\n<p class="MsoNormal">\r\n&nbsp;&nbsp;<strong>Note</strong>: Discount available depending upon +2/equivalent\r\ncourse&rsquo;s score.\r\n</p>\r\n<p class="MsoNormal">\r\n&nbsp; Contact college reception for Admission form and other details.\r\n</p>\r\n', '2015-09-06', '2015-09-15', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_partners_banner`
--

CREATE TABLE IF NOT EXISTS `up_partners_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('draft','live') NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `up_partners_banner`
--

INSERT INTO `up_partners_banner` (`id`, `title`, `sub_title`, `attachment`, `ent_date`, `upd_date`, `status`) VALUES
(20, 'Ayurveda 1', '', '20.jpg', '2015-02-02', '2015-02-02', 'live'),
(21, 'Ayurveda 2', '', '21.jpg', '2015-02-02', NULL, 'live'),
(22, 'Ayurveda 3', '', '22.jpg', '2015-02-02', NULL, 'live'),
(23, 'Ayurveda 4', '', '23.jpg', '2015-02-02', NULL, 'live'),
(24, 'Ayurveda 5', '', '24.jpg', '2015-02-02', NULL, 'live'),
(25, 'Ayurveda 6', '', '25.jpg', '2015-02-02', NULL, 'live'),
(26, 'Ayurveda 7', '', '26.jpg', '2015-02-02', NULL, 'live'),
(27, 'Ayurveda 8', '', '27.jpg', '2015-02-02', NULL, 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_permissions`
--

CREATE TABLE IF NOT EXISTS `up_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `roles` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `up_permissions`
--

INSERT INTO `up_permissions` (`id`, `group_id`, `roles`) VALUES
(1, 2, 'a:7:{i:1;s:9:"1v1e1d1a1";i:3;s:3:"3e3";i:4;s:7:"4v4d4a4";i:9;s:7:"9v9d9a9";i:10;s:14:"10v10e10d10a10";i:11;s:14:"11v11e11d11a11";i:12;s:14:"12v12e12d12a12";}');

-- --------------------------------------------------------

--
-- Table structure for table `up_photos`
--

CREATE TABLE IF NOT EXISTS `up_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category_id` int(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('draft','live') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

--
-- Dumping data for table `up_photos`
--

INSERT INTO `up_photos` (`id`, `name`, `category_id`, `attachment`, `description`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(27, '1', 17, '27.jpg', '1\r\n', '1', '2015-02-03', NULL, 'live'),
(28, '2', 17, '28.jpg', 'test 2\r\n', '2', '2015-02-03', NULL, 'live'),
(29, '3', 17, '29.jpg', 'test 3\r\n', '3', '2015-02-03', NULL, 'live'),
(30, '4', 17, '30.jpg', 'test 4\r\n', '4', '2015-02-03', NULL, 'live'),
(31, '2.1', 18, '31.jpg', 'test 5&nbsp;\r\n', '21', '2015-02-03', NULL, 'live'),
(32, '3.1', 19, '32.jpg', 'test 7\r\n', '31', '2015-02-03', NULL, 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_photoscategory`
--

CREATE TABLE IF NOT EXISTS `up_photoscategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('draft','live') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `up_photoscategory`
--

INSERT INTO `up_photoscategory` (`id`, `name`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(17, 'Ayurveda 1', 'ayurveda-1', '2015-02-03', NULL, 'live'),
(18, 'Ayurveda 2', 'ayurveda-2', '2015-02-03', NULL, 'live'),
(19, 'Ayurveda 3', 'ayurveda-3', '2015-02-03', NULL, 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_routine`
--

CREATE TABLE IF NOT EXISTS `up_routine` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department_id` varchar(255) NOT NULL,
  `semester_id` varchar(255) NOT NULL,
  `attachment1` varchar(255) NOT NULL,
  `attachment2` varchar(255) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('live','draft') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `up_routine`
--

INSERT INTO `up_routine` (`id`, `department_id`, `semester_id`, `attachment1`, `attachment2`, `ent_date`, `upd_date`, `status`) VALUES
(5, 'Computer', '2nd Semester', '5attachment1.jpg', '5attachment2.xlsx', '2015-08-26', '2015-09-15', 'live'),
(6, 'Electronics', '6th Semester', '6attachment1.jpg', '6attachment2.xlsx', '2015-08-26', '2015-09-17', 'live'),
(8, 'Civil', '5th Semester', '8attachment11.jpg', '8attachment2.xlsx', '2015-08-26', '2015-09-17', 'live'),
(9, 'Architecture', '5th Semester', '9attachment11.jpg', '9attachment2', '2015-08-26', '2015-09-17', 'live'),
(11, 'Computer', '5th Semester', '11attachment12.jpg', '11attachment2.xlsx', '2015-08-26', '2015-09-15', 'live'),
(12, 'Architecture', '6th Semester', '12attachment11.jpg', '12attachment2.pdf', '2015-08-27', '2015-09-17', 'live'),
(13, 'Civil', '2nd Semester', '13attachment1.jpg', '13attachment2.xlsx', '2015-09-17', NULL, 'live'),
(14, 'Civil', '6th Semester', '14attachment1.jpg', '14attachment2', '2015-09-17', '2015-09-17', 'live'),
(15, 'Computer', '6th Semester', '15attachment1.jpg', '15attachment2.xls', '2015-09-17', NULL, 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_settings`
--

CREATE TABLE IF NOT EXISTS `up_settings` (
  `id` int(11) NOT NULL,
  `site_name` varchar(255) NOT NULL,
  `meta_topic` varchar(255) NOT NULL,
  `meta_data` varchar(255) NOT NULL,
  `contact_email` varchar(255) NOT NULL,
  `date_format` varchar(255) NOT NULL,
  `frontend_enabled` enum('open','closed') NOT NULL DEFAULT 'open',
  `unavailable_message` text NOT NULL,
  `favicon` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `up_settings`
--

INSERT INTO `up_settings` (`id`, `site_name`, `meta_topic`, `meta_data`, `contact_email`, `date_format`, `frontend_enabled`, `unavailable_message`, `favicon`, `logo`) VALUES
(1, 'Acme Engineering College', 'Acme Engineering College', 'Engineering College in Nepal.', 'acme@acme.edu.np', 'F j, Y', 'open', 'Sorry, this website is currently unavailable. Please try again later. Thank You!', 'favicon.png', 'logo.png');

-- --------------------------------------------------------

--
-- Table structure for table `up_socialmedia`
--

CREATE TABLE IF NOT EXISTS `up_socialmedia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `social_link` varchar(255) DEFAULT NULL,
  `attachment` varchar(255) DEFAULT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('draft','live') NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `up_socialmedia`
--

INSERT INTO `up_socialmedia` (`id`, `title`, `social_link`, `attachment`, `ent_date`, `upd_date`, `status`) VALUES
(6, 'Twitter', '', '6.png', '2015-01-12', NULL, 'live'),
(7, 'Skype', '', '7.png', '2015-01-12', NULL, 'live'),
(8, 'Facebook', '', '8.png', '2015-01-12', NULL, 'live'),
(9, 'Linkedin', '', '9.png', '2015-01-12', NULL, 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_student`
--

CREATE TABLE IF NOT EXISTS `up_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `ent_date` int(11) NOT NULL,
  `upd_date` int(11) DEFAULT NULL,
  `status` enum('draft','live') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `up_student`
--

INSERT INTO `up_student` (`id`, `name`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'Bibek Munikar', 'bibek-munikar', 2014, 2014, 'draft'),
(4, 'Ashish Bhandari', 'ashish-bhandari', 2014, 2014, 'draft'),
(6, 'Pak Slovak and Business Forum', 'pak-slovak-and-business-forum', 2014, NULL, 'draft'),
(7, 'Harry Sijapati', 'harry-sijapati', 2014, NULL, 'draft');

-- --------------------------------------------------------

--
-- Table structure for table `up_testimonial`
--

CREATE TABLE IF NOT EXISTS `up_testimonial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `posted_by` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attachment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('live','draft') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `up_testimonial`
--

INSERT INTO `up_testimonial` (`id`, `title`, `posted_by`, `attachment`, `description`, `category`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'Manisha Lama', 'Manisha Lama, BE Computer. Batch: 2009', '1.jpg', '<div>\r\nAcme was the best choice that I made in my life. Since the first moment I came to Acme  Engineering College ,I met nothing but energy and enthusiasm. I got a very friendly environment overhere, the faculties and staffs never made me feel awkward and low. In my years here I had the greatest time ever and made some of the best friends a person could have. All of the teachers were amazing. They never made me to get stucked on my books only rather encouraged me to be familiar with the ongoing trends in the IT market. My teachers helped me beyond words I can say. "A diamond just need a rub to shine", Acme has so many oppurtunities for the students who believe in hardwork and talent. Acme is not just a college to me but a family, a family that will behold me for the rest of my life.\r\n</div>\r\n', 'Student', 'manisha-lama', '2015-06-30', '2015-09-16', 'live'),
(2, 'Prabin Bhandari', 'Prabin Bhandari, BE Electronics and Communication. Batch: 2009', '2.jpg', '<div>\r\nIt was life changing experience at ACME, great infrastructure, marvellous team of teacher support us at length througout our studies. They provided the best platform and guidence  to achieve best height and success in future. The time that I spent in the Robotics group is the haunting memories of my life. At acme, teachers became the friends, friends became the soulmate and the college became the best place of my life. Finally, there are something money cannot buy, that I got at ACME.\r\n</div>\r\n', 'Student', 'prabin-bhandari', '2015-06-30', '2015-09-16', 'live'),
(3, 'Sanjay Sah', 'Sanjay Sah, BE Electronics and Communication. Batch: 2011', '3.jpg', 'Among Nepal''s best colleges, Acme is the one. It has highly experienced, dedicated & professional faculties. They inspire us to become creative & innovative to face any kind of challenges. Acme helped us to achieve our academic goals and develop qualities like versatility, leadership and ethical values as well.\r\n', 'Student', 'sanjay-sah', '2015-07-02', '2015-09-16', 'live'),
(4, 'Subash Gautam', 'Subash Gautam, BE Electronics and Communication, Batch: 2009', '4.jpg', 'My experience at Acme Engineering College has been outstanding. My teachers and the friends I have made at the college have been so supportive, approachable and like-minded that made the past four years some of the most enjoyable of my life. I chose to study Electronics and Communication Engineering and at first, I was interested in communication field, but later years I was fascinated by robotics and found myself at Acme Robotics Team, participating in national and international robotics competitions. Great infrastructure with well-equipped laboratories, cooperative academic staffs, exceptional learning environments, spacious classrooms, abundant library resources are the inherent features that allured me to be a part of this college.\r\n', 'Student', 'subash-gautam', '2015-09-16', '2015-09-16', 'live'),
(5, 'Pratikshya Baral', 'Pratikshya Baral, BE Computer. Batch: 2012', '5.jpg', '<div>\r\nAcme engineering college, is one of the best colleges of Purbanchal University. Acme, like its name suggests gives the best of academics, extracurricular activities and overall development of students. Experienced faculty members, well equipped labs and outstanding management are the major strengths of the college.\r\n</div>\r\n', 'Student', 'pratikshya-baral', '2015-09-16', '2015-09-16', 'live');

-- --------------------------------------------------------

--
-- Table structure for table `up_users`
--

CREATE TABLE IF NOT EXISTS `up_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `display_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `group_id` int(11) DEFAULT NULL,
  `activation_code` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `last_login` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Registered User Information' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `up_users`
--

INSERT INTO `up_users` (`id`, `email`, `display_name`, `username`, `password`, `group_id`, `activation_code`, `created_on`, `last_login`) VALUES
(1, 'manahari.sijapati@gmail.com', 'Manahari Sijapati', 'admin', 'c2bc653ba87b56385b77c0fd8992545e09bf5b1985ae2d6e32be8de8c79bdd2a73dd16538f4a905aca9a5a61eb3eb70d3f62fc3a55284198d9de813273797e6b', 1, NULL, 1390966800, 1390966800),
(2, 'drpunkrocker@yahoo.com', 'Harry', 'user', '86b0c3e5f0fcfa7939794dadab22dbf1b5721458d2d370e2326be11b0b23612de4b45d2e175115e832477b501ca62c9c716b1a3433d8d40351914883b6abf8ad', 2, NULL, 1390966800, 1390966800),
(3, 'ayurveda@gmail.com', 'Ayurveda', 'ayurveda', '244c2d002eb221262c0693a76bb8a28c6729178047bd3d0368b59b615136a8e19636df82b9c00da8778bba0123c05db175a99b5d95649cd9922edd6c8d06be91', 1, NULL, 2015, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `up_users_groups`
--

CREATE TABLE IF NOT EXISTS `up_users_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `up_users_groups`
--

INSERT INTO `up_users_groups` (`id`, `name`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `up_users_profiles`
--

CREATE TABLE IF NOT EXISTS `up_users_profiles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `first_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `dob` int(11) DEFAULT NULL,
  `gender` enum('m','f') COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_on` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `up_users_profiles`
--

INSERT INTO `up_users_profiles` (`id`, `created`, `updated`, `created_by`, `user_id`, `first_name`, `last_name`, `bio`, `dob`, `gender`, `phone`, `mobile`, `address`, `updated_on`) VALUES
(1, NULL, NULL, NULL, 1, 'sachit', 'karki', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `up_users_student`
--

CREATE TABLE IF NOT EXISTS `up_users_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('live','draft') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `up_users_student`
--

INSERT INTO `up_users_student` (`id`, `username`, `password`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'student', 'eeb1132d1cbc3f74cad654db4862d20fdc7bf9e65e985eee8339b90ac0f43e696940f7dad236b55db954fdf9d33620bfab01b1d269bcad5e9b51672ed8a6dd9f', '2015-06-26', NULL, 'live'),
(2, 'student2', 'e36103206f8b2777a1dc2f822b3d37c80b79aa40130208f204c3562ed2ee1a64cbf9fdc7fa9ff553f5c8c5b6ccd26b730b94b98e91e8de308c2c4f42df1c7de0', '2015-06-26', NULL, 'live');
