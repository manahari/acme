<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Template extends MX_Controller
{

	function __construct() {
        if (!$this->session->userdata('timeout') || $this->session->userdata('timeout') < time()) {
            $this->session->set_userdata('timeout', time() + 86400);
            mysql_query("UPDATE up_counter SET counter = counter + 1 ");
        }

	parent::__construct();
	}

	function front($data){
        $data['student_id'] = $this->session->userdata("student_id");
        $data['teacher_id'] = $this->session->userdata("student_id");
        $data['front_group']=$this->session->userdata("student_group_id");
		$data['site_settings'] = $this->get_site_settings();
		$data['banner'] = $this->get_banner();
        //var_dump($data['banner']); die;
		$data['headernav'] = $this->get_header('1');	//this is id number of header, and since header can't be edited or deleted, we are using this id number to be precise
                
		$data['footernav'] = $this->get_footer('2');	//similar as above but 2 is for footer nav

                
//                $data['partners_banner'] = $this->get_partners_banner();
                $data['socialmedia'] = $this->get_socialmedia();
                $data['dept_notice_list'] = $this->get_notice_list();
                $data['exam_notice_list'] = $this->get_exam_notice_list();
//        var_dump($data['exam_notice_list']);die;

        $notice_list=array_merge($data['dept_notice_list'],$data['exam_notice_list']);
        foreach ($notice_list as $key => $part) {
            $ent_date[$key] = $part->ent_date;
        }
        array_multisort( $ent_date, SORT_DESC, $notice_list);
        $data['notice_list']=$notice_list;
                $data['notes_list'] = $this->get_notes_list();
		$data['news'] = $this->get_news_list();
                $data['testimonial']=$this->get_testimonial('id');
        $data['counter']=$this->get_counter();
        $data['flash_news'] = $this->get_flash_news();
		//$data['sidebar'] = $this->get_navigation_from_navigation_name('sidebar'); 
		// ^ this line is to use navigation group for the future use by passing the parameter you can see in development code column of navigation group
		$this->load->view('front', $data);
	}
    function front_table($data){
        $data['student_id'] = $this->session->userdata("student_id");

        $data['site_settings'] = $this->get_site_settings();
        $data['banner'] = $this->get_banner();
        //var_dump($data['banner']); die;
        $data['headernav'] = $this->get_header('1');	//this is id number of header, and since header can't be edited or deleted, we are using this id number to be precise

        $data['footernav'] = $this->get_footer('2');	//similar as above but 2 is for footer nav


//                $data['partners_banner'] = $this->get_partners_banner();
        $data['socialmedia'] = $this->get_socialmedia();
        $data['notice_list'] = $this->get_notice_list();
        $data['notes_list'] = $this->get_notes_list();
        $data['news'] = $this->get_news_list();
        $data['testimonial']=$this->get_testimonial('id');
        $data['counter']=$this->get_counter();
        $data['flash_news'] = $this->get_flash_news();
        //$data['sidebar'] = $this->get_navigation_from_navigation_name('sidebar');
        // ^ this line is to use navigation group for the future use by passing the parameter you can see in development code column of navigation group
        $this->load->view('table', $data);
    }
	
        function get_introduction($slug){
            $this->load->model('pages/mdl_pages');
            $introduction = $this->mdl_pages->get_introduction($slug);
            return $introduction;
        }
        function get_message_from_director($slug){
            $this->load->model('pages/mdl_pages');
            $introduction = $this->mdl_pages->get_message_from_director($slug);
            return $introduction;
        }
        function get_message_from_founder($slug){
            $this->load->model('pages/mdl_pages');
            $introduction = $this->mdl_pages->get_message_from_founder($slug);
            return $introduction;
        }

        
        function get_socialmedia(){
		$this->load->model('socialmedia/mdl_socialmedia');
		$query = $this->mdl_socialmedia->get_socialmedia();
		$result = $query->result();
		return $result;	
	}
        function get_partners_banner(){
	$this->load->model('partners_banner/mdl_partners_banner');
	$query = $this->mdl_partners_banner->get_partners_banner();
	return $query->result_array();
	}
	function get_site_settings(){	
            $this->load->model('settings/mdl_settings');
            $query = $this->mdl_settings->get_settings();
            $result = $query->result_array();
            return $result[0];
	}	
	
	function get_navigation_from_navigation_name($navigation_name){		
		$this->load->model('navigation/mdl_navigation');
		$query = $this->mdl_navigation->get_navigation_from_navigation_name($navigation_name);
		$result = $this->add_href($query->result_array());
		return $result;	
	}
	
	function errorpage(){
		$this->load->view('404');
	}
	
	function userlogin($data){
		$this->load->view('userlogin', $data);
	}
    function studentlogin($data){
//        $this->load->module('users_student');
        $this->load->view('users_student/loginform', $data);
    }
	
	function get_banner(){
	$this->load->model('banner/mdl_banner');
	$query = $this->mdl_banner->get_banner();
	return $query->result_array();
	}
	
	function get_parent($group_id){
	$this->load->model('navigation/mdl_navigation');
	$query = $this->mdl_navigation->get_parentnav_for_frontend($group_id);	
	$result = $this->add_href($query);
	return $result;
	}
	
	function get_child($group_id,$parent_id){
	$this->load->model('navigation/mdl_navigation');
	$query = $this->mdl_navigation->get_childnav_for_frontend($group_id,$parent_id);	
	$result = $this->add_href($query);
	return $result;
	}
	
	function get_header($group_id){
		$data['parentnav'] = $this->get_parent($group_id);	//
		if($data['parentnav'] == NULL){return NULL;}
		$i=0;
		foreach($data['parentnav'] as $nav){
			$children =  $this->get_child($group_id,$nav['id']);
			if($children != NULL){ 
				$nav['children'] = $children;
                               
                                /*for grand child*/
//                                if($nav['children'] == NULL){return NULL;}
//                                $j=0;
//                                
//                                foreach($nav['children'] as $sub_child){
//                                    $grand_children =  $this->get_child($group_id,$sub_child['id']);
//                                    
//                                    if($grand_children != NULL)
//                                        { 
//                                            $nav['children']['grand_children'] = $grand_children;
//                                            //var_dump($nav['children']['grand_children']); die();
//                                        }
//                                        $navigation[$i][$j] = $nav;
//                                        $j++;	
//                            //var_dump($nav['children']); die();
//			}
                        /*grand child end*/
			}
                         
		$navigation[$i] = $nav;
		$i++;	
		}
		return $navigation;
	}
	
	/*function get_footer($group_id){
	$this->load->model('mdl_template');
	$query = $this->mdl_template->get_footernav($group_id);
	return $query->result_array();
	}*/
	
	function get_footer($group_id){
	$this->load->model('mdl_template');
	$query = $this->mdl_template->get_footernav($group_id);	
	$result = $this->add_href($query->result_array());
	return $result;
	}
	
	
	
	
	function add_href($result){
		$count = count($result);
		for($i=0;$i<$count;$i++){			
				if($result[$i]['navtype'] == 'Module'){$result[$i]['href'] = $this->get_name_from_module($result[$i]['module_id']);$result[$i]['target'] = "_self";}
				
				elseif($result[$i]['navtype'] == 'Page'){$result[$i]['href'] = $this->get_name_from_page($result[$i]['page_id']);$result[$i]['target'] = "_self";}
				
				elseif($result[$i]['navtype'] == 'URI'){$result[$i]['href'] = $result[$i]['site_uri'];$result[$i]['target'] = "_self";}
				
				else{$result[$i]['href'] = $result[$i]['link_url'];$result[$i]['target'] = "_blank";}
                                
		}
		return $result;
	}
	
	
	
	function get_name_from_module($module_id){
	$this->load->model('mdl_template');
	$query = $this->mdl_template->get_name_from_module($module_id);	
       // var_dump($query);die;
	return $query['0']['slug'];	
	}
	
	function get_name_from_page($page_id){		
	$this->load->model('mdl_template');
	$query = $this->mdl_template->get_name_from_page($page_id);
        if(isset ($query['0']))
        {
            return $query['0']['slug'];	 
        }
       
	}
        function get_notice_list(){
            $this->load->model('notice/mdl_notice');
            $query = $this->mdl_notice->get_notice();
            return $query->result();
        }
    function get_exam_notice_list(){
        $this->load->model('examination/mdl_examination');
        $query = $this->mdl_examination->get_exam_notice();
        return $query->result();
    }
        function get_notes_list(){
            $this->load->model('notes/mdl_notes');
            $query = $this->mdl_notes->get_notes();
            return $query;
        }
        function get_news_list(){
            $this->load->model('news/mdl_news');
            $query = $this->mdl_news->get_news();
            return $query;
        }
        function get_flash_news(){
            $this->load->model('flash_news/mdl_flash_news');
            $query = $this->mdl_flash_news->get_flash_news();
            return $query;
        }
    function get_counter(){
        $query=$this->db->get('up_counter');
        return $query;
    }
        function get_testimonial($id){
            $this->load->model('testimonial/mdl_testimonial');
            $query = $this->mdl_testimonial->get_front($id);
            return $query;
        }    
}