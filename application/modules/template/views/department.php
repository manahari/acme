<?php $base_url=  base_url();?> 
<div class="row">
    <div class="col-md-12">
	<div class="col-md-12">
        <div class="widget box"> 
            <div class="widget-header"> 
            	
            </div> 
            <div class="widget-content">
                <div class="tabpanel" role="tabpanel">
                      <div class="col-md-12">
                                                   
							<ul class="nav nav-tabs" role="tablist">
                                                             <?php
                                                            $active='active';
                                                            $i=1;
                                                            foreach ($department as $row)
                                                            {
                                                                
                                                            ?>
								<li role="presentation" class="show_again <?php echo $active;?>"><a href="#tab<?php echo $i;?>" aria-controls="tab<?php echo $i;?>" role="tab" data-toggle="tab"><?php echo $row->name;?></a></li>
							
							<?php
                                                        $active='';
                                                        $i++;
                                                            }
                                                        ?>
                                                        </ul>
                                                        
							<div class="tab-content">
                                                            <?php
                                                            $active='active';
                                                            $i=1;
                                                            foreach ($department as $row)
                                                            {
                                                                
                                                            ?>
                                                            
                                                            <div role="tabpanel" class="tab-pane <?php echo $active;?>" id="tab<?php echo $i;?>">
                                                                <div class="col-md-12">
                                                                    <div class="col-md-9">
                                                                        <div class="col-md-12 faculty_description" >
                                                                            <h2><?php echo $row->name;?></h2>
                                                                            <?php
                                                                            if(!isset($row->attachment))
                                                                            {
                                                                            ?>
                                                                                <p><img src='<?php  echo $base_url;?>uploads/department/College_logo.png' style="width:200px; height:300; padding: 20px" class="img-responsive " id="page_image" align="right">
                                                                            <?php }
                                                                            else{
                                                                            ?>
                                                                            <p><img src='<?php  echo $base_url;?>uploads/department/<?php echo $row->attachment;?>' style="width:200px; height:300; padding: 20px" class="img-responsive " id="page_image" align="right">

                                                                            <?php
                                                                            }
                                                                            ?>
                                                                                <?php echo $row->description;?></p>
                                                                        </div>
                                                                        <div class="col-md-12">
                                                                            <div class="col-md-8">

                                                                                <div class="page_description description_div" id="description_div" style="align-self: center">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">

                                                                                <div class="page_description image_div" id="image_div" style="display:none ;" >

                                                                                    <center><img style="width:200px; height:300;" class="img-responsive page_image" id="page_image"></center>
                                                                                </div>
                                                                                <div style="text-align: center;"><div class="name name_div" id="name_div">
                                                                                </div></div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                    <aside class="col-md-3">
                                                                        <div class="panel dark-style">
                                                                            <!--                                                                        <div class="panel light-style">
                                                                                                                                                        <h3>Mission Statement</h3>
                                                                                                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo, porro, molestiae! Dolorum illum cum assumenda perferendis, iusto sapiente et! Suscipit aliquid harum culpa odio, doloribus. Nulla asperiores voluptatem incidunt cumque?</p>
                                                                                                                                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quisquam magnam nisi, rerum inventore culpa. Aut maiores expedita debitis, soluta voluptate inventore quis cupiditate nemo veritatis ratione ipsam tempora, vero.</p>

                                                                                                                                                    </div>-->

                                                                            <h3>Faculty Members</h3>
                                                                            <ul class="side-list">
                                                                                <?php
                                                                                $this->load->module('department');
                                                                                $department_slug=$row->slug;
                                                                                $department_list=$this->department->get_department_id($department_slug);
                                                                                foreach ($department_list as $row)
                                                                                {
                                                                                    $department_id=$row->id;
                                                                                }
                                                                                $faculty_member=$this->department->get_faculty_members($department_id);
                                                                                foreach ($faculty_member->result() as $row) {?>
                                                                                    <li ><a href="javascript:" onclick="ajaxFacultyMember(this)" target="_self" id="<?php echo $row->slug;?>"><?php echo $row->name;?></a></li>
                                                                                <?php }
                                                                                ?>

                                                                            </ul>

                                                                        </div>
                                                                        <div class="panel dark-style">

                                                                            <h3>Department Notice</h3>
                                                                            <ul class="side-list">
                                                                                <?php
                                                                                $this->load->module('department');
                                                                                //$department_list=$this->department->get_department_id($department_slug);
                                                                                //                                                                            foreach ($department_list as $row)
                                                                                //                                                                            {
                                                                                //                                                                                $department_id=$row->id;
                                                                                //                                                                            }
                                                                                $department_notice=$this->department->get_department_notice($department_id);
                                                                                $count=1;
                                                                                foreach ($department_notice->result() as $row) {
                                                                                    if($count<=2){
                                                                                    ?>

                                                                                    <li ><a href="<?php echo base_url();?>uploads/notice/<?php echo $row->attachment; ?>" target="_blank" id="<?php echo $row->slug;?>"><?php echo $row->name;?></a></li>
                                                                                <?php }
                                                                                    $count++;
                                                                                }
                                                                                ?>
                                                                            </ul>
                                                                            <ul class="side-list department" style="display: none;">
                                                                                <?php
                                                                                $count=1;
                                                                                foreach ($department_notice->result() as $row) {
                                                                                    if($count>2){
                                                                                    ?>
                                                                                    <li ><a href="<?php echo base_url();?>uploads/notice/<?php echo $row->attachment; ?>" target="_blank" id="<?php echo $row->slug;?>"><?php echo $row->name;?></a></li>
                                                                                <?php
                                                                                    }
                                                                                    $count++;
                                                                                }
                                                                                ?>
                                                                            </ul>
                                                                            <button id="hide" class="hide_more" style="display: none;">Show Less</button>
                                                                            <button id="show" class="show_more">Show More</button>
                                                                        </div>
                                                                    </aside>

                                                                    </div>
                                                                </div>
									<?php
                                    $active='';
                                    $i++;
                                    }
                                    ?>
								</div>
                                                          
                      </div>
                    
        
                                                        
    
						</div>
            	
    </div>
</div>
                <script>
                    $('.show_again').click(function()
                    {
                       $('.faculty_description').show(); 
                       $('.name_div').hide();
                       $('.image_div').hide();
                       $('.description_div').hide();
                       
                    });
                function ajaxFacultyMember(e) {
                    var csrf_test_name = $("input[name=csrf_test_name]").val();
                    var slug = e.id;
                    var base_url='<?php echo $base_url;?>'.concat('department/get_faculty_members_details');
                    
                    $.ajax({
                        type:'POST',
                        url:base_url,
                        data: { 'csrf_test_name' : csrf_test_name,'slug' : slug,'test':'test'  },
                        success:function(data) 
                        {  
                           $.each($.parseJSON(data), function(k, v) {
                               $('.faculty_description').hide();
                               $('.name_div').html(v.name+' ('+v.post+')').show();
                               $('.image_div').show();
                           $('.image_div').attr("style", '');
                               var attachment = (v.attachment===null)?'College_logo.png': v.attachment;
                         $('.page_image').attr("src", '<?php echo $base_url;?>' +"uploads/faculty_members/" + attachment);
                          $('.description_div').html(v.description).show();
                          $("html, body").animate({ scrollTop: 0 }, "slow");
                  return false;
                });
                        }
                    });
                }
                </script>
        <script>
            $(document).ready(function(){
                $(".hide_more").click(function(){
                    $(".show_more").show();
                    $(".hide_more").hide();
                    $(".department").hide();
                });
                $(".show_more").click(function(){
                    $(".show_more").hide();
                    $(".hide_more").show();
                    $(".department").show();
                });
            });
        </script>