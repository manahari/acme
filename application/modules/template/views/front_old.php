<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $site_settings['site_name'];?></title>
<meta name="title" content="<?php echo $site_settings['site_name'];?>">
<meta name="keywords" content="<?php echo $site_settings['meta_topic'];?>">
<meta name="description" content="<?php echo $site_settings['meta_data'];?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="<?php echo base_url();?>uploads/settings/<?php echo $site_settings['favicon'];?>">

    <!-- Scripts -->
<!--    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>-->
    
<!--<link media="all" rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/main.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/normalize.css">



<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/responsive-slider.css">

<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/smoothDivScroll.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/flags32.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/jquery.feedBackBox.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/bootstrap.min.css">

<link rel="stylesheet" href="<?php echo base_url();?>design/admin/css/fontawesome/font-awesome.min.css">
<link href="<?php echo base_url();?>design/admin/css/icons.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/all.css">

<script src="<?php echo base_url();?>design/frontend/js/vendor/modernizr-2.6.2.min.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/jquery.feedBackBox.js"></script>-->
<link rel="shortcut icon" href="<?php echo base_url();?>uploads/settings/favicon.ico">

<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/normalize.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/main.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/responsive-slider.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/all.css">
<script src="<?php echo base_url();?>design/frontend/js/jquery.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/vendor/modernizr-2.6.2.min.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/bootstrap.min.js"></script>

<!-- For Responsive Photo Gallery-->
<script src="<?php echo base_url();?>design/frontend/js/gallery/jquery.blueimp-gallery.min.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/gallery/bootstrap-image-gallery.min.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/gallery/demo.js"></script>
<script src="https://maps.googleapis.com/maps/api/js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/gallery/blueimp-gallery.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/gallery/bootstrap-image-gallery.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/gallery/demo.css">




</head>
<body >


  
        
        <div class="col-lg-12" >
	<div id="wrapper" >
            
		<header id="header">
			
				<div class="header-holder row">
					<div class="col-sm-6">
                                            <div class="col-sm-1"></div>
						<div class="logo">
                                                    <a href="#"><img src="<?php echo base_url();?>design/frontend/img/images/logo.png" alt="ACME"></a>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="header-frame">
							<ul class="add-nav">
								<li><a href="#">Feedback</a></li>
								<li><a href="#">Webmail</a></li>
								<li><a href="#">FAQ</a></li>
								<li><a href="#">Contact</a></li>
								<li><a href="#">Quick Link</a></li>
							</ul>
							<ul class="social-networks">
								<li>
                                                                    
                                                                    <a href="#" class="icon-facebook"></a></li>
								<li><a href="#" class="icon-twitter"></a></li>
								<li><a href="#" class="icon-phone"></a></li>
							</ul>
						</div>
						<div class="header-block">
							<!-- <ul class="language-list">
								<li class="active"><a href="#">English</a></li>
								<li><a href="#">नेपाली</a></li>
							</ul> -->
							<div class="block_title slogan">
								<i class="pull-right">
									<fieldset>
                                                                            <div class="col-sm-12">
                                                                                <div class="col-sm-11">
							An Academic Centre of Excellence.
                                                                                </div>
                                                                                <div class="col-sm-1"></div>    
                                                                            </div>
									</fieldset>

								</i>
							</div>
                            <div class="clearfix"></div>
                            <div class="block_title">

                                <i>
                                    <fieldset class="pull-right">

                                        <?php if($student_id==1)
                                        {?>
                                            <div class="pull-right">
                                                <div class="col-md-12">
                                                    <div class="col-md-8">
                                                        <i class="slogan">Welcome!! Student</i> 
                                                    </div>
                                                    <div class="col-md-3">
                                            <i class="btn-default"> <a href="<?php echo base_url();?>users_student/logout">&nbsp;Logout&nbsp;</a></i>
                                            </div>
                                                    <div class="col-md-1"></div>
                                                </div>
                                            </div>
                                       <?php }
                                        else{ ?>
                                         <?php // load the form helper so you can use set_value, form_open etc
        $this->load->helper('form');
        echo form_open('users_student/submit');
        ?>
                                            <div class="col-sm-12">
                                                <div class="col-sm-2"></div>
                                                <div class="col-sm-3">
                                                    <i class="icon-user"></i>
                                                    <?php echo form_input('username', '', 'class="cust_form_control" placeholder="Username" autofocus data-rule-required="true" data-msg-required="Please enter your username."'); ?>
                                                </div>

                                                <div class="col-sm-3">
                                                    <i class="icon-lock"></i>
                                                    <?php echo form_password('pword', '', 'class="cust_form_control" placeholder="Password" data-rule-required="true" data-msg-required="Please enter your password."'); ?>
                                                </div>

                                                <div class="col-sm-3">
                                                        <div class="form-actions">
            <?php
            $style = 'id = "btn" style = "width:75px; height:22px; font-size:13px; margin-left:15%;" ';
            echo form_submit('submit', 'Login', $style);
            ?>
                                                        </div>
                                                </div>
                                                <div class="col-sm-1"></div>
                                            </div> 
        <?php echo form_close();?>
                                                
                                     <?php   }
                                        ?>
                                    </fieldset>

                                </i>
                            </div>
						</div>
					</div>
				</div>
		
    	<!--navigation bar------------------------------------------------------------>
        
        <div class="navbar navbar-default" role="navigation">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                </div>
                

                
                <div class="navbar-collapse collapse" id="navbar-collapse" style="height: 1px;">
                  <ul class="nav navbar-nav nav-justified ">
                  
                    <?php
                    foreach($headernav as $header){//still need to work on slug?>
                    <?php 
                    $segment=$this->uri->segment(1);
                    if($this->uri->segment(1)=='')
                    {
                        $segment='home';
                        $header['slug']=='home';
                    }
                    if(empty($header['children'])){if($segment==$header['slug']) {echo  '<li class="active">';} else{echo  '<li>';}/*this is for no-child*/} else{
                        //var_dump($header['children']);die;
                        $count=0;
                        if($this->uri->segment(1)!='')
                        {
                        foreach($header['children'] as $row) {
                            //var_dump($row);die;
                            if (in_array($this->uri->segment(1), $row)) {
                                $count++;
                            }
                        }
                        }
                        if($count>0)
                        {?>
                            <li class="active dropdown">
                        <?php }
                        else
                        { ?>
                            <li class="dropdown">
                        <?php }
                            }/*this is for dropdown*/?>
                    
							<?php if(!empty($header['children'])){/*this is for dropdown*/?> 
                            	<?php if($header['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                                <a href="<?php echo $prefix.$header['href']?>" target="<?php echo $header['target']?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $header['title']?>
                                    <b class="caret"></b></a>
                            <?php }/*end of if*/
                            else { /*this is for no-child*/?>                            
                            	<?php if($header['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                                <a href="<?php echo $prefix.$header['href']?>" target="<?php echo $header['target']?>"><?php echo $header['title']?>
                            <?php } ?>                           
                                </a>
                                    
									<?php if(!empty($header['children'])){/*this is for dropdown*/?> 
                                    <ul class="dropdown-menu">
                                        <?php foreach($header['children'] as $child){ ?>  
                                            <li>                                            
                            					<?php if($child['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                                                <a href="<?php echo $prefix.$child['href']; ?>" target="<?php echo $child['target']?>"><?php echo $child['title']; ?></a>
                                            </li>
                                        <?php }/*end of child foreach*/ ?>
                                    </ul>
                                    <?php } ?>  
                        
                    </li>
                    <?php }/*end of parent foreach*/ ?>
                    
                  </ul>
                </div><!--/.nav-collapse -->
		</header>
		<main role="main">
			                        <?php 
					$first_bit = $this->uri->segment(1);
					if($first_bit=="" || $first_bit == "home"){?>
						<div class="col-lg-10">
                                <div id="map_canvas"></div>
                         </div>
                                

<body onLoad="initialize()">
	                    <?php
if(isset($flash_news))
{
    foreach ($flash_news->result() as $row)
    {
        
                    ?>
	<div class="modal fade" id="basicModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">

    <div class="modal-dialog">

        <div class="modal-content">

            <div class="modal-header">

            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>

            <h4 class="modal-title" id="myModalLabel"><?php echo $row->name; ?></h4>

            </div>

            <div class="modal-body" align="justify";>
                <div class="col-lg-12">
                    <div class="col-lg-12">
                        <?php
                        if(isset($row->attachment) && $row->attachment!='' )
                        {
                        ?>
                       <div class="inline-img">
                           <img src="<?php echo base_url().'uploads/flash_news/'.$row->attachment;?>" height="100%" width="100%" alt="Image Description"></div> 
                    </div>
                        <?php }?>
                    <div class="col-lg-12">
                      <h5> <?php echo $row->description; ?></h5>  
                    </div> 
                </div>
                
            
            </div>

            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Continue</button>

        </div>

    </div>

  </div>

</div>
    
    <?php }}?>
<script>

$(document).ready(function(e) {
    
//    if(window.location=="http://www.acme.edu.np/")
if(window.location=='<?php echo base_url();?>')
{
    $('#basicModal').modal();
}


});

</script>
                    
                            
					<?php }else{?>
                    
					             
    		<div class="col-lg-12">          
            <?php
				if(!isset($view_file)){
					$view_file="";
				}
				if(!isset($module)){
                                    
					$module = $this->uri->segment(1);
				}
				if(($view_file!='') && ($module!='')){
					$path = $module."/".$view_file;
					$this->load->view($path);
				} 
				else {
					$new_description = str_replace("../../../","./",$description); //replacing image location as root location to display image
//					echo '<h2>'.$title.'</h2>';
//					echo $new_description;//put your designs here
//                    $this->load->module('pages');
                    $this->load->view('pages/view');
				}
				
				//echo '<pre>';
				//print_r($this->session);
				//die();
            ?>
            </div>
            
          <?php } ?>
                   

			<div class="container">
                            
                    <?php 
               $first_bit = $this->uri->segment(1);
               if($first_bit=="" || $first_bit == "home"){?>                             
			<div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carousel" data-slide-to="0" class="active"></li>
					<li data-target="#carousel" data-slide-to="1"></li>
					<li data-target="#carousel" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="active item" style="background-image: url(<?php echo base_url();?>design/frontend/img/images/img1.jpg)">
					</div>
					<div class="item" style="background-image: url(<?php echo base_url();?>design/frontend/img/images/img2.jpg)">
					</div>
					<div class="item" style="background-image: url(<?php echo base_url();?>design/frontend/img/images/img3.jpg)">
					</div>
				</div>
				<a class="carousel-control left" href="#carousel" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
				<a class="carousel-control right" href="#carousel" data-slide="next"><i class="fa fa-chevron-right"></i></a>
			</div>
                          
				<div class="two-col row">
					<div class="col-md-8 main-content">
						<div class="intro-block panel light-style">
							<h1>Acme Engineering College</h1>
							<div class="text-holder clearfix">
								<div class="inline-img"><img src="<?php echo base_url();?>design/frontend/img/images/img4.jpg" height="336" width="252" alt="image description"></div>
								<p>Acme Engineering College, established in 2000 A.D., aims to provide quality education and has grown to become leading centre for engineering education in Nepal within a short span of its history.</p>
								<p>The College has many distinguished scholors on its faculty honored by their peers for important contribution and excellent performance to the field they study. Contact with these hard-working educators offers students the best possible entry point to the world of today where ideas and technology mesh.</p>
								<p>The College is situated at very easy location and is very much a part of the locality Sitapaila.</p>
							</div>
						</div>
						<div class="tabpanel" role="tabpanel">
							<ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="#tab1" aria-controls="tab1" role="tab" data-toggle="tab">Tab One</a></li>
								<li role="presentation"><a href="#tab2" aria-controls="tab2" role="tab" data-toggle="tab">Tab Two</a></li>
								<li role="presentation"><a href="#tab3" aria-controls="tab3" role="tab" data-toggle="tab">Tab Three</a></li>
								<li role="presentation"><a href="#tab4" aria-controls="tab4" role="tab" data-toggle="tab">Tab Four</a></li>
							</ul>
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="tab1">
									<h2>Lorem ipsum dolor</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At quasi reiciendis, nihil, est quidem eligendi temporibus numquam officia quod distinctio! Quas quisquam ut voluptatem excepturi officiis earum blanditiis ducimus voluptate.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse culpa earum nihil ipsa impedit error sapiente facere vero. Illo aperiam inventore itaque quasi reprehenderit soluta doloribus optio eos est excepturi.</p>
								</div>
								<div role="tabpanel" class="tab-pane" id="tab2">
									<h2>Lorem ipsum dolor</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At quasi reiciendis, nihil, est quidem eligendi temporibus numquam officia quod distinctio! Quas quisquam ut voluptatem excepturi officiis earum blanditiis ducimus voluptate.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse culpa earum nihil ipsa impedit error sapiente facere vero. Illo aperiam inventore itaque quasi reprehenderit soluta doloribus optio eos est excepturi.</p>
								</div>
								<div role="tabpanel" class="tab-pane" id="tab3">
									<h2>Lorem ipsum dolor</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At quasi reiciendis, nihil, est quidem eligendi temporibus numquam officia quod distinctio! Quas quisquam ut voluptatem excepturi officiis earum blanditiis ducimus voluptate.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse culpa earum nihil ipsa impedit error sapiente facere vero. Illo aperiam inventore itaque quasi reprehenderit soluta doloribus optio eos est excepturi.</p>
								</div>
								<div role="tabpanel" class="tab-pane" id="tab4">
									<h2>Lorem ipsum dolor</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At quasi reiciendis, nihil, est quidem eligendi temporibus numquam officia quod distinctio! Quas quisquam ut voluptatem excepturi officiis earum blanditiis ducimus voluptate.</p>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Esse culpa earum nihil ipsa impedit error sapiente facere vero. Illo aperiam inventore itaque quasi reprehenderit soluta doloribus optio eos est excepturi.</p>
								</div>
							</div>
						</div>
					</div>
					<aside class="col-md-4 sidebar">
                                            
            <div class="intro-block panel light-style">
                
                    <div class="text-holder clearfix">
                    <h3 class="block_title"> Message From Principal</h3>
                        <div class="inline-img"><img src="<?php echo base_url();?>design/frontend/img/images/principal.jpg" height="100" width="120" alt="image description"></div>
                    
                    
                        <p>It is with great pride that I welcome you to Acme Engineering College. Our College is a learning community that seeks success for each student as we strive to deepen our core values of: faith, life-long learning, problem solving, and responsible citizenship. These  Learning Expectations help us to illustrate what we expect from the graduates at Acme Engineering College. We envision them to be fully prepared to not only enter into the college  of their choice, but to also be prepared to step forward as citizens who understand their responsibility to give back to others and to their communities. Coupled together, these provide a solid base for students to face new challenges and to meet them with confidence and grace.
                           <br/><strong>Thank you</strong>
                            <br/>Pradip Kumar Paudyal
                            <br/>Principal
                            <br/>Acme Engineering College(AEC)</p>
                    
                    </div> 
               

            </div>
						<div class="panel light-style">
		<h3 class="block_title"> Resources</h3>
                                <?php 
								$i=1;
								foreach($notice_list->result() as $row) { 
								if($i<=3)
								{
								?>
                               
                            <div class="front_body block_border">
                                <?php
                                if($student_id==1)
                                {?>
                                    <h4><a href="<?php echo base_url();?>uploads/notice/<?php echo $row->attachment; ?>" target="_blank"><?php echo word_limiter($row->name,15); ?></a></h4>
                               <?php }
                                else {?>
                                    <h4><a href="<?php echo base_url();?>users_student/login" ><?php echo word_limiter($row->name,15); ?></a></h4>
                                    <?php }
                                ?>

<!--                                <span class="date_info">--><?php //echo $row->published_date; ?><!--</span>-->
                                <p><?php echo word_limiter($row->description,5).'...';?></p>
                             </div>
                                <?php }
								$i++; 
								}?>
                
                           <a href="<?php echo base_url();?>notice">>>View All...</a>
           
						</div>
						<div class="panel dark-style">
							<h3>News and Events</h3>
							                                <?php 
								$i=1;
								foreach($news->result() as $row) {
									if($i<=5)
									{
									 ?>
                                <div class="front_body block_border">
                                    <h4><a href="<?php echo base_url();?>news/details/<?php echo $row->slug;?>"><?php echo $row->title; ?></a></h4>
                                    <span class="date_info"><?php echo $row->published_date; ?></span>
                                    <p><?php echo word_limiter($row->description,5).'...';?></p>
                                    
                                </div>
                             
                                <?php }
								$i++;
								}?>
                                                        <a href="<?php echo base_url();?>news">>>View All...</a>
						</div>
						<div class="panel dark-style">
							<h3>Academic Overview</h3>
							<ul class="side-list">
								<li><a href="#">Master Degree</a></li>
								<li><a href="#">Bachelor Degree</a></li>
							</ul>
						</div>
					</aside>
				</div>
                            <div class="col-md-12">
				<div class="row testimonial">
					<h2>Testimonials</h2>
					<div id="carousel-testimonial" class="carousel slide" data-ride="carousel">
						<div class="carousel-inner" role="listbox">
                                                   <?php                                                    //var_dump($testimonial->result()); die;
                                                   foreach($testimonial->result() as $row)
                                                   {
                                                     if($row->id==2)
                                                     {
                                                   ?>
                                                    
							<div class="item active">
								<blockquote>
									<q>
									<?php	echo $row->description;?>
									</q>
                                                                    <cite><?php echo ucfirst($row->posted_by).','.ucfirst($row->category);?></cite>
								</blockquote>
							</div>
                                                    <?php
                                                   }else
                                                   {?>
                                                       <div class="item">
								<blockquote>
									<q>
									<?php	echo $row->description;?>
									</q>
                                                                    <cite><?php echo ucfirst($row->posted_by).','.ucfirst($row->category);?></cite>
								</blockquote>
							</div>
                                                  <?php }
                                                   
                                                     }
                                                    ?>
<!--							<div class="item">
								<blockquote>
									<q>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate illum ipsum nemo possimus, sapiente sit totam eaque officia a cumque animi sequi reiciendis ea aliquid, officiis nam illo cum in.
									</q>
									<cite>Sanjay kumar Siwa, Teacher</cite>
								</blockquote>
							</div>
							<div class="item">
								<blockquote>
									<q>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate illum ipsum nemo possimus, sapiente sit totam eaque officia a cumque animi sequi reiciendis ea aliquid, officiis nam illo cum in.
									</q>
									<cite>Nabin Bhattarai, Student</cite>
								</blockquote>
							</div>
							<div class="item">
								<blockquote>
									<q>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptate illum ipsum nemo possimus, sapiente sit totam eaque officia a cumque animi sequi reiciendis ea aliquid, officiis nam illo cum in.
									</q>
									<cite>Sanjay kumar Siwa, Teacher</cite>
								</blockquote>
							</div>-->
						</div>
						<a class="carousel-control left" href="#carousel-testimonial" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
						<a class="carousel-control right" href="#carousel-testimonial" data-slide="next"><i class="fa fa-chevron-right"></i></a>
					</div>
				</div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                            <div>
                                
				<div class="row column-holder">
					<div class="col-md-6">
						<div class="panel light-style">
							<h3>Mission Statement</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo, porro, molestiae! Dolorum illum cum assumenda perferendis, iusto sapiente et! Suscipit aliquid harum culpa odio, doloribus. Nulla asperiores voluptatem incidunt cumque?</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quisquam magnam nisi, rerum inventore culpa. Aut maiores expedita debitis, soluta voluptate inventore quis cupiditate nemo veritatis ratione ipsam tempora, vero.</p>
							<a href="#">Read More</a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel light-style">
							<h3>Academic Overview</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis eveniet numquam, est porro facilis reiciendis illo laborum distinctio magni. Doloremque cumque doloribus, placeat voluptate sapiente numquam nesciunt velit. Modi, laboriosam.</p>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quisquam magnam nisi, rerum inventore culpa. Aut maiores expedita debitis, soluta voluptate inventore quis cupiditate nemo veritatis ratione ipsam tempora, vero.</p>
							<a href="#">Read More</a>
						</div>
					</div>
				</div>
			</div>
                            </div>
                            <?php }?> 
                            <div class="clearfix"></div>
		<footer id="footer">
			<div class="footer-holder">
				<div class="container">
					<div class="row">
						<div class="col col-sm-6 col-md-3">
							<h3>Contact Us</h3>
							<address>
								Acme Engineering College (AEC) <br>
								Sitapaila, Kathmandu, Nepal <br>
								<a href="mailto:acme@acme.edu.np">acme@acme.edu.np</a> <br>
								Tel: +9771-4282962, 4280445, 4670924, 4670925 <br>
								Fax: +9771-4282947 <br>
								Post Box No.: 8849
							</address>
						</div>
						<div class="col col-sm-6 col-md-3">
							<h3>Subscribe to Newsletter</h3>
							<form class="newsletter-form" action="#">
								<fieldset>
									<p>Sign up with your Email to get updates about new Offers.</p>
									<div class="form-group">
										<label for="newsletter-email">Email address:</label>
										<input type="email" class="form-control" id="newsletter-email">
									</div>
									<button type="submit" class="btn btn-default"><i class="fa fa-envelope-o"></i>Suscribe</button>
								</fieldset>
							</form>
						</div>
						<div class="col col-sm-6 col-md-3">
							<h3>Quick Links</h3>
							<ul class="footer-links">
								<li><a href="#">Feedback</a></li>
								<li><a href="#">Webmail</a></li>
								<li><a href="#">FAQ</a></li>
								<li><a href="#">Contact</a></li>
								<li><a href="#">Quick Link</a></li>
							</ul>
						</div>
						<div class="col col-md-3 col-sm-6">
							<h3>About Us</h3>
							<ul class="footer-links">
								<li><a href="#">About Us</a></li>
								<li><a href="#">Help</a></li>
								<li><a href="#">Terms Of Use</a></li>
								<li><a href="#">Privacy Policy</a></li>
								<li><a href="#">Jobs</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-frame">
				<div class="container">
					<div class="row">
						<div class="col-md-6">
							<p>&copy;2015, Acme Engineering College, All Rights Reserved.</p>
						</div>

						<div class="col-md-6">
							<span class="by"><?php foreach($counter->result() as $row){?>
                                    <p style="padding-top:7px;">Page Visit:<?php echo $row->counter;?></p>
                                <?php }	?>
</span>
						</div>
					</div>
				</div>
			</div>
		</footer>
	</div>
        
    </div>
        </div>
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>


</body>
</html>