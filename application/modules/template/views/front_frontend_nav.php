<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $site_settings['site_name'];?></title>
<meta name="title" content="<?php echo $site_settings['site_name'];?>">
<meta name="keywords" content="<?php echo $site_settings['meta_topic'];?>">
<meta name="description" content="<?php echo $site_settings['meta_data'];?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
<link rel="shortcut icon" href="<?php echo base_url();?>uploads/settings/<?php echo $site_settings['favicon'];?>">

<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/normalize.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/main.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/bootstrap.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/responsive-slider.css">

<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/smoothDivScroll.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/flags32.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/jquery.feedBackBox.css">

<script src="<?php echo base_url();?>design/frontend/js/jquery.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/vendor/modernizr-2.6.2.min.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/bootstrap.min.js"></script>

<script src="<?php echo base_url();?>design/frontend/js/jquery.feedBackBox.js"></script>


</head>
<body>
    <div class="container">   
        
        <section class="header">
        <div id="heading_top">
          <div class="row">
            <div class="col-lg-1"> 
<!--                <a title="Upveda Technology Pvt. Ltd, Nepal" href="#"><img src="<?php echo base_url();?>uploads/settings/<?php echo $site_settings['logo'];?>" alt="SERDeN logo" height="80" style="text-align:center;"></a>-->
<!--                <img src="<?php echo base_url();?>design/frontend/img/ayurveda_logo_white.png">-->
             </div>
            <div class="col-lg-7">
                <div class="col-lg-4 logo_info_title">
                	<p>Ayurveda</p> 
                        <strong>Nepal - German Joint Venture</strong>
                </div>	
                <div class="col-lg-3 logo_info">
                    <img src="<?php echo base_url();?>design/frontend/img/a_11.png" class="img-responsive">
<!--                <img src="<?php echo base_url();?>design/frontend/img/head_first.png"/>
                <img src="<?php echo base_url();?>design/frontend/img/head_second.png"/>-->
                </div>
                <div class="col-lg-5 logo_info_title">
                	<p>Health Home</p>
                        <strong>Pioneer Panca-Karma Centre of Nepal</strong>
                </div>
            </div>
            <div class="col-lg-3">
<!--                <ul class="icons">
                    <li class="icon"><a href=""><img src="<?php echo base_url();?>design/frontend/img/twitter.jpg"></a></li>
                    <li class="icon"><a href=""><img src="<?php echo base_url();?>design/frontend/img/facebook.png"></a></li>
                    <li class="icon"><a href=""><img src="<?php echo base_url();?>design/frontend/img/google.jpg"></a></li>
                </ul>-->
                <div class="language_margin">
<!--                <ul class="language f32">
                    <li><a href="#" class="flag np" title="Nepali"></a></li>
                     <li><a href="#" class="flag de" title="German"></a></li>  DEU for germany 
                    <li><a href="#" class="flag gb" title="English"></a></li>
                    <li><a href="#" class="flag jp" title="Japanese"></a></li>
                    <li><a href="#" class="flag ru" title="Russian"></a></li>
                   
                </ul>-->
                <ul class="language language_padding" style="letter-spacing: 3px;">
                    <li><a href="#">Eng</a></li>
                    <li><a href="#">Ger</a></li>
                    <li><a href="#">Nep</a></li>
                    <li><a href="#">Jap</a></li>
                    <li><a href="#">Rus</a></li>
                </ul>
                    
                </div>
                <div class="clearfix"></div>
                <div class="search search_margin">
                                <form role="form">
                                    <input type="text" class="search-form" autocomplete="off" placeholder="Search">
                                    <i class="fa fa-search"></i>
                                </form>
                </div>
                <div class="clearfix"></div>
                <div class="signin">
                    <a href="#"> <p>Guest Sign in</p></a>
                </div>
             
                
            </div>
              <div class="col-lg-1"></div>
          </div>
        </div>
        </section>
    	<div class="clearfix"></div>
    	<!--navigation bar------------------------------------------------------------>
        <div class="menu_bar">
            <div class="col-lg-1"></div>
            <div class="col-lg-10">
                <div class="navbar navbar-default background_image_default" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="navbar-collapse collapse menu_background" style="height: 1px;">
<!--                    <ul class="nav navbar-nav menu_color">
                                    <?php foreach($headernav as $header){//still need to work on slug?> 
                                    <li class="col-lg-2">
                                        
                                        <?php if($header['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                                         <?php
                                         //print_r($header);

                                                            if($header['title']=="home")
                                                            {
                                                               // echo "skdkfajf";
                                                        ?>
                                                            
                                                            <a href="<?php echo $prefix.$header['href']?>" target="<?php echo $header['target']?>"><img src="<?php echo base_url();?>design/frontend/img/home_icon.png" class="home_link nav-current" /></a>
                                                             <?php

                                                                }
                                                             else{
                                                            ?>
                                                                <a href="<?php echo $prefix.$header['href']?>" target="<?php echo $header['target']?>"><?php echo $header['title']?>
                                                            <?php }
                                                                 ?>                           
                                                                </a>		
                                        </li>
                                        <?php }/*end of parent foreach*/ ?>
                                    </ul>-->
                    
                    
                    
                    
                    
                    
                  <ul class="nav navbar-nav menu_color">
                     <?php foreach($headernav as $header){//still need to work on slug?> 
                   <?php //var_dump($headernav[0]); die();?>
                      
                          
                         <?php if(empty($header['children'])){ echo '<li>';/*this is for no-child*/} else{
    ?>
                     
                      <li class="dropdown dropdown_background ">
                          
                         <?php }/*this is for dropdown*/?>
                                 <?php if(!empty($header['children'])){/*this is for dropdown*/?> 
                         
                       <?php if($header['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                           
                          <a href="<?php echo $prefix.$header['href']?>" target="<?php echo $header['target']?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $header['title']?>                                    
                              <b class="caret"></b> 
                          
                             <?php }/*end of if*/
                             
                             else { /*this is for no-child*/?>                            
                            	<?php if($header['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                              
                                <a href="<?php echo $prefix.$header['href']?>" target="<?php echo $header['target']?>"><?php echo $header['title']?>
                            <?php } ?>                           
                                </a>
                            <?php if(!empty($header['children'])){/*this is for dropdown*/?> 
                              
                                    <ul class="dropdown-menu dropdown_menu_background">
                                        
                                        <?php foreach($header['children'] as $key=>$child){ 
                                            //var_dump($header['children']); 
                                            if ($key=='0'|| $key!='grand_children')
                                            {
                                            ?>  
                                        
                                            <li>                                            
                            			<?php 
                                                if($child['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                                                <a href="<?php echo $prefix.$child['href']; ?>" target="<?php echo $child['target']?>"><?php echo $child['title']; ?></a>
                                            <?php 
                                            $value_id=$child['id'];
                                            
                                            if (isset($header['children']['grand_children']))
                                                {
                                                foreach($header['children']['grand_children'] as $grand_child)
                                                  {
                                                    if ($grand_child['parent_id']==$value_id)
                                                     {
                                                    ?>
                                                
                                             
                                                
                                                <ul class="dropdown-menu">
                                                    <li>
                                                           <?php if(!empty($grand_child['children']['grand_children'])){/*this is for dropdown of grand child*/?> 

                                                                 <?php if($grand_child['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>

                                                          <a href="<?php echo $prefix.$grand_child['href']?>" target="<?php echo $grand_child['target']?>" class="dropdown-toggle" data-toggle="dropdown"><?php echo $grand_child['title']?>                                    
                                                              <b class="caret"></b> 

                                                             <?php }/*end of if*/

                                                         else { /*this is for no-grand_child*/?>                            
                                                            <?php if($grand_child['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>

                                                            <a href="<?php echo $prefix.$grand_child['href']?>" target="<?php echo $grand_child['target']?>"><?php echo $grand_child['title']?>
                                                        <?php } ?> 
                                                    </li>
                                                </ul>
                                                
                                                
                                                
                                                
                                               <?php }}}?> 
                                            </li>
                                        <?php }}/*end of child foreach*/ ?>
                                    </ul>
                                        <?php } ?>  
                        
                    </li>
                          
                    <?php }/*end of parent foreach*/ ?>
                      
                  </ul>
                </div><!--/.nav-collapse -->
              </div><!--banner-------------------------------------------------------------------->
              </div>
            
            <div class="col-lg-1"></div>
        </div>
              
    						
				<!--============================================start of responsive slider============================-->
                    <div class="col-lg-12 margin_bottom">
                        <div class="right_corner_content">
<!--                            <nav class="social">
                                <ul>
                                    <li><a href="#"><p>Twitter</p> <i class="fa fa-twitter"></i></a></li>
                                  <li><a href="#"><p>Skype</p> <i class="fa fa-skype"></i></a></li>
                                  <li><a href="#"><p>Facebook</p> <i class="fa fa-facebook"></i></a></li>
                                  <li><a href="http://behance.net">Behance <i class="fa fa-behance"></i></a></li>
                                </ul>
                            </nav>-->
                            
                            <ul id="floating-social-icons">
                                <li><a href="#"><img src="<?php echo base_url();?>design/frontend/img/icon/twitter.png" alt="" class="img-responsive" /></a></li>
                                <li><a href="#"><img src="<?php echo base_url();?>design/frontend/img/icon/skype.png" alt="" class="img-responsive" /></a></li>
                                <li><a href="#"><img src="<?php echo base_url();?>design/frontend/img/icon/facebook.png" alt="" class="img-responsive" /></a></li>
<!--                                <li><a href="#"><img src="<?php echo base_url();?>design/frontend/img/icon/facebook.png" alt="" class="img-responsive" /></a></li>
                                <li><a href="#"><img src="<?php echo base_url();?>design/frontend/img/icon/facebook.png" alt="" class="img-responsive" /></a></li>-->
                            </ul>
                            <div id="feedback">
<!--                                <a href="#"><img src="design/frontend/img/feedback.png" alt="" title="Feedback form"/></a>-->
<!--                                <div id="feedback"></div>-->
                            </div>
                            <div class="chat_blank">
                                <a href="#"><img src="design/frontend/img/chat_box.png" alt="" title="You can make a Live chat! Click here"/></a>
                             </div>
                        </div>

                       
                        
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10 small_body">
                            
                             	<?php 
					$first_bit = $this->uri->segment(1);
					if($first_bit=="" || $first_bit == "home"){?>
									
			<!-- start of responsive slider -->
                            
                             <div class="slider_and_content">
                                    <div class="col-lg-3">
                                        <div class="introduction">
                                            <?php foreach($introduction->result() as $row)
                                            {  
                                            ?>
                                            <h2><strong><?php echo ucwords($row->title);?></strong></h2>
                                            <div class="row">
                                                <div class="col-lg-12" style="text-align:justify;"> 
<!--                                                     <img src="<?php echo base_url();?>design/frontend/img/fac1.jpg" width="90px" style="float:left; padding:10px;">-->
                                                    <p> <?php echo word_limiter($row->description,40);?></p>
<!--                                                    <p> <?php //echo $row->description;?></p>-->
                                                </div>
                                                <a href="<?php echo base_url();?>introduction" style="float:right; color:#06F; margin-right:12px;" class="read_more">Read More...</a>
                                            </div>
                                            <?php 
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="responsive-slider" data-spy="responsive-slider" data-autoplay="true">
                                            <div class="slides" data-group="slides">
                                                    <ul>
                                                        <?php foreach($banner as $row){ //displaying all banner ?> 
                                                        <li>
                                                            <div class="slide-body" data-group="slide">
                                                                <img src="<?php echo base_url();?>uploads/banner/<?php echo $row['attachment']?>" style="width:1140px;">
                                                            </div>
                                                        </li>
                                                            <?php }?>
                                                    </ul>
                                            </div>
                                            <a class="slider-control left" href="#" data-jump="prev">
                                            <img src="<?php echo base_url();?>design/frontend/img/sprite_icons_left.PNG" >    
                                            </a>
                                            <a class="slider-control right" href="#" data-jump="next">
                                                <img src="<?php echo base_url();?>design/frontend/img/sprite_icons_right.PNG" > 
                                            </a>
                                        </div>
                                    </div>
                                </div>
                    
                                <script src="<?php echo base_url();?>design/public/js/responsive-slider.js"></script>
                                <script src="<?php echo base_url();?>design/public/js/jquery.event.move.js"></script>

              <!--==============================================end of responsive slider============================-->   
				
            <div class="clearfix"></div>	
            
            <!-- section of our branches-->
            <div class="col-lg-12 branches">
                <div class="col-lg-4">
                    <h3><strong>Our Location</strong></h3>
                  <div class="support">
                      <?php foreach($pokhara->result() as $row) {?>
                    <h4> <?php echo ucwords($row->title);?></h4>
                    <div class="row">
                      <div class="col-lg-12" style="text-align:justify;"> <img src="<?php echo base_url();?>design/frontend/img/pokh.jpg" width="90px" style="float:left; padding:10px;"> 
                          <p> <?php echo word_limiter($row->description,15);?></p>
<!--                           <p> <?php //echo $row->description;?></p>-->
                          <a href="<?php echo base_url();?>pokhara" style="float:right; color:#06F; margin-right:12px;" class="read_more">Read More...</a> </div>
                    </div>
                      <?php 
                                            }
                                            ?>
                    <?php foreach($kathmandu->result() as $row) {?>
                      <h4> <?php echo ucwords($row->title);?></h4>
                    <div class="row">
                      <div class="col-lg-12" style="text-align:justify;"> <img src="<?php echo base_url();?>design/frontend/img/kat.jpg" width="90px" style="float:left; padding:10px;"> 
<!--                          <p> <?php //echo //word_limiter($row->description,30);?></p>-->
                           <p> <?php echo $row->description;?></p>
                          <a href="<?php echo base_url();?>kathmandu" style="float:right; color:#06F; margin-right:12px;" class="read_more">Read More...</a> </div>
                      </div>
                      <?php 
                                            }
                                            ?>
                  </div>
                </div>
                <div class="col-lg-4">
                <h3><strong>Our Medical Team</strong></h3>
                  <div class="support" style="height: 254px;">
                      <?php foreach($doctors->result() as $row) {?>
                      <h4> <?php echo ucwords($row->title);?></h4>
                      <div class="row">
                          <div class="col-lg-12" style="text-align:justify;">
                              <p> <?php echo word_limiter($row->description,15);?></p>
<!--                           <p> <?php //echo $row->description;?></p>-->

                      <a href="<?php echo base_url();?>doctors" style="float:right; color:#06F; margin-right:12px;" class="read_more">Read More...</a> 
                          </div>
                      </div>
                      <?php 
                                            }
                                            ?>
                      <?php foreach($therapist->result() as $row) {?>
                      <h4> <?php echo ucwords($row->title);?></h4>
                      <div class="row">
                          <div class="col-lg-12" style="text-align:justify;">
                              <p> <?php echo word_limiter($row->description,15);?></p>
<!--                           <p> <?php //echo $row->description;?></p>-->
<!--                      <p>Dr Rishi Ram Koirala is a Ayurvedic physician and medical director at the Ayurveda Health Home (AHH). He studied 11 years in Nepal, India and Sri Lanka and since 1982 he treated successfully countless numbers of patients in Nepal and abroad. In addition to his extraordinary medical ability, he is also a spiritual healer due to his deep roots from a traditional healer-family.</p>
                      <p>He mainly specialises in Ayurveda medicine, Pancakarma, medicinal plants and etc...... </p>-->
                      <a href="<?php echo base_url();?>therapist" style="float:right; color:#06F; margin-right:12px;" class="read_more">Read More...</a> 
                          </div>
                      </div>
                      <?php 
                                            }
                                            ?>
                  </div>
                  </div>
        
                 <div class="col-lg-4">
                <h3><strong>Message from</strong></h3>
                  <div class="support future_projects_height_given" style="height: 254px;">
                      <h4></h4>
                      <div class="row">
                          <div class="col-lg-12" style="text-align:justify;"> <img src="<?php echo base_url();?>design/frontend/img/doc_rishi.jpg" width="90px" style="float:left; padding:10px;"> 
                          <?php foreach($message_from_director->result() as $row) {?>
                          <strong><?php echo ucwords($row->title);?></strong>
                          <p> <?php //echo word_limiter($row->description,30);?></p>
                           <p> <?php echo $row->description;?></p>
                          <a href="<?php echo base_url();?>message-from-director" style="float:right; color:#06F; margin-right:12px;" class="read_more">Read More...</a> </div>
                     </div>
                      <?php 
                                            }
                                            ?>
                    <div class="row">
                      <div class="col-lg-12" style="text-align:justify;"> <img src="<?php echo base_url();?>design/frontend/img/doc_rishi.jpg" width="90px" style="float:left; padding:10px;"> 
                          <?php foreach($message_from_founder->result() as $row) {?>
                          <strong><?php echo ucwords($row->title); ?></strong>
                          <p> <?php //echo word_limiter($row->description,30);?></p>
                           <p> <?php echo $row->description;?></p>
                          <a href="<?php echo base_url();?>message-from-founder" style="float:right; color:#06F; margin-right:12px;" class="read_more">Read More...</a> </div>
                    </div>
                      <?php 
                                            }
                                            ?>
                      
<!--                    <div class="row">
                      <div class="col-lg-12" style="text-align:justify;"> <img src="<?php echo base_url();?>design/frontend/img/doc_rishi.jpg" width="90px" style="float:left; padding:10px;"> 
                          <?php foreach($message_from_medical_director->result() as $row) {?>
                          <strong><?php echo ucwords($row->title); ?></strong>
                          <p> <?php //echo word_limiter($row->description,30);?></p>
                           <p> <?php echo $row->description;?></p>
                          <a href="<?php echo base_url();?>message-from-medical-director" style="float:right; color:#06F; margin-right:12px;" class="read_more">Read More...</a> </div>
                    </div>-->
                      <?php 
                                            }
                                            ?>
                      
                      
<!--                      width="560" height="315"
                    <h4> Youtube Video</h4>
                    <div class="row">
                      <div class="col-lg-12" style="text-align:justify;"> <img src="<?php echo base_url();?>design/frontend/img/fac1.jpg" width="90px" style="float:left; padding:10px;"> With latest trends in Web Services, we provide fast turnaround time with best quality at reasonable price. Static websites, database Driven, WordPress, Joomla, Drupal, CMS, Custom Programming etc.....  </div>
                      <a href="#" style="float:right; color:#06F; margin-right:12px;" class="read_more">Read More...</a> 
                    </div>-->
                  </div>
                </div>
            </div>
           
            <!-- end of our branches-->
            
           <!-- section of our services--> 
           <div class="our_services">
                <div class="col-lg-9">
<!--                <div class="scrollingHotSpotLeft scrollingHotSpotLeftVisible" style="display: none; opacity: 0.35;"></div>
                    <div class="scrollingHotSpotRight scrollingHotSpotRightVisible" style="opacity: 0; display: block;"></div>-->
<!--                    <div id="makeMeScrollable">
                     
                            <a href="#"><img src="design/frontend/img/gallery/1.jpg" alt="" id="" title="Ayurveda1"/>
                                <p class="text_over_image">Choose from our offers </p>
                                <p class="text_over_image_br">or ask for more</p>
                            </a>
                     
                        
                        
                        <a href="#"><img src="design/frontend/img/gallery/2.jpg" alt="" id="" title="Ayurveda2"/>
                            <p class="text_over_image">Classical Panacakarma</p>
                        <div class="text_over_image">
					<p class="text_over_image">describe the desire</p>
                        </div> 
                        </a>
                        
                        <a href="#"><img src="design/frontend/img/gallery/3.jpg" alt="" id="" title="Ayurveda3"/>
                            <p class="text_over_image">Benefit of other Therapies</p>
                                <p class="text_over_image_br"> and Programs we offer</p>
                        <div class="text_over_image">
					<p class="text_over_image">describe the desire</p>
                        </div> 
                        </a>
                        
                        <a href="#"><img src="design/frontend/img/gallery/4.jpg" alt="" id="" title="Ayurveda4"/>
                            <p class="text_over_image">Special Therapy Packages </p>
                                <p class="text_over_image_br">& Training</p>
                        <div class="text_over_image">
					<p class="text_over_image">describe the desire</p>
                        </div> 
                        </a>
                        
                        <a href="#"><img src="design/frontend/img/gallery/5.jpg" alt="" id="" title="Ayurveda5"/>
                            <p class="text_over_image">AYURVEDA a science of life</p>
                        <div class="text_over_image">
					<p class="text_over_image">describe the desire</p>
                        </div> 
                        </a>
                        
                        <a href="#"><img src="design/frontend/img/gallery/6.jpg" alt="" id="" title="Ayurveda6"/>
                              <p class="text_over_image">Accommodation</p>
                        <div class="text_over_image">
					<p class="text_over_image">describe the desire</p>
                    </div> 
                        </a>
                      
                        <a href="#"><img src="design/frontend/img/gallery/7.jpg" alt="" id="" title="Ayurveda7"/></a>
                        <a href="#"><img src="design/frontend/img/gallery/8.jpg" alt="" id="" title="Ayurveda8"/></a>
                        <a href="#"><img src="design/frontend/img/gallery/9.jpg" alt="" id="" title="Ayurveda9"/></a>
                    </div>-->

                    <div id="makeMeScrollable">
                        <a href="#"><img src="design/frontend/img/gallery/1.jpg" alt="" id="" title="Ayurveda1"/></a>
                        <a href="#"><img src="design/frontend/img/gallery/2.jpg" alt="" id="" title="Ayurveda2"/></a>
                        <a href="#"><img src="design/frontend/img/gallery/3.jpg" alt="" id="" title="Ayurveda3"/></a>
                        <a href="#"><img src="design/frontend/img/gallery/4.jpg" alt="" id="" title="Ayurveda4"/></a>
                        <a href="#"><img src="design/frontend/img/gallery/5.jpg" alt="" id="" title="Ayurveda5"/></a>
                        <a href="#"><img src="design/frontend/img/gallery/6.jpg" alt="" id="" title="Ayurveda6"/></a>
<!--                        <a href="#"><img src="design/frontend/img/gallery/7.jpg" alt="" id="" title="Ayurveda7"/></a>
                        <a href="#"><img src="design/frontend/img/gallery/8.jpg" alt="" id="" title="Ayurveda8"/></a>
                        <a href="#"><img src="design/frontend/img/gallery/9.jpg" alt="" id="" title="Ayurveda9"/></a>-->
                    </div>
                    
        <script src="<?php echo base_url();?>design/frontend/js/gallery/jquery-ui-1.10.3.custom.min.js"></script>
	<script src="<?php echo base_url();?>design/frontend/js/gallery/jquery.mousewheel.min.js"></script>     
        <script src="<?php echo base_url();?>design/frontend/js/gallery/jquery.kinetic.min.js"></script>
	<script src="<?php echo base_url();?>design/frontend/js/gallery/jquery.smoothdivscroll-1.3-min.js"></script>
             

                </div>
                <div class="col-lg-3">
                    <div class="testimonial">
                    <h3>Testimonials</h3>
                      <div class="media testimonial-inner">
                            <div class="pull-left image_width">
                                <img class="img-responsive img-circle" src="<?php echo base_url();?>design/frontend/img/testimonials.jpg">
                            </div>
                            <div class="media-body">
                                <p>This is used for testing</p>
                                <span><strong>Testing</strong> </span>
                            </div>
                       </div>
<!--                       <div class="media testimonial-inner">
                            <div class="pull-left image_width">
                                <img class="img-responsive img-circle" src="<?php echo base_url();?>design/frontend/img/testimonials.jpg">
                            </div>
                            <div class="media-body">
                                <p>This is used for testing</p>
                                <span><strong>Testing</strong> </span>
                            </div>
                       </div>-->
                      
                    </div>
                      <div class="master_card col-lg-12">
                           <h3>We Accept</h3>
                           <div class="padding_image">
                                <img class="img-responsive" src="<?php echo base_url();?>design/frontend/img/visa.jpg">
                                <img class="img-responsive" src="<?php echo base_url();?>design/frontend/img/master.jpg">
                                <img class="img-responsive" src="<?php echo base_url();?>design/frontend/img/western_union.jpg">
                                <img class="img-responsive" src="<?php echo base_url();?>design/frontend/img/bank_transfer.jpg">
                           </div>
                       </div>
                       
                       
                </div>
            </div>
           <!-- end of our services--> 
           
            
           
                                   
                        
				<?php }
                                else{?>
                      
    		
         <!--   start of the content ---------------------------------------->
            <div class="col-lg-12 other_page">
            <div class="col-lg-8 left_div">    
               
            <?php
				if(!isset($view_file)){
					$view_file="";
				}
				if(!isset($module)){
					$module = $this->uri->segment(1);
				}
				if(($view_file!='') && ($module!='')){
					$path = $module."/".$view_file;
					$this->load->view($path);
				} 
				else {
					$new_description = str_replace("../../../","./",$description); //replacing image location as root location to display image
					echo nl2br($new_description);//put your designs here
				}
				
				//echo '<pre>';
				//print_r($this->session);
				//die();
            ?>
               
            </div>
            
            <div class="col-lg-4 right_div">
              <div class="support">
                <h4> Web Services</h4>
                <div class="row">
                  <div class="col-lg-12" style="text-align:justify;"> 
                      <img src="<?php echo base_url();?>design/frontend/img/fac1.jpg" width="90px" style="float:left; padding:10px;"> 
                      With latest trends in Web Services, we provide fast turnaround time with best quality at reasonable price. Static websites, database Driven, WordPress, Joomla, Drupal, CMS, Custom Programming etc.....  
                  </div>
                  <a href="#" style="float:right; color:#06F; margin-right:12px;">Read More...</a> </div>
              </div>
              <div class="support">
                <img src="<?php echo base_url();?>design/frontend/img/youtube.png" class="img-responsive" style="width:150%;">
<!--                  <iframe  width="217" height="170" border="1px solid #000" /*src="http://www.youtube.com/embed/Nl39QcbOkJg"*/ frameborder="0" allowfullscreen></iframe>-->
              </div>
              <div class="support">
                <h4>Important Links</h4>
                <div class="link">
                <ul class="list-group"><li class="list-group-item"> <a href="#">Seminar</a></li>
                  <li class="list-group-item"><a href="#">Conference</a></li>
                  <li class="list-group-item"><a href="#">Portfolio</a></li>
                 </ul>
                </div>
              </div>
            </div>
            </div>
          
           <!-- end-content-->
           
           
	    <?php
             }
             ?>	 
            
            
                        </div>

            
                        
                        <!-- start of footer section-->
            
          <div class="clearfix"></div>
          
          <div class="footer">
<!--             
            <div class="row">-->
                <div class="col-lg-1"></div>
                <div class="col-lg-10 background_colour">
                    <div class="col-lg-5 margin_left">
<!--                  <div class="footernav">-->
                    <ul class="nav padding_left_footer">
                    	<?php foreach($footernav as $footer){//still need to work on slug?> 
                        	<li>
                            	<?php if($footer['navtype']=='URL'){$prefix = '';}else{$prefix = base_url();}?>
                                <a href="<?php echo $prefix.$footer['href'];?>" target="<?php echo $footer['target'];?>"><?php echo $footer['title']; ?></a>
                            </li>
                      	<?php } ?>
                    </ul>
                       
<!--                  </div>-->
                    </div>
                    <div class="col-lg-3 footernav pull-right">
                        <p>All rights reserved! &copy; <?php echo date("Y"); ?></p>
                    </div>
                    <div class="col-lg-4 footernav pull-right">
                        <p>Ayurveda Health Home</p>
<!--                         <a href="http://upvedatech.com">Upveda CMS</a>-->
<!--                <img src="<?php echo base_url();?>design/frontend/img/logo.png" width="100" height="50">-->
                    </div>
                </div>
<!--            </div>-->
              <div class="col-lg-1 ">
                             
              </div>
              

          </div>
            <!-- end of footer section -->
                        <div class="col-lg-1"></div>
                         
                        
                        
                    </div>
                                
         
         
        
	</div><!--end of container-->
	<script src="<?php echo base_url();?>design/frontend/js/plugins.js"></script> 
        <script src="<?php echo base_url();?>design/frontend/js/main.js"></script> 
        <script src="<?php echo base_url();?>design/frontend/js/jquery.event.move.js"></script>
        <script src="<?php echo base_url();?>design/frontend/js/responsive-slider.js"></script>
        
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. --> 
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>
        
        <!-- script for moving image gallery -->
        <script type="text/javascript">
	$(document).ready(function () {
		$("#makeMeScrollable").smoothDivScroll({
			mousewheelScrolling: "allDirections",
			manualContinuousScrolling: true,
			autoScrollingMode: "onStart"
		});
	});
        </script>
        
        <script type="text/javascript">
        //<![CDATA[    
        $(document).ready(function () {
            $('#feedback').feedBackBox();
        });
        // ]]>
        </script>
    
    
</body>
</html>