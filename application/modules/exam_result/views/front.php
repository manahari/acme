<div class="col-lg-12 background-ffffff footer-top">
	<h2>Notice</h2>
<!--        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
            <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th>
                    <th>Notice Type</th>
                    <th>Date</th>
                    <th>Title</th>
                    <th>Download</th>
                </tr>
            </thead>
            <tbody>
    <?php
    $i=1;
                foreach($examination as $row)
                {
                    ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo($row->department_id==1)?'Internal': 'University';?></td>
                    <td><?php echo $row->published_date; ?></td>
                    <td><?php echo $row->name; ?></td>
                    <td>
                        <?php
                        if(isset($row->attachment)&&($row->attachment!=NULL))
                        {
//                            if($student_id==1)
//                            {
                        ?>
                        <a href="<?php echo base_url();?>uploads/examination/<?php echo $row->attachment;?>">Download</a>
                        <?php //}else{?>
                                <a href="<?php //echo base_url();?>users_student/login">Download</a>
                           <?php }
//                        }
                        ?>
                    </td>
                </tr>
                    
    <?php
    $i++;
				}
				?>
        </tbody>
        </table>-->
        <div class="panel dark-style col-md-5">
            <h3>Internal Notice</h3>
            <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable">
                 <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th>
                    <th>Date</th>
                    <th>Title</th>
                    <th></th>
                </tr>
            </thead>
                <tr>
                <?php 
                $sn=1;
                foreach ($examination as $row) {
                    if ($row->department_id == 1) {
                        ?>
                      <td><?php echo $sn;?></td>
                    <td><?php echo $row->published_date; ?></td>
                    <td><?php echo $row->name; ?></td>
                    <td><a title="Download" href="<?php echo base_url();?>uploads/examination/<?php echo $row->attachment;?>"><img src="<?php echo base_url();?>design/frontend/img/file_download.png" alt="download"/></a></td>
                        <?php $sn++;}
                    } ?>
             </tr>
             </table>
        </div>
        <div class="col-md-1"></div>
    <div class="panel dark-style col-md-5">
        <h3>University Notice</h3>
        
             <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable">
                 <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th>
                    <th>Date</th>
                    <th>Title</th>
                    <th></th>
                </tr>
            </thead>
                 <tr>
                <?php 
                $sn=1;
                foreach ($examination as $row) {
                    if ($row->department_id == 2) {
                        ?>
                      <td><?php echo $sn;?></td>
                    <td><?php echo $row->published_date; ?></td>
                    <td><?php echo $row->name; ?></td>
                    <td><a title="Download" href="<?php echo base_url();?>uploads/examination/<?php echo $row->attachment;?>"><img src="<?php echo base_url();?>design/frontend/img/file_download.png" alt="download"/></a></td>
                        <?php $sn++;}
                    } ?>
             </tr>
             </table>
            

    </div>
    <?php if($front_group==2){?>
    <div class="panel dark-style col-md-11">
        <h3>Student Results (Only for teacher)</h3>

        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable">
            <thead>
            <tr>
                <th class="checkbox-column">S.No.</th>
                <th>Date</th>
                <th>Title</th>
                <th></th>
            </tr>
            </thead>
            <tr>
                <?php
                $sn=1;
                foreach ($examination as $row) {
                    if ($row->department_id == 2) {
                        ?>
                        <td><?php echo $sn;?></td>
                        <td><?php echo $row->published_date; ?></td>
                        <td><?php echo $row->name; ?></td>
                        <td><a title="Download" href="<?php echo base_url();?>uploads/examination/<?php echo $row->attachment;?>"><img src="<?php echo base_url();?>design/frontend/img/file_download.png" alt="download"/></a></td>
                        <?php $sn++;}
                } ?>
            </tr>
        </table>


    </div>
    <?php }?>
</div>
