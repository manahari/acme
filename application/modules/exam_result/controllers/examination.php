<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class examination extends MX_Controller
{

	function __construct() {
		$this->load->model('mdl_examination');
		parent::__construct();
	}


	function index(){
		$examination= $this->mdl_examination->get_front('id');
		$data['examination'] = $examination;
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);
	}
	
	function examinationdetail()
	{
		$member_id = $this->uri->segment(3);
		$member= $this->mdl_member->get_where($member_id);
		$data['member'] = $member;
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);
	}
}