<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class notice extends MX_Controller
{

	function __construct() {
		$this->load->model('mdl_notice');
		parent::__construct();
	}


	function index(){
		$notice= $this->mdl_notice->get_front('id');
		$data['notice'] = $notice;
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);
	}
	
	function noticedetail()
	{
		$member_id = $this->uri->segment(3);
		$member= $this->mdl_member->get_where($member_id);
		$data['member'] = $member;
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);
	}
}