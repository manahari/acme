<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('forum'); //module name is pages here	
	}
	
	function index()
	{	
		$data['query'] = $this->get('id');
		
		$data['view_file'] = "admin/table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
	
	function get_data_from_post()
	{
		$data['title'] = $this->input->post('title', TRUE);			
		$data['slug'] = strtolower(url_title($data['title']));	
		$data['description'] = $this->input->post('description', TRUE);	
                $data['published_date'] = $this->input->post('published_date', TRUE);	
		$data['status'] = $this->input->post('status', TRUE);
		
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$data['upd_date'] = strtotime(date("Y-m-d"));
			}
			else
			{
				$data['ent_date'] = strtotime(date("Y-m-d"));
				$data['upd_date'] = NULL;
			}
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['title'] = $row->title;
			$data['description'] = $row->description;
                        $data['published_date'] = $row->published_date;	
			$data['status'] = $row->status;			
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
	
	
	function create()
	{
		$update_id = $this->uri->segment(4);
		if($update_id != 1){
			$submit = $this->input->post('submit', TRUE);
		
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
			
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
					
			$data['view_file'] = "admin/form";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
			
		}else{
		redirect('admin/forum');
		}
	}

	
	function delete()
	{	$this->load->model('mdl_forum');
		$delete_id = $this->uri->segment(4);							
		
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/forum');
			}
		else
		{
			$this->mdl_forum->_delete($delete_id);
			redirect('admin/forum');
		}			
			
	}
	
	
	function submit()
	{		
	
	$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		if(is_numeric($update_id)){						
			$this->form_validation->set_rules('title', 'Title', 'required|xss_clean'); //we don't want unique_validation error while editing
		}
		else{
		$this->form_validation->set_rules('title', 'Title', 'required|xss_clean|is_unique[up_pages.title]|is_unique[up_modules.title]'); //unique_validation check while creating new
		}
		/*end of validation rule*/
		
		
		if ($this->form_validation->run($this) == FALSE)
		{
			$this->create();
		}
		else
		{
			$data = $this->get_data_from_post();
			
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){				
				$this->_update($update_id, $data);
			} else {
				$this->_insert($data);
			}
			
			redirect('admin/forum');
		}
			
	}
		

	function get($order_by){
	$this->load->model('mdl_forum');
	$query = $this->mdl_forum->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_forum');
	$query = $this->mdl_forum->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_forum');
	$this->mdl_forum->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_forum');
	$this->mdl_forum->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_forum');
	$this->mdl_forum->_delete($id);
	}
	
	/*
	function error404(){
	$this->load->module('template');
	$this->template->errorpage();	
	}*/
}