<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class forum extends MX_Controller
{

	function __construct() {
	parent::__construct();
	}

	function index(){
//		$first_bit = $this->uri->segment(1);
//		$second_bit = $this->uri->segment(2);
//		
//			if ($second_bit==""){
//				//this must be a custom page from the CMS
//				
//					if($first_bit==""){
//						$first_bit = "home";
//						//$banner = $this->banner();
//					}
//			}
//				
//			$query = $this->get_pages_if_live('slug', $first_bit);
//			
//			foreach($query->result() as $row){
//				
//				$data['title'] = $row->title;					
//				$data['slug'] = strtolower(url_title($data['title']));
//				$data['description'] = $row->description;
//				$data['ent_date'] = date("Y-m-d");
//				$data['upd_date'] = NULL;
//				
//			}
//			
//			if($query->num_rows == 0)
//			{
//				$this->load->module('template');
//				$this->template->errorpage();
//			}
//			else
//			{
//				$this->load->module('template');
//				$this->template->front($data);
//			}
            
		$data['forum']=$this->get_forum_list('id');
		$data['view_file']="table";
		$this->load->module('template');
		$this->template->front($data);
	}
        
        function details(){
                $slug = $this->uri->segment(3);
                if($slug!=""){
                    
                
                $data['details']=$this->get_detail($slug);
                $data['comment_detail']=$this->get_comments_detail($slug);
                $data['view_file']="forum_details";
		$this->load->module('template');
		$this->template->front($data);
                }
                else{
                    $this->index();
                }
        }
        function get($order_by){
	$this->load->model('mdl_forum');
	$query = $this->mdl_forum->get($order_by);
	return $query;
	}
        function get_forum_list($order_by){
	$this->load->model('mdl_forum');
	$query = $this->mdl_forum->get_forum_list($order_by);
	return $query;
	}
        
        function get_detail($slug){
	$this->load->model('mdl_forum');
	$query = $this->mdl_forum->get_details($slug);
	return $query;
	}

	function get_comments_detail($slug){
        $this->load->model('mdl_forum');
	$forum_id = $this->mdl_forum->get_forum_id($slug);
        $this->load->model('forum_comment/mdl_forum_comment');
        $query = $this->mdl_forum_comment->get_comments($forum_id);
        return $query;
        }
	function get_pages_if_live($col, $value) {
	$this->load->model('mdl_forum');
	$query = $this->mdl_forum->get_pages_if_live($col, $value);
	return $query;
	}

	
	function get_where_custom($col, $value) {
	$this->load->model('mdl_forum');
	$query = $this->mdl_forum->get_where_custom($col, $value);
	return $query;
	}

}