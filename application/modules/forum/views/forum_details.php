<div class="col-md-12">
    <a href="<?php echo base_url()?>forum" class="no_hover"><h2 style="color:#5e7e95;" >AEC <span>Blog</span></h2></a>
   <div class="widget box">
        <div class="blog_details">
            <div class="form-group">
                <?php foreach($details->result() as $row){
                    $forum_id=$row->id;?>
                <div class="col-md-12">
                    <h3><?php echo $row->title;?></h3>
                    <p><strong>Published On: </strong> <?php echo $row->published_date;?></p>
                    <div class="col-md-12" style="text-align: justify"><?php echo $row->description;?></div>
                 <?php }?>
                </div>
                </div>
            </div>
        </div>
    </div> 
<div class="col-md-12">
    <div class="widget box">
        <div class="member_list_wrapper">
            <div class="form-group">
                <h4>Comments</h4>
            </div>
        </div>
        
    </div>
    <div class="col-md-12">
        <div class="col-md-1"></div>
        <div class="col-md-11 margin_bottom_comments">
             <?php
                    echo validation_errors('<p style="color: red;">', '</p>');
                    echo form_open_multipart('forum_comment/submit_comment', 'class="form-horizontal row-border" id="validate-1"');				
            ?>
            <div class="form-group">
            <?php echo form_input('name','', 'class="form-control required" placeholder="Name*"');?>
                <?php echo form_hidden('forum_id',$forum_id, 'class="form-control required" placeholder="Name*"');?>
            </div>
            <div class="form-group">
            <?php echo form_textarea('comment','', 'class="form-control required" placeholder="Comment*"');?>
            </div>
            <?php 							
                  echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
             ?>
            <?php echo form_close(); ?>  
        </div>
<!--        <div class="col-md-1"></div>-->
    </div>
<div class="widget box">
<!--    <div class="col-md-1"></div>-->
    <div class="col-md-12">
         <?php foreach($comment_detail->result() as $row){?>
         <div class="form-group comments_div"> 
             <div class="col-md-2" ><img src="<?php echo base_url();?>design/frontend/img/comment.png" style="height: 48px;width: 48px; border-radius:4px;"/></div>
             <div class="col-md-10">
                 <p class="comment_title"><strong><?php echo ucwords($row->name);?></strong></p>
                 <p><?php echo $row->comment;?></p>
                 <p><strong>Commented On:</strong> <?php echo date("Y-m-d",$row->ent_date);?></p>
             </div>
        </div>
         <?php }?>
    </div>
<!--    <div class="col-md-1"></div>-->
</div>
</div>

    
    
