<div class="col-lg-12  margin_bottom_forum">
    <h2 class="blog_heading" style="color:#5e7e95;">AEC  <span>Blog</span></h2>
	<div class="blog_details">
    	<ul class="publications list-group list-icon">
            <?php $sno = 1;?>
            <?php foreach($forum->result() as $row){?>
            <a href="<?php echo base_url()?>forum/details/<?php echo $row->slug;?>">
                <h3><?php echo $sno.". "; $sno++;?><?php echo $row->title;?></h3></a>
            <p><strong>Published On: </strong> <?php echo $row->published_date;?></p>
            <div style="text-align: justify"><?php echo word_limiter($row->description,100);?></div>
            <div><a href="<?php echo base_url()?>forum/details/<?php echo $row->slug;?>" class="read_more">Read more</a></div>
            <?php }?>
            
    	</ul>
        </div>
</div>

