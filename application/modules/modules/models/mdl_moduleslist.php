<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_moduleslist extends CI_Model {

	function __construct() {
	parent::__construct();
	}

	function get_table() {
	$table = "up_modules";
	return $table;
	} 

	function get($order_by){
	$table = $this->get_table();
	$this->db->order_by($order_by,'ASC');
	$query=$this->db->get($table);
	return $query;
	}
	
	function get_where($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$query=$this->db->get($table);
	return $query;
	}
	
	function _insert($data){
	$table = $this->get_table();
	$this->db->insert($table, $data);
	}
	
	function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}
	
	function _delete($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->delete($table);
	$this->delete_permissions_of_that_module_too($id);
	}
	
	
	function delete_permissions_of_that_module_too($id){	
	$table = 'up_permissions';	
	$query=$this->db->get($table)->result();echo '<pre>';
		foreach($query as $row){
				$roles_array = unserialize($row->roles);
				foreach($roles_array as $check){
					if($check == $id){
						unset($roles_array[$id]);	
						$new_role = serialize($roles_array);										
						$this->db->where('id', $row->id);
						$this->db->update($table, array('roles' => $new_role));					
					}
				}
		}
	}
	
	
	function get_modules_dropdown()
	{
	$this->db->select('id, title');	
	$this->db->order_by('title');
	$dropdowns = $this->db->get('up_modules')->result();
	foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->title;
		}
	if(empty($dropdownlist)){return NULL;}
	$finaldropdown = $dropdownlist;
	return $finaldropdown;
	}
	
	function get_module_ids(){
	$this->db->select('id');	
	$this->db->order_by('id');
	$dropdowns = $this->db->get('up_modules')->result();
	foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->id;
		}
	if(empty($dropdownlist)){return NULL;}
	$finaldropdown = $dropdownlist;
	return $finaldropdown;	
	}
	
	function get_id_from_modulename($modulename){	
	$table = $this->get_table();		
	$this->db->select('id')	;
	$this->db->where('slug', $modulename);		
	$query=$this->db->get($table)->result();
	$result = $query[0]->id;
	return $result;
	}
	
	
	function get_all_slug_from_module(){	
	$table = $this->get_table();		
	$this->db->select('slug');	
	$query=$this->db->get($table)->result();
		$i = 1;
		foreach($query as $row){
			$new_array[$i] = $row->slug;
			$i++;				
		}
	return $new_array;
	}
	
	
	function get_slug_from_moduleid($module_id){	
	$table = $this->get_table();		
	$this->db->select('slug')	;
	$this->db->where('id', $module_id);		
	$query=$this->db->get($table)->result();
	$result = $query[0]->slug;
	return $result;
	}

}