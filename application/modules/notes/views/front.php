<?php $base_url=  base_url();?>
<div class="col-lg-12 background-ffffff footer-top">

	<h2>Notes</h2>
    <?php
    echo form_open_multipart('admin/notes/submit', 'class="form-horizontal row-border" id="validate-1"');
    ?>
    <div class="col-lg-12 panel light-style">
        <div class="col-lg-3">
            <div class="form-group">
        <label class="col-md-2 control-label">Faculty</label>
        <div class="col-md-10">
            <?php $options = array(
                '0' => 'Select Faculty',
                'Computer'    => 'Computer',
                'Electronics'  => 'Electronics',
                'Civil'    => 'Civil',
                'Architecture'  => 'Architecture',
                'ISE' => 'Information System Engineering',
                'EM' => 'Engineering Management',

            );
            echo form_dropdown('search_faculty', $options, '','class="form-control" id="search_faculty"');?>
        </div>
    </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
        <label class="col-md-2 control-label">Type</label>
        <div class="col-md-10">
            <?php $options = array(
                '0' => 'Select Type',
                'Notes'    => 'Notes',
                'Syllabus'  => 'Syllabus',

            );
            echo form_dropdown('type', $options, '','class="form-control" id="type"');?>
        </div>
    </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
        <label class="col-md-3 control-label">Semester</label>
        <div class="col-md-9">
            <?php $options = array(
                '0' => 'Select Semester',
                '1st Semester'    => '1st Semester',
                '2nd Semester'  => '2nd Semester',
                '3rd Semester'    => '3rd Semester',
                '4th Semester'  => '4th Semester',
                '5th Semester'    => '5th Semester',
                '6th Semester'  => '6th Semester',
                '7th Semester'    => '7th Semester',
                '8th Semester'  => '8th Semester',
                '9th Semester'    => '9th Semester',
                '10th Semester'  => '10th Semester',

            );
            echo form_dropdown('semester', $options, '','class="form-control" id="semester"');?>
        </div>
    </div>
        </div>
        <div class="col-lg-3">
            <div class="form-actions">
                <span><a href="javascript:;" onclick="ajaxSearchNotes()" target="_self" class="btn btn-primary" >Search</a></span>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
    <div id="selected_record">
    <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable">
            <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th> 
                    <th>Faculty</th>
                    <th>Semester</th>
                    <th>Subject</th>
                    <th>Download</th>
                </tr>
            </thead>
            <tbody>
    <?php
    $i=1;
                foreach($notes as $row)
                {
                    ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row->faculty; ?></td>
                    <td><?php echo $row->semester; ?></td>
                    <td><?php echo $row->subject; ?></td>
                    <td>
                        <?php
                        if(isset($row->attachment)&&($row->attachment!=NULL))
                        {
                            if($student_id==1)
                            {
                        ?>
                        <a href="<?php echo base_url();?>uploads/notes/<?php echo $row->attachment;?>">Download</a>
                        <?php }else{?>
                                <a href="<?php echo base_url();?>users_student/login">Download</a>
                           <?php }
                        }
                        ?>
                    </td>
                </tr>
                    
    <?php
    $i++;
				}
				?>
        </tbody>
        </table>
 
</div>
</div>
<script>

    function ajaxSearchNotes() {
//        var slug = event.target.id;
        var csrf_test_name = $("input[name=csrf_test_name]").val();
        var faculty = $('#search_faculty').val();
        var type = $('#type').val();
        var semester = $('#semester').val();
        var base_url='<?php echo $base_url;?>'.concat('notes/get_selected_notes');
//            data:"faculty=" + faculty + "&type=" + type + "&semester="+semester,
        $.ajax({
            type:'POST',
            url:base_url,
            data: { 'csrf_test_name' : csrf_test_name,'faculty' : faculty,'type' : type,'semester' : semester  },
            success:function(data)
            {
                $('#selected_record').html(data);

            }
        });
    }
</script>