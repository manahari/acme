<?php $base_url=  base_url();?>
<div class="col-lg-12 background-ffffff footer-top">
	<h2>Notes</h2>

    <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable">
            <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th> 
                    <th>Faculty</th>
                    <th>Semester</th>
                    <th>Subject</th>
                    <th>Download</th>
                </tr>
            </thead>
            <tbody>
    <?php
    $i=1;
                foreach($notes as $row)
                {
                    ?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row->faculty; ?></td>
                    <td><?php echo $row->semester; ?></td>
                    <td><?php echo $row->subject; ?></td>
                    <td>
                        <?php
                        if(isset($row->attachment)&&($row->attachment!=NULL))
                        {
                            if($student_id==1)
                            {
                        ?>
                        <a href="<?php echo base_url();?>uploads/notes/<?php echo $row->attachment;?>">Download</a>
                        <?php }else{?>
                                <a href="<?php echo base_url();?>users_student/login">Download</a>
                           <?php }
                        }
                        ?>
                    </td>
                </tr>
                    
    <?php
    $i++;
				}
				?>
        </tbody>
        </table>
 
</div>
