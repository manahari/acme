-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 28, 2015 at 04:52 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `acme`
--

-- --------------------------------------------------------

--
-- Table structure for table `up_notes`
--

CREATE TABLE IF NOT EXISTS `up_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `semester` varchar(255) NOT NULL,
  `faculty` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('live','draft') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `up_notes`
--

INSERT INTO `up_notes` (`id`, `semester`, `faculty`, `subject`, `attachment`, `description`, `ent_date`, `upd_date`, `status`) VALUES
(1, '1st Semester', 'Computer', 'Advance Computer Architecture', '1.docx', 'fdsafsadfsadf\r\n', '2015-08-28', NULL, 'live');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
