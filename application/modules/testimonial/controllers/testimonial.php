<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class testimonial extends MX_Controller
{

	function __construct() {
            
		$this->load->model('mdl_testimonial');
		parent::__construct();
	}

function index(){
            
		$testimonial = $this->get('id');
                $data['testimonial']=$testimonial;
		$data['view_file']="table";
		$this->load->module('template');
		$this->template->front($data);
	}
	
function details()
	{ 
		$slug = $this->uri->segment(3);
                
		$query= $this->get_where($slug);
                foreach($query->result() as $row)
		{
			$data['title'] = $row->title;
                        $data['description'] = $row->description;
                        $data['attachment'] = $row->attachment;
                       $data['published_date'] = $row->published_date;
		}
                
                
               
		$data['view_file']="form";
		$this->load->module('template');
		$this->template->front($data);
	}
         function get_where($id){
	
	$query = $this->mdl_testimonial->get_where($id);
	return $query;
	}
       function get($order_by){
	$this->load->model('mdl_testimonial');
	$query = $this->mdl_testimonial->get($order_by);
	return $query;
	}
}
