-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 28, 2015 at 12:57 PM
-- Server version: 5.5.32
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `hprnepal`
--

-- --------------------------------------------------------

--
-- Table structure for table `up_testimonial`
--

CREATE TABLE IF NOT EXISTS `up_testimonial` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `published_date` int(11) NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `ent_date` int(11) NOT NULL,
  `upd_date` int(11) DEFAULT NULL,
  `status` enum('draft','live') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `up_testimonial`
--

INSERT INTO `up_testimonial` (`id`, `title`, `published_date`, `description`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(3, 'Annual General Assembly', 2014, 'HPR Nepal is conducting its Annual General Assembly Soon\n', 'annual-general-assembly', 2014, 2014, 'live');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
