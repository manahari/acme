<link rel="stylesheet" type="text/css" href="<?php echo base_url().'bootstrap/datepicker/css/datepicker.css'?>" />
<link rel="stylesheet/less" type="text/css" href="<?php echo base_url().'bootstrap/datepicker/less/datepicker.less'?>" />
<script src="<?php echo base_url().'bootstrap/datepicker/js/bootstrap-datepicker.js'?>"></script>
<!-- TinyMCE -->
<script language="javascript" type="text/javascript" src="<?php echo base_url();?>assets/tinyfck/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		plugins : "table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,zoom,flash,searchreplace,print,paste,directionality,fullscreen,noneditable,contextmenu",
		theme_advanced_buttons1_add_before : "save,newdocument,separator",
		theme_advanced_buttons1_add : "fontselect,fontsizeselect",
		theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,zoom,separator,forecolor,backcolor,liststyle",
		theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator,search,replace,separator",
		theme_advanced_buttons3_add_before : "tablecontrols,separator",
		theme_advanced_buttons3_add : "emotions,iespell,flash,advhr,separator,print,separator,ltr,rtl,separator,fullscreen",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		plugin_insertdate_dateFormat : "%Y-%m-%d",
		plugin_insertdate_timeFormat : "%H:%M:%S",
		extended_valid_elements : "hr[class|width|size|noshade]",
		file_browser_callback : "fileBrowserCallBack",
		paste_use_dialog : false,
		theme_advanced_resizing : true,
		theme_advanced_resize_horizontal : false,
		theme_advanced_link_targets : "_something=My somthing;_something2=My somthing2;_something3=My somthing3;",
		apply_source_formatting : true
	});

	function fileBrowserCallBack(field_name, url, type, win) {
		var connector = "<?php echo base_url();?>assets/tinyfck/filemanager/browser.html?Connector=connectors/php/connector.php";
		var enableAutoTypeSelection = true;

		var cType;
		tinyfck_field = field_name;
		tinyfck = win;

		switch (type) {
			case "image":
				cType = "Image";
				break;
			case "flash":
				cType = "Flash";
				break;
			case "file":
				cType = "File";
				break;
		}

		if (enableAutoTypeSelection && cType) {
			connector += "&Type=" + cType;
		}

		window.open(connector, "tinyfck", "modal,width=600,height=400");
	}
</script>
<!-- /TinyMCE -->


<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i> Notice</h4> 
            </div> 
            <div class="widget-content">
            	<?php
					echo form_open_multipart('admin/flash_news/submit', 'class="form-horizontal row-border" id="validate-1"');				
				?>                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Title</label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('name', $name, 'class="form-control"');?>
						</div> 
                    </div>
                                        
                    <div class="form-group">
                    	<label class="col-md-2 control-label">Attachment <span class="required">*</span></label> 
                        <div class="col-md-10"> 
								<?php  if(!empty($update_id)){
										
											$attach_prop = array(
												'type' => 'file',
												'name' => 'userfile',
												'value' => $attachment
												);
										}else{
											$attach_prop = array(
												'type' => 'file',
												'name' => 'userfile',
												'value' => $attachment,
												'class' => 'required'
												);
										}
								?>
                    
                            	<?php echo form_upload($attach_prop);?>
                            <p class="help-block">
                            Images only (jpg/jpeg/gif/png)</p>
                            <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                            <?php if(!empty($update_id)){?>
                            	<img src="<?php echo base_url();?>uploads/flash_news/<?php echo $attachment;?>" style="height:100px;"/>
                            <?php }?>
                            </label>
                        </div>
                        
                    </div>
                    
                    <div class="form-group"> 
                        <label class="col-md-2 control-label">Description</label> 
                        <div class="col-md-10">
                        	<?php echo form_textarea(array('id' => 'elm1', 'name' =>'description', 'value' => $description,'rows'=>'15', 'cols'=>'80', 'style'=> 'width: 100%', 'class'=> 'form-control'));?>
                        </div> 
					</div>
                    <?php 
                    if($published_date=='')
                    {
                        $published_date=$date_today;
                    }
                    ?>                    
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Published Date</label> 
                    	<div class="col-md-10 input-append date" id="datepicker1" data-date="" data-date-format="yyyy-mm-dd"> 
                            <?php echo form_input('published_date',$published_date, 'class="span2" size="16" type="text" value="" ');?>
                            <span class="add-on"><i class="icon-th"></i></span>
			</div>
                    </div>
                         
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Status</label> 
                    	<div class="col-md-10"> 
                            <?php $selected = $status;$options = array(
                              'draft'  => 'draft',
                              'live'    => 'live',
                            );                            
                            echo form_dropdown('status', $options, $selected,'class="form-control"');?>
						</div> 
                    </div>  
                    			
                  
                
                    
                    <div class="form-actions"> 
						<?php 		
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
							if (!empty($update_id)){echo form_hidden('update_id',$update_id);}	
						?>
					</div>                 
                    
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>
<script>
$('#datepicker1').datepicker();

</script>