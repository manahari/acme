<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class examination extends MX_Controller
{

	function __construct() {
		$this->load->model('mdl_examination');
		$this->load->model('exam_result/mdl_exam_result');
		parent::__construct();
	}


	function index(){
		$examination= $this->mdl_examination->get_front('id');

		$data['examination'] = $examination;
        $data['exam_result']= $this->mdl_exam_result->get_exam_result();
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);
	}
	
	function examinationdetail()
	{
		$member_id = $this->uri->segment(3);
		$member= $this->mdl_member->get_where($member_id);
		$data['member'] = $member;
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);
	}
}