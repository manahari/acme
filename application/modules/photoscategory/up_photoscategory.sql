-- phpMyAdmin SQL Dump
-- version 3.1.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 28, 2014 at 09:35 AM
-- Server version: 5.1.32
-- PHP Version: 5.2.9-1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `upvedacms`
--

-- --------------------------------------------------------

--
-- Table structure for table `up_photoscategory`
--

CREATE TABLE IF NOT EXISTS `up_photoscategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('draft','live') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `up_photoscategory`
--

INSERT INTO `up_photoscategory` (`id`, `name`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'Folder 1', 'folder-1', '2014-07-08', '2014-07-08', 'live'),
(4, 'Folder 3', 'folder-3', '2014-07-08', NULL, 'live'),
(3, 'Folder 2', 'folder-2', '2014-07-08', '2014-07-08', 'live');
