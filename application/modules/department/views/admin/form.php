<script src="<?php echo base_url();?>assets/tinymce/js/tinymce.min.js"></script>
<script>
    tinymce.init({
        selector: "textarea",
        theme: "modern",
        /*width: 300,
         height: 300,*/
        plugins: [
            "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor filemanager"
        ],
        content_css: "content.css",
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ]
    });

</script>

<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i> Add Report Category </h4> 
            </div> 
            <div class="widget-content">
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
					echo form_open_multipart('admin/department/submit', 'class="form-horizontal row-border" id="validate-1"');				
				?>                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Name <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('name', $name, 'class="form-control required"');?>
						</div> 
                    </div>
                    <div class="form-group"> 
                        <label class="col-md-2 control-label">Description</label> 
                        <div class="col-md-10">
                        	<?php echo form_textarea(array('id' => 'elm1', 'name' =>'description', 'value' => $description,'rows'=>'15', 'cols'=>'80', 'style'=> 'width: 100%', 'class'=> 'form-control'));?>
                        </div> 
					</div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Attachment(HOD) <span class="required">*</span></label>
                    <div class="col-md-10">
                        <?php  if(!empty($update_id)){

                            $attach_prop = array(
                                'type' => 'file',
                                'name' => 'userfile',
                                'value' => $attachment
                            );
                        }else{
                            $attach_prop = array(
                                'type' => 'file',
                                'name' => 'userfile',
                                'value' => $attachment,
                                'class' => 'required'
                            );
                        }
                        ?>

                        <?php echo form_upload($attach_prop);?>
                        <p class="help-block">
                            Images only (jpg/jpeg/gif/png)</p>
                        <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                            <?php if(!empty($update_id)){?>
                                <img src="<?php echo base_url();?>uploads/department/<?php echo $attachment;?>" style="height:100px;"/>
                            <?php }?>
                        </label>
                    </div>

                </div>
                
               
                    
                
                
                <div class="form-group"> 
                    	<label class="col-md-2 control-label">Status <span class="required"></span></label> 
                    	<div class="col-md-10"> 
                            <?php
                            $selected = $status;$options = array(
                              'draft'  => 'draft',
                              'live'    => 'live',
                            ); 
                            
                            echo form_dropdown('status', $options, $selected,'class="form-control"');?>
						</div> 
                    
                    </div>
                    <div class="form-actions"> 
						<?php 							
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
							if (!empty($update_id)){echo form_hidden('update_id',$update_id);}	
						?>
					</div>                 
                    
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>