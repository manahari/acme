<link rel="stylesheet" type="text/css" href="<?php echo base_url().'bootstrap/datepicker/css/datepicker.css'?>" />
<link rel="stylesheet/less" type="text/css" href="<?php echo base_url().'bootstrap/datepicker/less/datepicker.less'?>" />
<script src="<?php echo base_url().'bootstrap/datepicker/js/bootstrap-datepicker.js'?>"></script>
<!-- TinyMCE -->
<script language="javascript" type="text/javascript" src="<?php echo base_url();?>assets/tinyfck/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		plugins : "table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,zoom,flash,searchreplace,print,paste,directionality,fullscreen,noneditable,contextmenu",
		theme_advanced_buttons1_add_before : "save,newdocument,separator",
		theme_advanced_buttons1_add : "fontselect,fontsizeselect",
		theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,zoom,separator,forecolor,backcolor,liststyle",
		theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator,search,replace,separator",
		theme_advanced_buttons3_add_before : "tablecontrols,separator",
		theme_advanced_buttons3_add : "emotions,iespell,flash,advhr,separator,print,separator,ltr,rtl,separator,fullscreen",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		plugin_insertdate_dateFormat : "%Y-%m-%d",
		plugin_insertdate_timeFormat : "%H:%M:%S",
		extended_valid_elements : "hr[class|width|size|noshade]",
		file_browser_callback : "fileBrowserCallBack",
		paste_use_dialog : false,
		theme_advanced_resizing : true,
		theme_advanced_resize_horizontal : false,
		theme_advanced_link_targets : "_something=My somthing;_something2=My somthing2;_something3=My somthing3;",
		apply_source_formatting : true
	});

	function fileBrowserCallBack(field_name, url, type, win) {
		var connector = "<?php echo base_url();?>assets/tinyfck/filemanager/browser.html?Connector=connectors/php/connector.php";
		var enableAutoTypeSelection = true;

		var cType;
		tinyfck_field = field_name;
		tinyfck = win;

		switch (type) {
			case "image":
				cType = "Image";
				break;
			case "flash":
				cType = "Flash";
				break;
			case "file":
				cType = "File";
				break;
		}

		if (enableAutoTypeSelection && cType) {
			connector += "&Type=" + cType;
		}

		window.open(connector, "tinyfck", "modal,width=600,height=400");
	}
</script>
<!-- /TinyMCE -->


<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i> Subjects</h4>
            </div> 
            <div class="widget-content">
            	<?php
					echo form_open_multipart('admin/subjects/submit', 'class="form-horizontal row-border" id="validate-1"');
				?>
                <div class="form-group">
                    <label class="col-md-2 control-label">Faculty</label>
                    <div class="col-md-10">
                        <?php $selected = $faculty;$options = array(
                            'Computer'    => 'Computer',
                            'Electronics'  => 'Electronics',
                            'Civil'    => 'Civil',
                            'Architecture'  => 'Architecture',
                            'ISE' => 'Information System Engineering',
                            'EM' => 'Engineering Management',

                        );
                        echo form_dropdown('faculty', $options, $selected,'class="form-control" id="faculty"');?>
                    </div>
                </div>
                <div class="form-group">
                    	<label class="col-md-2 control-label">Semester</label> 
                    	<div class="col-md-10"> 
                            <?php $selected = $semester;

                                $options = array(
                                    '1st Semester'  => '1st Semester',
                                    '2nd Semester'  => '2nd Semester',
                                    '3rd Semester'  => '3rd Semester',
                                    '4th Semester'  => '4th Semester',
                                    '5th Semester'  => '5th Semester',
                                    '6th Semester'  => '6th Semester',
                                    '7th Semester'  => '7th Semester',
                                    '8th Semester'  => '8th Semester',
                                    '9th Semester'  => '9th Semester',
                                    '10th Semester'  => '10th Semester',
                                );


                            echo form_dropdown('semester', $options, $selected,'class="form-control" id="semester"');?>
						</div> 
                    </div>
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Subject</label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('subject', $subject, 'class="form-control"');?>
						</div> 
                    </div>
                                        
                    <div class="form-group">
                    	<label class="col-md-2 control-label">Status</label> 
                    	<div class="col-md-10"> 
                            <?php $selected = $status;$options = array(
                               'live'    => 'live',
                                'draft'  => 'draft',
                             
                            );                            
                            echo form_dropdown('status', $options, $selected,'class="form-control"');?>
						</div> 
                    </div>  
                    			
                  
                
                    
                    <div class="form-actions"> 
						<?php 		
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
							if (!empty($update_id)){echo form_hidden('update_id',$update_id);}	
						?>
					</div>                 
                    
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>
<script>
$('#semester').change(function(){
    var semester = $('#semester').val();
    $.ajax({
        type: "POST",
        url: 'get_subject_semester',
        data: "semester=" + semester
    }).done(function( result ) {
        $("#subject").append($('<option>', {
            value: '',
            text: '--Select Vdc--',
        }));
        $.each($.parseJSON(result), function(k, v) {
            $("#subject").append($('<option>', {
                value: v.code,
                text: v.name_eng,
            }));

        });
    });
});

</script>