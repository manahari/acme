<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class notes extends MX_Controller
{

	function __construct() {
		$this->load->model('mdl_notes');
		parent::__construct();
	}


	function index(){
		$notes= $this->mdl_notes->get_front('id');
		$data['notes'] = $notes;
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);
	}
	
	function notesdetail()
	{
		$member_id = $this->uri->segment(3);
		$member= $this->mdl_member->get_where($member_id);
		$data['member'] = $member;
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);
	}

    function get_selected_notes()
    {
        $data['faculty'] = $this->input->post('faculty', TRUE);
        $data['type'] = $this->input->post('type', TRUE);
//        $data['subject'] = $this->input->post('subject', TRUE);
        $data['semester'] = $this->input->post('semester', TRUE);
//        $data['description'] = $this->input->post('description', TRUE);
        $this->load->model('mdl_notes');
       $data['notes']= $this->mdl_notes->get_selected_notes($data);
        $this->load->module('template');
        $this->template->front_table($data);

    }
}