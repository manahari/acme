<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kathmandu') ;
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('subjects'); //module name is subjects here	
	}
	
	function index()
	{	
		$data['query'] = $this->get('id');
		
		$data['view_file'] = "admin/table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
	
	function get_data_from_post()
	{
		$data['faculty'] = $this->input->post('faculty', TRUE);
//        $data['type'] = $this->input->post('type', TRUE);
        $data['subject'] = $this->input->post('subject', TRUE);
                $data['semester'] = $this->input->post('semester', TRUE);
//		$data['description'] = $this->input->post('description', TRUE);
		$data['status'] = $this->input->post('status', TRUE);	
		
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
//				$attach = $this->get_attachment_from_db($update_id);
//				$data['attachment'] = $attach['attachment'];
				$data['upd_date'] = date("Y-m-d");
			}
			else
			{
//				$data['attachment'] = $this->input->post('userfile', TRUE);
				$data['ent_date'] = date("Y-m-d");
				//$data['upd_date'] = NULL;
			}
			
		return $data;
	}


		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['faculty'] = $row->faculty;
//            $data['type'] = $row->type;
            $data['subject'] = $row->subject;
//			$data['description'] = $row->description;
			$data['semester'] = $row->semester;				
//			$data['attachment'] = $row->attachment;
			$data['status'] = $row->status;		
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
		
	function get_attachment_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{			
			$data['attachment'] = $row->attachment;				
		}
			return $data;
	}
	
	
	function create()
	{
		$update_id = $this->uri->segment(4);
			$submit = $this->input->post('submit', TRUE);
		
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
			
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
			$data['date_today']=  date('Y-m-d');		
			$data['view_file'] = "admin/form";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
	}

	
	function delete()
	{	$this->load->model('mdl_subjects');
		$delete_id = $this->uri->segment(4);				
			
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/subjects');
			}
		else
		{
			$this->mdl_subjects->_delete($delete_id);
			redirect('admin/subjects');
		}				
	}
	
	
	function submit()
	{		
	//no validation in subjects because it has been created from jquery	

		$data = $this->get_data_from_post();
		
		$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){

				$this->_update($update_id, $data);
			} else {

				$this->_insert($data);
			}
        redirect('admin/subjects');
			
	}
	
	
	function do_upload($id) 
	{ 
	   $config['upload_path']   =   "./uploads/subjects/"; 
	   $config['file_name'] = $id;		   
	   $config['overwrite'] = TRUE;
	   $config['allowed_types'] =   "pdf|doc|docx|xls|xlxs";  
	  // $config['max_size']      =   "20480"; //that's 20MB
	   //$config['max_width']     =   "1907"; 
	   //$config['max_height']    =   "1280"; 

	   $this->load->library('upload',$config);
 
	   
		if ( ! $this->upload->do_upload())
		{
			//echo 'File cannot be uploaded';
			$datas = array('error' => $this->upload->display_errors());
			//print_r($datas);
			//die();
		}
		else
		{
			//echo 'File has been uploaded';
			$datas = array('upload_data' => $this->upload->data());		
			//die();
			
		}
		
		return $datas;
	}
		

	function get_id(){
	$this->load->model('mdl_subjects');
	$id = $this->mdl_subjects->get_id();
	return $id;
	}
	
	function get($order_by){
	$this->load->model('mdl_subjects');
	$query = $this->mdl_subjects->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_subjects');
	$query = $this->mdl_subjects->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_subjects');
	$this->mdl_subjects->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_subjects');
	$this->mdl_subjects->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_subjects');
	$this->mdl_subjects->_delete($id);
	}

}