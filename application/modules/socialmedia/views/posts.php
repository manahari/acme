<style>
.post-dl dt{
width:100px !important;
text-align:left !important;
}
.post-dl dd{
margin-left: 0px !important;
}
.heading_anchor{
text-decoration:none !important;
font-size:24px !important;
}
</style>

<?php foreach($socialmedia as $row){?>
    <div class="row">
        <div class="col-lg-1 col-sm-1"></div>
        <div class="col-lg-4 col-sm-4">
        	<?php if(empty($row->attachment)){$row->attachment = 'default.jpg';}?>
            <img src="<?php echo base_url();?>uploads/socialmedia/<?php echo $row->attachment;?>" class="img-responsive">
        </div>
        <div class="col-lg-1 col-sm-1"></div>
        <div class="col-lg-6 col-sm-6">
            <h3><a href="<?php echo base_url();?>socialmedia/<?php echo $row->id;?>" class="heading_anchor"><?php echo $row->title;?></a></h3>
            <dl class="dl-horizontal post-dl">
                <dt>Social Link</dt>
                <dd><?php echo $row->social_link;?></dd>           
            </dl>
        </div>  
    </div>
    <p></p>
<?php }?>