
<style>
.post-dl dt{
width:100px !important;
text-align:left !important;
}
.post-dl dd{
margin-left: 0px !important;
}
.heading_anchor{
text-decoration:none !important;
font-size:24px !important;
}
</style>

<p class="row col-xs-offset-1">
<a href="<?php echo base_url();?>socialmedia" class="heading_anchor">Social Media</a>
</p>

<?php foreach($single as $row){?>
    <div class="row col-xs-offset-1">
        <div class="col-lg-5 col-sm-5">
        	<?php if(empty($row->attachment)){$row->attachment = 'default.jpg';}?>
            <img src="<?php echo base_url();?>uploads/socialmedia/<?php echo $row->attachment;?>" class="img-responsive">
        </div>
        <div class="col-lg-5 col-sm-5">
            <h3><?php echo $row->title;?></h3>
            <dl class="dl-horizontal post-dl">
                <dt>Social Link</dt>
                <dd><?php echo $row->social_link;?></dd>          
            </dl>
        </div>  
    </div>
    <p></p>
<?php }?>