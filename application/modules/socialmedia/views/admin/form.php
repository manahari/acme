<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i> Social Media</h4> 
            </div> 
            <div class="widget-content">
            	<?php
					echo form_open_multipart('admin/socialmedia/submit', 'class="form-horizontal row-border" id="validate-1"');				
				?>                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Title <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('title', $title, 'class="form-control required"');?>
						</div> 
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-2 control-label">Social Link <span class="required">*</span></label> 
                        <div class="col-md-10">
                        	<?php echo form_input('social_link', $social_link, 'class="form-control required" placeholder="Add http:// or https:// to your link"');?>
                        </div> 
					</div>  
                         
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Status</label> 
                    	<div class="col-md-10"> 
                            <?php $selected = $status;$options = array(
                              'draft'  => 'draft',
                              'live'    => 'live',
                            );                            
                            echo form_dropdown('status', $options, $selected,'class="form-control"');?>
						</div> 
                    </div>   
                    
                    <div class="form-group">
                    	<label class="col-md-2 control-label">Attachment <span class="required">*</span></label> 
                        <div class="col-md-10"> 
								<?php  if(!empty($update_id)){
										
											$attach_prop = array(
												'type' => 'file',
												'name' => 'userfile',
												'value' => $attachment
												);
										}else{
											$attach_prop = array(
												'type' => 'file',
												'name' => 'userfile',
												'value' => $attachment,
												'class' => 'required'
												);
										}
								?>
                    
                            	<?php echo form_upload($attach_prop);?>
                            <p class="help-block">
                            Images only (jpg/jpeg/gif/png)</p>
                            <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                            <?php if(!empty($update_id)){?>
                            	<img src="<?php echo base_url();?>uploads/socialmedia/<?php echo $attachment;?>" style="height:100px;"/>
                            <?php }?>
                            </label>
                        </div>
                        
                    </div>
                    			
                  
                
                    
                    <div class="form-actions"> 
						<?php 		
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
							if (!empty($update_id)){echo form_hidden('update_id',$update_id);}	
						?>
					</div>                 
                    
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>