<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Socialmedia extends MX_Controller 
{
		function index()
        {
			$data['socialmedia'] = $this->get_socialmedia();
			
				$member_id = $this->uri->segment(2);	
				if(!empty($member_id)){ //i.e. if member_id is not empty
					$this->single_view($member_id);
				}
				else{
					$this->all_view($data);	
				}
		}
		
		function all_view($data){			
			$data['view_file'] = "posts";
			$this->load->module('template');
			$this->template->front($data);
		}
		
		function single_view($member_id){
			$this->load->model('mdl_socialmedia');
			$data['single'] = $this->mdl_socialmedia->get_single_view($member_id);
						
			$data['view_file'] = "view";
			$this->load->module('template');
			$this->template->front($data);			
		}
		
		function get_socialmedia()
		{
			$this->load->model('mdl_socialmedia');
			$query = $this->mdl_socialmedia->get_socialmedia();
			return $query->result();
		}
		

}