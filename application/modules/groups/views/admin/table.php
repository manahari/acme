<div class="manage">
    <input type="button" value="Add Content" id="create" onclick="location.href='<?php echo base_url()?>admin/groups/create';"/> 
</div>

<div class="widget box"> 

	<div class="widget-header"> 
    	<h4><i class="icon-reorder"></i> User Groups </h4> 
        <div class="toolbar no-padding"> 
        	<div class="btn-group"> 
            	<span class="btn btn-xs widget-collapse">
                	<i class="icon-angle-down"></i>
        		</span> 
        	</div> 
        </div> 
	</div>
    

    <div class="widget-content"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
            <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th> 
                    <th data-class="expand">Module Name</th>  
                    <th>Set Permission</th> 
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 
            <tbody> 
            <?php $sno = 1;?>
               <?php foreach($query->result() as $row){?>
            
                <tr> 
                	<td class="checkbox-column"><?php echo $sno; $sno++;?></td> 
                    <td><?php echo Ucfirst($row->name);?></td>
                    <td>
                    	<?php if($row->id != '1'){
							echo '<a href="'.base_url().'admin/permissions/group/'.$row->id.'"><i class="icon-key"></i>  Set Permission</a>';
							}
							else{ echo "The Admin group has access to everything"; }
						?>             
                    </td> 
                    <td class="edit">
                    	<?php if($row->id != '1'){?>
                            <a href="<?php echo base_url()?>admin/groups/create/<?php echo $row->id;?>"><i class="icon-pencil"></i></a>
                        <?php } ?>
                  </td>
                </tr> 
                
				<?php }	?>                
            </tbody> 
        </table> 
    </div>

</div><!--end of class="widget box"-->