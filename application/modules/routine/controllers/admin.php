<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Kathmandu') ;
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('routine'); //module name is routine here	
	}
	
	function index()
	{	
		$data['query'] = $this->get('id');
		
		$data['view_file'] = "admin/table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
	
	function get_data_from_post()
	{
		$data['department_id'] = $this->input->post('department_id', TRUE);		
		$data['semester_id']	= $this->input->post('semester_id', TRUE);
		
		$data['status'] = $this->input->post('status', TRUE);	
		
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$attach = $this->get_attachment_from_db($update_id);
				$data['attachment1']=$attach['attachment1']; 
                                $data['attachment2']=$attach['attachment2'];
				$data['upd_date'] = date("Y-m-d");
			}
			else
			{
				$data['attachment1'] = $this->input->post('attachment1', TRUE);
                                $data['attachment2'] = $this->input->post('attachment2', TRUE);
				$data['ent_date'] = date("Y-m-d");
				//$data['upd_date'] = NULL;
			}
			
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['department_id'] = $row->department_id;
			$data['semester_id'] = $row->semester_id;			
			$data['attachment1'] = $row->attachment1;				
			$data['attachment2'] = $row->attachment2;
			$data['status'] = $row->status;		
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
		
	function get_attachment_from_db($update_id)
	{
		$this->load->model('mdl_routine');
            $query = $this->mdl_routine->get_where($update_id);
		foreach($query->result() as $row)
		{			
			$data['attachment1'] = $row->attachment1;
                        $data['attachment2'] = $row->attachment2;
		}
			return $data;
	}
	
	
	function create()
	{
		$update_id = $this->uri->segment(4);
			$submit = $this->input->post('submit', TRUE);
		
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
			
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
			$data['date_today']=  date('Y-m-d');		
			$data['view_file'] = "admin/form";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
	}

	
	function delete()
	{	$this->load->model('mdl_routine');
		$delete_id = $this->uri->segment(4);				
			
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/routine');
			}
		else
		{
			$this->mdl_routine->_delete($delete_id);
			redirect('admin/routine');
		}				
	}
	
	
	function submit()
	{		
	//no validation in routine because it has been created from jquery	

		$data = $this->get_data_from_post();
		
		$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){						
					
		$attach = $this->get_attachment_from_db($update_id);
                $uploadattachment = $this->do_upload($update_id,$data);
                foreach ($uploadattachment as $key=>$value) {

                    foreach ($value as $key1 => $value1) {
                        if($value1['image_type']!='')
                        {
                           $data['attachment'.$key]=$value1['file_name'];
                        }
                    }
                }
               // var_dump($attach['attachment1']);die;
                $attachment = $uploadattachment['upload_data']['file_name'];
                if(empty($data['attachment1'])){
                $data['attachment1'] = $attach['attachment1'];}
                if(empty($data['attachment2'])){
                $data['attachment2'] = $attach['attachment2'];}
                
				
				$this->_update($update_id, $data);
			} else {								
					
				$nextid = $this->get_id();
                                
                                $uploadattachment = $this->do_upload($nextid,$data);
                                foreach ($uploadattachment as $key=>$value) {

                                    foreach ($value as $key1 => $value1) {

                                           $data['attachment'.$key]=$value1['file_name'];
                                    }
                                }
                                
                                                $this->_insert($data);	
                                        }
		
		redirect('admin/routine');	
			
	}
	
	
	function do_upload($id,$data) {
 $this->load->library('upload');

    $files = $_FILES;
    for($i=1; $i<=2; $i++)
    {

        $_FILES['userfile']['name']= $files['userfile']['name'][$i-1];
        $_FILES['userfile']['type']= $files['userfile']['type'][$i-1];
        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i-1];
        $_FILES['userfile']['error']= $files['userfile']['error'][$i-1];
        $_FILES['userfile']['size']= $files['userfile']['size'][$i-1];    

        $this->upload->initialize($this->set_upload_options($id,$i));
        $this->upload->do_upload();
        $datas[$i] = array('upload_data' => $this->upload->data());
 }
 return $datas;
        }
        private function set_upload_options($id,$i)
{   
//  upload an image options
    $config = array();
    $config['upload_path'] = './uploads/routine/';
    $config['allowed_types'] = 'gif|doc|xls|pdf|xlsx|docx|jpg|jpeg|png|zip|rar';
    $config['max_size']      = '20480';
    $config['overwrite']     = FALSE;
    $config['max_width']     =   "1907"; 
    $config['max_height']    =   "1280";
    $config['file_name'] = $id.'attachment'.$i;

    return $config;
}
		

	function get_id(){
	$this->load->model('mdl_routine');
	$id = $this->mdl_routine->get_id();
	return $id;
	}
	
	function get($order_by){
	$this->load->model('mdl_routine');
	$query = $this->mdl_routine->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_routine');
	$query = $this->mdl_routine->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_routine');
	$this->mdl_routine->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_routine');
	$this->mdl_routine->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_routine');
	$this->mdl_routine->_delete($id);
	}
}