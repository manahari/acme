<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class routine extends MX_Controller
{

	function __construct() {
		$this->load->model('mdl_routine');
		parent::__construct();
	}


	function index(){
//		$routine= $this->mdl_routine->get_front('id');
//		$data['routine'] = $routine;
                $data['computer']= $this->mdl_routine->get_front('Computer');//get latest news in home page
                $data['electronics'] = $this->mdl_routine->get_front('Electronics');
                $data['civil']= $this->mdl_routine->get_front('civil');//get latest news in home page
                $data['architecture'] = $this->mdl_routine->get_front('architecture');
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);
	}
	
	function routinedetail()
	{
		$member_id = $this->uri->segment(3);
		$member= $this->mdl_member->get_where($member_id);
		$data['member'] = $member;
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);
	}
}