<div class="manage">
    <input type="button" value="Add Routine" id="create" onclick="location.href='<?php echo base_url()?>admin/routine/create';"/>  
</div>

<div class="widget box"> 

	<div class="widget-header"> 
    	<h4><i class="icon-reorder"></i> Routine </h4> 
        <div class="toolbar no-padding"> 
        	<div class="btn-group"> 
            	<span class="btn btn-xs widget-collapse">
                	<i class="icon-angle-down"></i>
        		</span> 
        	</div> 
        </div> 
	</div>
    

    <div class="widget-content"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
            <thead> 
                <tr> 
                	<th class="checkbox-column">S.No.</th> 
                    <th data-class="expand">Faculty</th>   
                    <th>Semester</th> 
                    <th>Status</th>
                    <th class="edit">Manage</th> 
                </tr> 
            </thead> 
            <tbody> 
            <?php $sno = 1;?>
               <?php foreach($query->result() as $row){?>
            
                <tr> 
                	<td class="checkbox-column"><?php echo $sno; $sno++;?></td> 
                    <td><?php echo $row->department_id;?></td>  
                    <td><?php echo $row->semester_id; ?></td>
                    <td><?php echo $row->status;?></td>
					<td class="edit">
                        <a href="<?php echo base_url()?>admin/routine/create/<?php echo $row->id;?>"><i class="icon-pencil"></i></a>                   
                        &nbsp;&nbsp;/&nbsp;&nbsp; 
                        <a href="<?php echo base_url()?>admin/routine/delete/<?php echo $row->id;?>" onclick="return confirm('Are you sure, you want to delete it?');"><i class="icon-trash"></i></a>
					</td> 
                </tr> 
                
				<?php }	?>                
            </tbody> 
        </table> 
    </div>

</div><!--end of class="widget box"-->