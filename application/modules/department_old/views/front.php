<?php $base_url=  base_url();?> 
<div class="row">
    <div class="col-md-12">
	<div class="col-md-7">
        <div class="widget box"> 
            <div class="widget-header"> 
            	
            </div> 
            <div class="widget-content">
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
					echo form_open_multipart('admin/pages/submit', 'class="form-horizontal row-border" id="validate-1"');				
				?>                

                    
                    <div class="form-group"> 

                        <div class="col-md-10" id="faculty_description">
                        	<?php 
                                foreach($query as $row)
                                {
                                   echo '<h4><i class="icon-reorder"></i>'.$row->name.'</h4>';
                                    
                                    echo '<p>'.$row->description.'</p>';
                                }
                                ?>
                        </div> 
                        
                            
                       
                       <div class="name" id="name_div">
                       </div>
                       <div class="page_description" id="image_div" style="display:none ;" >
                           <img style="width:100px; height:150;" class="img-responsive" id="page_image">
                       </div>
                       
                       <div class="page_description" id="description_div">
                       </div>

                         
					</div>
             
                    
                  

            </div> 
        </div> 
    </div>
    <div class="col-md-5">
        <div class="col-md-12">

            <div class="panel light-style">
                <h3>Mission Statement</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo, porro, molestiae! Dolorum illum cum assumenda perferendis, iusto sapiente et! Suscipit aliquid harum culpa odio, doloribus. Nulla asperiores voluptatem incidunt cumque?</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quisquam magnam nisi, rerum inventore culpa. Aut maiores expedita debitis, soluta voluptate inventore quis cupiditate nemo veritatis ratione ipsam tempora, vero.</p>
                <a href="#">Read More</a>
            </div>
            <div class="panel dark-style">
                <h3>Faculty Members</h3>
                <ul class="side-list">
                    <?php
                            foreach ($faculty_member->result() as $row) {?>
                    <li ><a href="javascript:;" onclick="ajaxFacultyMember()" target="_blank" id="<?php echo $row->slug;?>"><?php echo $row->name;?></a></li>
                            <?php }
                    ?>
                    
                </ul>
            </div>
        </div>
    </div>
        <?php echo form_close(); ?>
    </div>
</div>
                <script>
                function ajaxFacultyMember() {
                     
                    var slug = event.target.id;
                    var base_url='<?php echo $base_url;?>'.concat('department/get_faculty_members_details');
                    
                    $.ajax({
                        type:'POST',
                        url:base_url,
                        data:"slug=" + slug,
                        success:function(data) 
                        {  
                           $.each($.parseJSON(data), function(k, v) {
                               $('#faculty_description').hide();
                               $('#name_div').html(v.name+' ('+v.post+')').show();
                               $('#image_div').show();
                           $('#image_div').attr("style", '');
                         $('#page_image').attr("src", '<?php echo $base_url;?>' +"uploads/faculty_members/" + v.attachment);
                          $('#description_div').html(v.description).show();
                          $("html, body").animate({ scrollTop: 0 }, "slow");
                  return false;
                });
                        }
                    });
                }
                </script>