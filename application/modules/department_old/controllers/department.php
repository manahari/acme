<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class department extends MX_Controller
{

	function __construct() {
		$this->load->model('mdl_department');
		parent::__construct();
	}


	function index(){
            
		$department= $this->mdl_department->get_front('id');
		$data['department'] = $department;
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);
	}
	
	function departmentdetail()
	{
		$department_slug = $this->uri->segment(3);
                $department_list=$this->get_department_id($department_slug);
                foreach ($department_list as $row)
                {
                    $department_id=$row->id;
                }
                $data['faculty_member']=$this->get_faculty_members($department_id);
		$data['query']= $this->mdl_department->get_where_slug($department_slug);
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);
	}
        function get_department_id($department_slug)
        {
            $this->load->model('mdl_department');
            $query = $this->mdl_department->get_department_id($department_slug);
            return $query; 
            
        }
        function get_faculty_members($department_id)
        {
            $this->load->model('faculty_members/mdl_faculty_members');
            $query = $this->mdl_faculty_members->get_faculty_members($department_id);
            return $query; 
            
        }
        function get_faculty_members_details()
        {
            $slug= $this->input->post('slug', TRUE);
            $this->load->model('faculty_members/mdl_faculty_members');
            $query = $this->mdl_faculty_members->get_faculty_members_details($slug);
             echo json_encode( $query->result());
            
        }
}