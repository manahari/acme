<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Acme Engineering College</title>
    <link href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>design/admin/css/main.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>design/admin/css/plugins.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>design/admin/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>design/admin/css/icons.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>design/admin/css/login.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo base_url();?>design/admin/css/fontawesome/font-awesome.min.css" rel="stylesheet">
    <!--[if IE 7]><link rel="stylesheet" href="<?php echo base_url();?>design/admin/css/fontawesome/font-awesome-ie7.min.css"><![endif]-->
    <!--[if IE 8]><link href="<?php echo base_url();?>design/admin/css/ie8.css" rel="stylesheet" type="text/css"/><![endif]-->
<!--    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>-->
    <script type="text/javascript" src="<?php echo base_url();?>design/admin/js/libs/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>design/admin/js/libs/lodash.compat.min.js"></script>
    <!--[if lt IE 9]><script src="<?php echo base_url();?>design/admin/js/libs/html5shiv.js"></script><![endif]-->
    <script type="text/javascript" src="<?php echo base_url();?>plugins/uniform/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>plugins/validation/jquery.validate.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>plugins/nprogress/nprogress.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>design/admin/js/login.js"></script>
    <script>$(document).ready(function(){Login.init()});</script>
    <style>
        .error{
            text-align: center;
            color:red;
            font-size: 12px;
        }
    </style>
</head>
<body bgcolor="red" class="login">
<div class="logo">
    <img src="<?php echo base_url();?>design/frontend/img/images/logo.png" alt="logo"/>
</div>
<div class="box">
    <div class="content">
        <?php // load the form helper so you can use set_value, form_open etc
        $this->load->helper('form');
        echo form_open('users_student/submit');
        ?>
        <h3 class="form-title">Student Login</h3>
        <div class="alert fade in alert-danger" style="display: none;">
            <i class="icon-remove close" data-dismiss="alert"></i>
            Enter any username and password.
        </div>
        <div class="form-group">
            <div class="input-icon">
                <i class="icon-user"></i>
                <?php echo form_input('username', '', 'class="form-control" placeholder="Username" autofocus data-rule-required="true" data-msg-required="Please enter your username."'); ?>
            </div>
            <?php echo form_error('username', '<div class="error">', '</div>');
            if(!form_error('username')){
                echo '<div style="padding-top:13px;"></div>';
            }
            ?>
        </div>
        <div class="form-group">
            <div class="input-icon">
                <i class="icon-lock"></i>
                <?php echo form_password('pword', '', 'class="form-control" placeholder="Password" data-rule-required="true" data-msg-required="Please enter your password."'); ?>
            </div>
            <?php echo form_error('pword', '<div class="error">', '</div>');
            if(!form_error('pword')){
                echo '<div style="padding-top:13px !important;">&nbsp;</div>';
            }
            ?>
        </div>


        <?php
        if ($this->session->userdata('login_error')==1)
        {
            echo '<div class="error">Incorrect username or password</div>';

            $this->session->unset_userdata('login_error');/*to unset login_error if there is login error found*/	}
        else{
            echo '<br/>';
        }
        ?>

        <div class="form-actions">
            <?php
            $style = 'id = "btn" style = "background-image:url('.base_url().'design/admin/img/login-btn.png); width:103px; height:42px; margin-left:30%;" ';
            echo form_submit('submit', '', $style);
            ?>
        </div>
        <?php echo form_close();?>
    </div>

    <div class="inner-box">
        <div class="content">
            <i class="icon-remove close hide-default"></i>
            <a href="#" class="forgot-password-link">Forgot Password?</a>
            <!--form class="form-vertical forgot-password-form hide-default" action="login.html" method="post">
                <div class="form-group">
                    <div class="input-icon">
                        <i class="icon-envelope"></i>
                        <input type="text" name="email" class="form-control" placeholder="Enter email address" data-rule-required="true" data-rule-email="true" data-msg-required="Please enter your email."/>
                    </div>
                </div>
                <button type="submit" class="submit btn btn-default btn-block">Reset your Password </button>
            </form>
            <div class="forgot-password-done hide-default">
                <i class="icon-ok success-icon"></i>
                <span>Great. We have sent you an email.</span>
            </div-->

        </div>
    </div>
</div>
</body>
</html>