<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i> Add Users</h4> 
            </div> 
            <div class="widget-content">
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
					echo form_open_multipart('admin/users_student/submit', 'class="form-horizontal row-border" id="validate-1"');
				?>                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Username <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('username', $username, 'class="form-control required"');?>
						</div> 
                    </div>

                <div class="form-group">
                    <label class="col-md-2 control-label">Select Group</label>
                    <div class="col-md-10">
                        <?php $selected = $group_id;$options = array(
                            '1'  => 'Student',
                            '2'    => 'Teacher',
                        );
                        echo form_dropdown('group_id', $options, $selected,'class="form-control"');?>
                    </div>
                </div>
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Password <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php if($update_id){
								echo anchor('admin/users_student/chpwd/'.$update_id, 'Change Password');
							}else {?>
									<?php echo form_password('password', $password, 'class="form-control required"');?>
							<?php }?>                        	
						</div> 
                    </div>


                <div class="form-actions">
						<?php 							
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
							if (!empty($update_id)){echo form_hidden('update_id',$update_id);}	
						?>
					</div>                 
                    
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>