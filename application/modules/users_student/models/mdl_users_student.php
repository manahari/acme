<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_users_student extends CI_Model {
	
	function __construct() {
	parent::__construct();
	}
	
	function get_table() {
	$table = "up_users_student";
	return $table;
	}
	
	
	function get($order_by){
	$table = $this->get_table();	
	$this->db->select('up_users_student.*');
	//$this->db->select('up_users_student_groups.name as group_name');
	//$this->db->join('up_users_student_groups', 'up_users_student_groups.id = up_users_student.group_id');
	$this->db->order_by($order_by);
	$query=$this->db->get($table);
	return $query;
	}
	
	
	function get_username_from_user_id($user_id){
	$table = $this->get_table();
	$this->db->select('display_name');
	$this->db->where('id',$user_id);
	$query=$this->db->get($table)->result();
	return $query[0]->display_name;
	}
	
	
	function get_where($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$query=$this->db->get($table);
	return $query;
	}
	
	function _insert($data){
	$table = $this->get_table();
	$this->db->insert($table, $data);
	}
	
	function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}
	
	function _delete($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->delete($table);
	}
	
	function chpwd_update($id, $chpwd){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $chpwd);
	}
        function get_users_student_dropdown()
	{
		$this->db->select('id, display_name');
		$this->db->order_by('id','DESC');
		$dropdowns = $this->db->get('up_users_student')->result();
		foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->display_name;
		}
		if(empty($dropdownlist)){return NULL;}
		$finaldropdown = $dropdownlist;
		return $finaldropdown;
	}
    function check_login($username, $password)
    {
        $hashed_pword = $this->enc_hash_pwd($password);
        $query_str = "SELECT id,group_id FROM up_users_student WHERE ( username = '$username') and password = '$hashed_pword'";

        $result = $this->db->query($query_str, array($username, $hashed_pword));

        if($result->num_rows()==1)
        {
            return $result->row(0);
        }
        else
        {
            return false;
        }

    }
    function enc_hash_pwd($pword){	//this function is to generate a hashed pwd
        $hashed_pwd = hash('sha512', $pword . config_item('encryption_key'));
        return $hashed_pwd;
    }
	
}