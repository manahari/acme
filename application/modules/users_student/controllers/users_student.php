<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_student extends MX_Controller
{

    function __construct() {
        parent::__construct();
    }

    function index()
    {
        $this->check_session();
    }

    function login(){
        $data['view_file'] = "loginform";
        $this->load->module('template');
        $this->template->studentlogin($data);
    }
            function login_ajax(){
            extract($_POST);
            $this->load->model('mdl_users_student');
            $ids = $this->mdl_users_student->check_login($username, $password);
//                var_dump($ids);die;
            if(empty($ids)){
                $user_id = $group_id = '';
            }
            else{
                $user_id = $ids->id;
                $group_id=$ids->group_id;
            }

            if(!$user_id )
            {
                $this->session->set_userdata('login_error', TRUE);
                 echo 'login_fail';
            }
            else
            {
                $this->session->set_userdata(array(
                    'logged_in' => TRUE,
                    'student_id' => $user_id,
                    'student_group_id' => $group_id,
                ));
                echo 'login_success';
    }
        }


    function submit()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'required|max_length[100]|xss_clean');
        $this->form_validation->set_rules('pword', 'Password', 'required|max_length[100]|xss_clean');

        if ($this->form_validation->run($this) == FALSE)
        {
            redirect('home');
        }
        else
        {
            extract($_POST);
            $this->load->model('mdl_users_student');
            $ids = $this->mdl_users_student->check_login($username, $pword);
//            var_dump($ids); die;
            if(empty($ids)){
                $user_id = $group_id = '';
            }
            else{
                $user_id = $ids->id;
                $group_id=$ids->group_id;

            }

            if(!$user_id )
            {
                $this->session->set_userdata('login_error', TRUE);
                redirect('home');
            }
            else
            {
                $this->session->set_userdata(array(
                    'logged_in' => TRUE,
                    'student_id' => $user_id,
                    'student_group_id' => $group_id,
                    'permission' => $access
                ));


                $this->session->unset_userdata('login_error');
                redirect($this->session->userdata('current_url'));

            }
        }
    }

    function logout()
    {

        $this->session->unset_userdata('logged_in');
        $this->session->unset_userdata('student_id');
        $this->session->unset_userdata('student_group_id');
        redirect($this->session->userdata('current_url'));

    }

    function check_session()
    {
        if($this->session->userdata('logged_in'))/*to check session and make sure that it has only admin privilage*/
        {
            redirect('admin/dashboard');
        }
        else
        {
            $this->login();
        }
    }


    function check_session_and_permission($modulename)
    {
        $group_id = $this->session->userdata("group_id");
        $access = $this->get_all_permissions($group_id);

        if($this->session->userdata("logged_in")){

            if($access == 'granted'){
            }
            elseif($access == 'denied'){
                redirect('admin');
            }
            else{
                $moduleid = $this->get_id_from_modulename($modulename);
                if(!isset($access[$moduleid])){
                    redirect('admin/dashboard');
                }
            }

            //redirect('admin/dashboard');
        }
    }


    function get_all_permissions($group_id){

        /*checkin permission*/
        if($group_id != 1){ //for non-admin
            $permission_status = $this->check_permission_existence_other_than_admin($group_id);//finish this

            if($permission_status == TRUE){//finish this
                $access = $this->unserialize_role_array($group_id);	//finish this
            }else {
                $access = 'denied';
            }//finish this
        }
        else //for admin
        {
            $access = 'granted';
        }

        return $access;
    }

    function get_id_from_modulename($modulename){
        $this->load->model('modules/mdl_moduleslist');
        $query = $this->mdl_moduleslist->get_id_from_modulename($modulename);
        return $query;
    }

    function check_permission_existence_other_than_admin($group_id){
        $this->load->model('permissions/mdl_permissions');
        $query = $this->mdl_permissions->check_permission_existence($group_id);
        return $query;
    }

    function unserialize_role_array($group_id){
        $this->load->model('permissions/mdl_permissions');
        $array = $this->mdl_permissions->unserialize_role_array($group_id);
        return $array;
    }



    function get($order_by){
        $this->load->model('mdl_users_student');
        $query = $this->mdl_users_student->get($order_by);
        return $query;
    }

    function get_with_limit($limit, $offset, $order_by) {
        $this->load->model('mdl_users_student');
        $query = $this->mdl_users_student->get_with_limit($limit, $offset, $order_by);
        return $query;
    }

    function get_where($id){
        $this->load->model('mdl_users_student');
        $query = $this->mdl_users_student->get_where($id);
        return $query;
    }

    function get_where_custom($col, $value) {
        $this->load->model('mdl_users_student');
        $query = $this->mdl_users_student->get_where_custom($col, $value);
        return $query;
    }

    function _insert($data){
        $this->load->model('mdl_users_student');
        $this->mdl_users_student->_insert($data);
    }

    function _update($id, $data){
        $this->load->model('mdl_users_student');
        $this->mdl_users_student->_update($id, $data);
    }

    function _delete($id){
        $this->load->model('mdl_users_student');
        $this->mdl_users_student->_delete($id);
    }

    function count_where($column, $value) {
        $this->load->model('mdl_users_student');
        $count = $this->mdl_users_student->count_where($column, $value);
        return $count;
    }

    function get_max() {
        $this->load->model('mdl_users_student');
        $max_id = $this->mdl_users_student->get_max();
        return $max_id;
    }

    function _custom_query($mysql_query) {
        $this->load->model('mdl_users_student');
        $query = $this->mdl_users_student->_custom_query($mysql_query);
        return $query;
    }

}