<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_faculty_members extends CI_Model {

	function __construct() {
	parent::__construct();
	}
        function get_table() {
	$table = "up_faculty_members";
	return $table;
	}
	
	function get_front($order_by){
	$table = $this->get_table();
	$this->db->select('up_photos.name as photos, up_photos.attachment as attachment');
	$this->db->select('up_faculty_members.*');
	$this->db->join('up_photos', 'up_faculty_members.id = up_photos.faculty_members_id');
	$this->db->group_by('up_faculty_members.id');
	$query=$this->db->get($table)->result();
	return $query;
	}
	 
        
       
	function get($order_by){
	$table = $this->get_table();
	$this->db->order_by($order_by,'ASC');
	$query=$this->db->get($table);
	return $query;
	}
	
	function get_where_slug($slug){
	$table = $this->get_table();
	$this->db->where('slug', $slug);
        $this->db->where('status', 'live');
	$query=$this->db->get($table);
	return $query->result();
      
	}
        function get_faculty_members($id){
	$table = $this->get_table();
	$this->db->where('department_id', $id);
        $this->db->where('status', 'live');
        $this->db->order_by('post_order','ASC');
	$query=$this->db->get($table);
	return $query;
      
	}
        function get_faculty_members_details($slug){
	$table = $this->get_table();
	$this->db->where('slug', $slug);
	$query=$this->db->get($table);
	return $query;
      
	}
                function get_where($slug){
	$table = $this->get_table();
	$this->db->where('id', $slug);
//        $this->db->where('status', 'live');
	$query=$this->db->get($table);
	return $query;
      
	}
	
	function _insert($data){
	$next_id = $this->get_id();
		
	$table = $this->get_table();
	$this->db->insert($table, $data);
	}
	
	function get_id(){
	$result = mysql_query("SHOW TABLE STATUS LIKE 'up_faculty_members'");
	$row = mysql_fetch_array($result);
	$nextId = $row['Auto_increment']; 
	return $nextId;
	}
	
	function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}
	function _delete($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->delete($table);
	}
	
	function get_modules_dropdown()
	{
	$this->db->select('id, name');	
	$this->db->order_by('name');
	$dropdowns = $this->db->get('up_wo_status')->result();
	foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->name;
		}
	if(empty($dropdownlist)){return NULL;}
	$finaldropdown = $dropdownlist;
	return $finaldropdown;
	}
	
	function get_groups_dropdown()
	{
		$this->db->select('id, name');
                $this->db->where('status', 'live');
		$this->db->order_by('id','DESC');
		$dropdowns = $this->db->get('up_faculty_members')->result();
		foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->name;
		}
		if(empty($dropdownlist)){return NULL;}
		$finaldropdown = $dropdownlist;
		return $finaldropdown;
	}
         


}