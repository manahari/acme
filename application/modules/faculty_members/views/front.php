<div class="row">
    <div class="col-md-12">
	<div class="col-md-7">
        <div class="widget box"> 
            <div class="widget-header"> 
            	
            </div> 
            <div class="widget-content">
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
					echo form_open_multipart('admin/pages/submit', 'class="form-horizontal row-border" id="validate-1"');				
				?>                

                    
                    <div class="form-group"> 

                        <div class="col-md-10">
                        	<?php 
                                foreach($query as $row)
                                {
                                   echo '<h4><i class="icon-reorder"></i>'.$row->name.'</h4>';
                                    
                                    echo '<p>'.$row->description.'</p>';
                                }
                                ?>
                        </div> 
					</div>
             
                    
                  

            </div> 
        </div> 
    </div>
    <div class="col-md-5">
        <div class="col-md-12">

            <div class="panel light-style">
                <h3>Mission Statement</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo, porro, molestiae! Dolorum illum cum assumenda perferendis, iusto sapiente et! Suscipit aliquid harum culpa odio, doloribus. Nulla asperiores voluptatem incidunt cumque?</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo quisquam magnam nisi, rerum inventore culpa. Aut maiores expedita debitis, soluta voluptate inventore quis cupiditate nemo veritatis ratione ipsam tempora, vero.</p>
                <a href="#">Read More</a>
            </div>
            <div class="panel dark-style">
                <h3>Faculty Members</h3>
                <ul class="side-list">
                    <li><a href="#">Sarad Poudel (HOD)</a></li>
                    <li><a href="#">Subarna Bijay Khadka (DHOD)</a></li>
                    <li><a href="#">Debebdra Bdr. Rout</a></li>
                    <li><a href="#">Roshani Ghimire</a></li>
                    <li><a href="#">Prajjwal Acharya</a></li>
                    <li><a href="#">Manahari Sijapati</a></li>
                </ul>
            </div>
        </div>
    </div>
        <?php echo form_close(); ?>
    </div>
</div>