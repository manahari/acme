<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('faculty_members'); //module name is groups here	
	}
	
	function index()
	{	
		$data['query'] = $this->get('id');
		
		$data['view_file'] = "admin/table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
	
	function get_data_from_post()
	{
                        $data['name'] = $this->input->post('name', TRUE);
                        $data['post'] = $this->input->post('post', TRUE);
                        $data['post_order'] = $this->input->post('post_order', TRUE);
                        $data['department_id'] = $this->input->post('department_id', TRUE);
                        $data['description'] = $this->input->post('description', TRUE);
                        $data['attachment'] = $this->input->post('attachment', TRUE);
                        $data['status'] = $this->input->post('status', TRUE);
                        $data['slug'] = strtolower(url_title($data['name']));
		
			$update_id = $this->input->post('update_id', TRUE);
                        if(is_numeric($update_id))
			{
                            $attach = $this->get_attachment_from_db($update_id);
                            $data['attachment'] = $attach['attachment'];
                            $data['upd_date'] = date("Y-m-d");
                               
			}
			else
			{
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
                                $data['attachment'] = $this->input->post('userfile', TRUE);
			}
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['name'] = $row->name;
                        $data['post'] = $row->post;
                        $data['post_order'] = $row->post_order;
                        $data['department_id'] = $row->department_id;
                        $data['attachment'] = $row->attachment;
                        
                       $data['description'] = $row->description;
                        $data['status'] = $row->status;
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
	
	
	function create()
	{
		$update_id = $this->uri->segment(4);
		
			$submit = $this->input->post('submit', TRUE);
	
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
		
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
			$data['department_list'] = $this->get_department_dropdown();		
			$data['view_file'] = "admin/form";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
			
		
	}
	
	function get_department_dropdown()
	{
	$this->load->model('department/mdl_department');
	$query = $this->mdl_department->get_groups_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}

	function submit()
	{		
	
	$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		if(is_numeric($update_id)){						
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
                      
		}
		else{
		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean|is_unique[up_faculty_members.name]'); //unique_validation check while creating new
		
                
                }
		/*end of validation rule*/
		
		
		if ($this->form_validation->run($this) == FALSE)
		{
			$this->create();
		}
		else
		{
			$data = $this->get_data_from_post();

            //var_dump($data); die;
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){
                            $attach = $this->get_attachment_from_db($update_id);
				$uploadattachment = $this->do_upload($update_id);
				$data['attachment'] = $uploadattachment['upload_data']['file_name'];
				if(empty($data['attachment'])){
					$data['attachment'] = $attach['attachment'];}
				$this->_update($update_id, $data);
			} else {
                            $nextid = $this->get_id();
				$uploadattachment = $this->do_upload($nextid);
				$data['attachment'] = $uploadattachment['upload_data']['file_name'];
				$this->_insert($data);
			}
			
			redirect('admin/faculty_members');
		}
			
	}
		function delete($id){
	$this->load->model('mdl_faculty_members');
	$this->mdl_faculty_members->_delete($id);
        redirect('admin/faculty_members');
	}
	

	function get($order_by){
	$this->load->model('mdl_faculty_members');
	$query = $this->mdl_faculty_members->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_faculty_members');
	$query = $this->mdl_faculty_members->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_faculty_members');
	$this->mdl_faculty_members->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_faculty_members');
	$this->mdl_faculty_members->_update($id, $data);
	}
        
         function get_groups()
	{
	$this->load->model('faculty_membersfaculty_members/mdl_faculty_membersfaculty_members');
	$query = $this->mdl_faculty_membersfaculty_members->get_groups_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}
        function get_attachment_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{			
			$data['attachment'] = $row->attachment;				
		}
			return $data;
	}
        	function do_upload($id) 
	{ 
	   $config['upload_path']   =   "./uploads/faculty_members/"; 
	   $config['file_name'] = $id;		   
	   $config['overwrite'] = TRUE;
	   $config['allowed_types'] =   "gif|jpg|jpeg|png";  
	   $config['max_size']      =   "20480"; //that's 20MB
	   $config['max_width']     =   "1907"; 
	   $config['max_height']    =   "1280"; 

	   $this->load->library('upload',$config);
 
	   
		if ( ! $this->upload->do_upload())
		{
			//echo 'File cannot be uploaded';
			$datas = array('error' => $this->upload->display_errors());
		}
		else
		{
			echo 'File has been uploaded';
			$datas = array('upload_data' => $this->upload->data());		
			
		}
		
		return $datas;
	}
		

	function get_id(){
	$this->load->model('mdl_faculty_members');
	$id = $this->mdl_faculty_members->get_id();
	return $id;
	}
}