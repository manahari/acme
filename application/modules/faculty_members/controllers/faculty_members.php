<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class faculty_members extends MX_Controller
{

	function __construct() {
		$this->load->model('mdl_faculty_members');
		parent::__construct();
	}


	function index(){
            
		$faculty_members= $this->mdl_faculty_members->get_front('id');
		$data['faculty_members'] = $faculty_members;
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);
	}
	
	function faculty_membersdetail()
	{
		 $faculty_members_slug = $this->uri->segment(3);
		$data['query']= $this->mdl_faculty_members->get_where_slug($faculty_members_slug);
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);
	}
}