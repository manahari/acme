<div class="row">
    <div class="col-md-12">
	<div class="col-md-8">
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i><?php echo $title?></h4>
            </div> 
            <div class="widget-content">
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
					echo form_open_multipart('admin/pages/submit', 'class="form-horizontal row-border" id="validate-1"');				
				?>                

                    
                    <div class="form-group"> 

                        <div class="col-md-12">
                        	<?php echo $description?>
                        </div> 
					</div>
             
                    
                  

            </div> 
        </div> 
    </div>
    <div class="col-md-4">
        <div class="col-md-12">

            <div class="panel light-style">
                <h3>Download</h3>
                <ul class="side-list">

                    <li>
                        <a href="<?php echo base_url();?>uploads/entrance/SchalarshipAct_2.pdf" target="_blank">PU Scholarship Act</a>
                    </li>
                </ul>
            </div>
            <div class="panel dark-style">
                <h3>Academic Programs</h3>
                <ul class="side-list">
                    <li><a href="<?php echo base_url();?>civil-engineering">B.E. Civil</a></li>
                    <li><a href="<?php echo base_url();?>computer-electronics-and-communication">B.E Computer</a></li>
                    <li><a href="<?php echo base_url();?>computer-electronics-and-communication">B.E. Electronics and Communication</a></li>
                    <li><a href="<?php echo base_url();?>architecture">B.Arch</a></li>
                    <li><a href="<?php echo base_url();?>home">M.Sc. Information System Engineering</a></li>
                    <li><a href="<?php echo base_url();?>home">M.Sc. Engineering Management</a></li>
                </ul>
            </div>
        </div>
    </div>
        <?php echo form_close(); ?>
    </div>
</div>