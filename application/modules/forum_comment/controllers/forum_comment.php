<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class forum_comment extends MX_Controller
{

	function __construct() {
	parent::__construct();
	}

        function submit_comment(){
            $forum_id= $this->input->post('forum_id',TRUE);
            $title=($this->get_detail($forum_id));
            $this->load->library('form_validation');
            $this->form_validation->set_rules('name', 'Name', 'required|xss_clean');
            $this->form_validation->set_rules('comment', 'Comment', 'required|xss_clean');
            if ($this->form_validation->run($this) == FALSE)
		{
                
//                $data['comment_detail']=$this->get_comments_detail($forum_id);
                redirect(base_url()."forum/details/"."$title");
              
		}
            else
		{
                  $data = $this->get_data_from_post();
                  $this->_insert($data);
                  redirect(base_url()."forum/details/"."$title");
                }
        }
        function get_data_from_post()
	{
		$data['name'] = $this->input->post('name', TRUE);			
		$data['slug'] = strtolower(url_title($data['name']));	
		$data['comment'] = $this->input->post('comment', TRUE);	
                $data['forum_id'] = $this->input->post('forum_id',TRUE);	
		
		
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$data['upd_date'] = strtotime(date("Y-m-d"));
			}
			else
			{
				$data['ent_date'] = strtotime(date("Y-m-d"));
				$data['upd_date'] = NULL;
			}
                        
		return $data;
	}
        
        
        
        function details(){
                $slug = $this->uri->segment(3);
                $data['details']=$this->get_detail($slug);
                $data['view_file']="forum_details";
		$this->load->module('template');
		$this->template->front($data);
        }
        function get($order_by){
	$this->load->model('mdl_forum_comment');
	$query = $this->mdl_forum_comment->get($order_by);
	return $query;
	}
        
        function get_detail($forum_id){
	$this->load->model('forum/mdl_forum');
	$query = $this->mdl_forum->get_details_where($forum_id);
        foreach($query->result() as $row){
            $title = $row->slug;
        }
	return $title;
	}
        function get_comments_detail($forum_id){
        $this->load->model('mdl_forum_comment');
	$query = $this->mdl_forum_comment->get_comments($forum_id);
	return $query;  
        }
	
	function get_pages_if_live($col, $value) {
	$this->load->model('mdl_forum_comment');
	$query = $this->mdl_forum_comment->get_pages_if_live($col, $value);
	return $query;
	}

	
	function get_where_custom($col, $value) {
	$this->load->model('mdl_forum_comment');
	$query = $this->mdl_forum_comment->get_where_custom($col, $value);
	return $query;
	}
        function _insert($data){
	$this->load->model('mdl_forum_comment');
	$this->mdl_forum_comment->_insert($data);
	}

}