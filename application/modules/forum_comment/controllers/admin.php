<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('forum_comment'); //module name is pages here	
	}
	
	function index()
	{	
		$data['query'] = $this->get_detail('id');
		
		$data['view_file'] = "admin/table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
	
	function get_data_from_post()
	{
		$data['name'] = $this->input->post('name', TRUE);			
		$data['slug'] = strtolower(url_title($data['name']));	
		$data['comment'] = $this->input->post('comment', TRUE);	
//                $data['published_date'] = $this->input->post('published_date', TRUE);	
		$data['status'] = $this->input->post('status', TRUE);
		
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$data['upd_date'] = strtotime(date("Y-m-d"));
			}
			else
			{
				$data['ent_date'] = strtotime(date("Y-m-d"));
				$data['upd_date'] = NULL;
			}
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['name'] = $row->name;
			$data['comment'] = $row->comment;
                        $data['ent_date'] = $row->ent_date;	
			$data['status'] = $row->status;			
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
	
	
	function create()
	{
		$update_id = $this->uri->segment(4);
		if($update_id != 1){
			$submit = $this->input->post('submit', TRUE);
		
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
			
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
					
			$data['view_file'] = "admin/form";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
			
		}else{
		redirect('admin/forum_comment');
		}
	}

	
	function delete()
	{	$this->load->model('mdl_forum_comment');
		$delete_id = $this->uri->segment(4);							
		
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/forum_comment');
			}
		else
		{
			$this->mdl_forum_comment->_delete($delete_id);
			redirect('admin/forum_comment');
		}			
			
	}
	
	
	function submit()
	{		
	
	$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		if(is_numeric($update_id)){						
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
		}
		else{
		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //unique_validation check while creating new
		}
		/*end of validation rule*/
		
		
		if ($this->form_validation->run($this) == FALSE)
		{
			$this->create();
		}
		else
		{
			$data = $this->get_data_from_post();
			
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){				
				$this->_update($update_id, $data);
			} else {
				$this->_insert($data);
			}
			
			redirect('admin/forum_comment');
		}
			
	}
		

	function get($order_by){
	$this->load->model('mdl_forum_comment');
	$query = $this->mdl_forum_comment->get($order_by);
	return $query;
	}
        function get_detail($order_by){
	$this->load->model('mdl_forum_comment');
	$query = $this->mdl_forum_comment->get_detailof($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_forum_comment');
	$query = $this->mdl_forum_comment->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_forum_comment');
	$this->mdl_forum_comment->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_forum_comment');
	$this->mdl_forum_comment->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_forum_comment');
	$this->mdl_forum_comment->_delete($id);
	}
	
	/*
	function error404(){
	$this->load->module('template');
	$this->template->errorpage();	
	}*/
}