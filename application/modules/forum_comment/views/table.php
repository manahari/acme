<div class="col-lg-12 background-ffffff footer-top">
	<h3>Ayurveda Health Home Forum</h3>
	<div class="member_list_wrapper">
    	<ul class="publications list-group list-icon">
            <?php $sno = 1;?>
            <?php foreach($forum->result() as $row){?>
            <a href="<?php echo base_url()?>forum/details/<?php echo $row->slug;?>"><h4><?php echo $sno.". "; $sno++;?><?php echo $row->title;?></h4></a>
            <h6><strong>Published On: </strong> <?php echo $row->published_date;?></h6>
            <div><?php echo word_limiter($row->description,100);?></div>
            <div><a href="<?php echo base_url()?>forum/details/<?php echo $row->slug;?>">Read more</a></div>
            <?php }?>
            
    	</ul>
        </div>
</div>

