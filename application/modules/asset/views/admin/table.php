<div class="manage">
    <input type="button" value="Add Asset" id="create" onclick="location.href='<?php echo base_url()?>admin/asset/create';"/> 
</div>

<div class="widget box"> 

	<div class="widget-header"> 
            <h4><i class="icon-reorder"></i> Asset </h4> 
            <div class="toolbar no-padding"> 
                    <div class="btn-group"> 
                            <span class="btn btn-xs widget-collapse">
                            <i class="icon-angle-down"></i>
                            </span> 
                    </div> 
            </div> 
	</div>
    

    <div class="widget-content"> 
        <table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
            <thead> 
                <tr> 
                    <th class="checkbox-column">S.No.</th> 
                    <th data-class="expand">Name</th>  
                    <th>Manufacturer</th>
                    <th>Attachment</th>
                    <th>Purchase_Date</th> 
                    <th>Cost</th>
                    <th>User</th> 
                    <th>Department</th>
                    <th>Created_On</th>
                    <th>Status</th>
                    <th class="edit">Manage</th> 
                            
                </tr> 
            </thead> 
       <tbody> 
            <?php $sno = 1;?>
               <?php foreach($query->result() as $row){?>
            
                <tr> 
                	<td class="checkbox-column"><?php echo $sno; $sno++;?></td> 
                    <td><?php echo $row->name;?></td>
                    <td><?php echo $row->manufacturer_name;?></td>
                    <td><img src="<?php echo base_url();?>uploads/asset/<?php echo $row->attachment;?>" style="height:50px;"/></td> 
                    <td><?php echo $row->purchase_date;?></td>
                    <td><?php echo $row->cost;?></td>
                    <td><?php echo $row->user_name;?></td>
                    <td><?php echo $row->department_name;?></td>
                    
                   <td><?php echo $row->ent_date;?></td>
                    
                     
                    
                     <td><?php echo $row->status;?></td>
                    
                    <td class="edit">
                            <a href="<?php echo base_url()?>admin/asset/create/<?php echo $row->id;?>"><i class="icon-pencil"></i></a>                   
                            &nbsp;&nbsp;/&nbsp;&nbsp; 
                            <a href="<?php echo base_url()?>admin/asset/delete/<?php echo $row->id;?>" onclick="return confirm('Are you sure, you want to delete it?');"><i class="icon-trash"></i></a>
                	</td> 
                </tr> 
				<?php }	?>                
         </tbody> 
      </table> 
    </div>

</div><!--end of class="widget box