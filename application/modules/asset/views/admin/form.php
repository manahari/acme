<!--              <link rel="stylesheet" type="text/css" href="<?php echo base_url().'bootstrap/datepicker/css/datepicker.css'?>"/>
<link rel="stylesheet/less" type="text/css" href="<?php echo base_url().'bootstrap/datepicker/less/datepicker.less'?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>admindesign/css/multiselect.css" media="screen" />
<script src="<?php echo base_url().'bootstrap/datepicker/js/bootstrap-datepicker.js'?>"></script> -->

<!-- TinyMCE -->
<script language="javascript" type="text/javascript" src="<?php echo base_url();?>assets/tinyfck/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
	tinyMCE.init({
		mode : "textareas",
		theme : "advanced",
		plugins : "table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,zoom,flash,searchreplace,print,paste,directionality,fullscreen,noneditable,contextmenu",
		theme_advanced_buttons1_add_before : "save,newdocument,separator",
		theme_advanced_buttons1_add : "fontselect,fontsizeselect",
		theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,zoom,separator,forecolor,backcolor,liststyle",
		theme_advanced_buttons2_add_before: "cut,copy,paste,pastetext,pasteword,separator,search,replace,separator",
		theme_advanced_buttons3_add_before : "tablecontrols,separator",
		theme_advanced_buttons3_add : "emotions,iespell,flash,advhr,separator,print,separator,ltr,rtl,separator,fullscreen",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		plugin_insertdate_dateFormat : "%Y-%m-%d",
		plugin_insertdate_timeFormat : "%H:%M:%S",
		extended_valid_elements : "hr[class|width|size|noshade]",
		file_browser_callback : "fileBrowserCallBack",
		paste_use_dialog : false,
		theme_advanced_resizing : true,
		theme_advanced_resize_horizontal : false,
		theme_advanced_link_targets : "_something=My somthing;_something2=My somthing2;_something3=My somthing3;",
		apply_source_formatting : true
	});

	function fileBrowserCallBack(field_name, url, type, win) {
		var connector = "<?php echo base_url();?>assets/tinyfck/filemanager/browser.html?Connector=connectors/php/connector.php";
		var enableAutoTypeSelection = true;

		var cType;
		tinyfck_field = field_name;
		tinyfck = win;

		switch (type) {
			case "image":
				cType = "Image";
				break;
			case "flash":
				cType = "Flash";
				break;
			case "file":
				cType = "File";
				break;
		}

		if (enableAutoTypeSelection && cType) {
			connector += "&Type=" + cType;
		}

		window.open(connector, "tinyfck", "modal,width=600,height=400");
	}
</script>


	
	
		
		


    
<div class="row"> 
	<div class="col-md-10"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i> Add Asset</h4> 
            </div> 
            <div class="widget-content">
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
                                        $js = array('onsubmit' =>'multipleSelectOnSubmit()', 'class'=>'form-horizontal row-border', 'id'=>'validate-1');
					echo form_open_multipart('admin/asset/submit', $js);				
				?> 
                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Name <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('name', $name, 'class="form-control required"');?>
			</div> 
                    </div>
                
                <div class="clearfix"></div>
                <div class="col-md-3"><strong>Dimension</strong></div>
                <div class="col-md-9"></div>
                <div class="clearfix"></div>
                
                <div class="col-md-4">
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Length <span class="required"></span></label> 
                    	<div class="col-md-10"> 
                            <?php echo form_input('length', $length, 'class="form-control "');?>
			</div> 
                    </div>
                </div>
                
                <div class="col-md-4">
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Width <span class="required"></span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('width', $weight, 'class="form-control "');?>
			</div> 
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Height <span class="required"></span></label> 
                    	<div class="col-md-10"> 
                            <?php echo form_input('height', $height, 'class="form-control "');?>
			</div> 
                    </div>
                </div>
                <div class="clearfix"></div>
                
                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Capacity <span class="required"></span></label> 
                        <div class="col-md-10"> 
                            <?php echo form_input('capacity', $capacity, 'class="form-control required"');?>
                        </div> 
                    </div>
              
                 
               
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Weight <span class="required"></span></label> 
                        <div class="col-md-10"> 
                            <?php echo form_input('weight', $weight, 'class="form-control required "');?>
                        </div> 
                        
                    </div>
              
                
                        <div class="form-group"> 
                            <label class="col-md-2 control-label">Manufacturer <span class="required">*</span></label> 
                                <div class="col-md-10"> 
                        	<?php $selected = $name;$options = $group_array_manufacturer;
				
		 		echo form_dropdown('manufacturer', $options, $selected, 'class="form-control"');?>
                                </div> 
                        </div>
                  
                    
             
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Model_No <span class="required"></span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('model_no', $model_no, 'class="form-control required"');?>
			</div> 
                        
                    </div>
               
                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Serial_No <span class="required"></span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('serial_no', $serial_no, 'class="form-control required"');?>
			</div> 
                        
                    </div>
                
                      <div class="form-group"> 
                            <label class="col-md-2 control-label">Location <span class="required">*</span></label> 
                            <div class="col-md-10"> 
                        	<?php $selected = $name;$options = $group_array_location;
				
		 		echo form_dropdown('location', $options, $selected, 'class="form-control"');?>
                            </div> 
                       </div>
                 
                
                
                    <div class="form-group"> 
                        <label class="col-md-2 control-label">Purchase Date <span class="required">*</span></label>
                        <div class="col-md-10"> 
                            <div class="input-append date" id="datepicker" data-date="" data-date-format="yyyy-mm-dd" >
                             <?php echo form_input('purchase_date', $purchase_date, 'class=""');?>
                            <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                        </div>
                    </div>
                 
                
                       <div class="form-group"> 
                            <label class="col-md-2 control-label">Cost <span class="required"></span></label> 
                            <div class="col-md-10"> 
                        	<?php echo form_input('cost', $cost, 'class="form-control required"');?>
                            </div> 
                        
                        </div>
                  
                     <div class="form-group"> 
                            <label class="col-md-2 control-label">Life_Span <span class="required"></span></label> 
                            <div class="col-md-10"> 
                        	<?php echo form_input('life_span', $life_span, 'class="form-control required"');?>
                            </div> 
                        
                    </div>
            
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Parent <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php $selected = $parent_id;$options = $group_array_asset;
				
		 		echo form_dropdown('parent_id', $options, $selected, 'class="form-control"');?>
			</div> 
                    </div>
                
                
                    <div class="form-group"> 
                        <label class="col-md-2 control-label">Description</label> 
                        <div class="col-md-10">
                        	<?php echo form_textarea(array('id' => 'elm1', 'name' =>'description', 'value' => $description,'rows'=>'15', 'cols'=>'80', 'style'=> 'width: 100%', 'class'=> 'form-control'));?>
                        </div> 
                    </div>
                
                 <div class="form-group">
                    	<label class="col-md-2 control-label">Attachment <span class="required">*</span></label> 
                        <div class="col-md-10"> 
				<?php  if(!empty($update_id)){
				$attach_prop = array(
                                    'type' => 'file',
                                    'name' => 'userfile',
                                    'value' => $attachment
                                                    );
                                
                                                    }else{
                                                        $attach_prop = array(
                                                            'type' => 'file',
                                                            'name' => 'userfile',
                                                            'value' => $attachment,
                                                            'class' => 'required'
									);
										}
								?>
                    
                            	<?php echo form_upload($attach_prop);?>
                            <p class="help-block">
                            Images only (jpg/jpeg/gif/png)</p>
                            <label for="attachment" class="has-error help-block" generated="true" style="display:none;">
                            <?php if(!empty($update_id)){?>
                            	<img src="<?php echo base_url();?>uploads/asset/<?php echo $attachment;?>" style="height:100px;"/>
                            <?php }?>
                            </label>
                        </div>
                        
                    </div>
                         
                <div class="form-group"> 
                    	<label class="col-md-2 control-label">Web_Link <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('web_link', $web_link, 'class="form-control required"');?>
			</div> 
                 </div>
                
                <div class="form-group"> 
                    	<label class="col-md-2 control-label">User <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php $selected = $user_id;$options = $group_array;
				
		 		echo form_dropdown('user_id', $options, $selected, 'class="form-control required"');?>
			</div> 
                </div>
                
                <div class="form-group"> 
                    	<label class="col-md-2 control-label">Department <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php $selected = $department_id;$options = $group_array_department;
				
		 		echo form_dropdown('department_id', $options, $selected, 'class="form-control"');?>
			</div> 
                 </div>
               
                <div class="form-group"> 
                    	<label class="col-md-2 control-label">Spareparts <span class="required"></span></label> 
                    	<div class="col-md-10"> 
                             <?php
                             $spareparts = array(
                              'agriculture'  => 'कृषि, पशुृपालन वा फलफूल खेती ',
                              'business'    => 'व्यापार व्यवसाय   ',
                                'job'    => 'सेवा वा नोकरी ',
                                'labour'    => 'ज्याला मजदुरी  ',
                                'foreign'    => 'विदेश',
                                'other'    => 'अन्य',
                                
                            ); 
                             echo form_multiselect('fromBox', $spareparts,0,'id="fromBox"');
                             if(!$update_id){
                            $newservices = array();}
                            echo form_multiselect('toBox[]', $newservices,$this->input->post('toBox[]'),'id="toBox"'); ?>
                        </div>
                </div>
                
       
                
                <div class="form-group"> 
                    	<label class="col-md-2 control-label">Status <span class="required"></span></label> 
                    	<div class="col-md-10"> 
                            <?php
                            $selected = $status;$options = array(
                              'draft'  => 'draft',
                              'live'    => 'live',
                            ); 
                            
                            echo form_dropdown('status', $options, $selected,'class="form-control"');?>
			</div> 
                    
                 </div>
                    
                    
                 <div class="form-actions"> 
			<?php
                        echo form_submit('submit','Submit','class="btn btn-primary pull-right"','id="submit"'); //name,value...type is default submit 
			if (!empty($update_id)){echo form_hidden('update_id',$update_id);}	
			?>
                 </div>  
                
             <script>
$('#datepicker').datepicker();
$('#datepicker2').datepicker();
</script>
                <script type="text/javascript">
                
                    
                    var config = {
                      '.chosen-select'           : {},
                      '.chosen-select-deselect'  : {allow_single_deselect:true},
                      '.chosen-select-no-single' : {disable_search_threshold:10},
                      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                      '.chosen-select-width'     : {width:"95%"}
                    }
                    for (var selector in config) {
                      $(selector).chosen(config[selector]);
                    }
                </script>
                
                     <script type="text/javascript">
            createMovableOptions("fromBox","toBox",300,300,'Spareparts','Selected Spareparts');
            </script>
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>