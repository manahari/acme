<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_asset extends CI_Model {

	function __construct() {
	parent::__construct();
	}

	function get_table() {
	$table = "up_asset";
	return $table;
	} 
        
       
	function get($order_by){
	$table = $this->get_table();	
	$this->db->select('up_asset.*');
	$this->db->select('up_users.username as user_name');
        $this->db->select('up_department.name as department_name');
        $this->db->select('up_manufacturer.name as manufacturer_name');
        
	$this->db->join('up_users', 'up_users.id = up_asset.user_id');
        $this->db->join('up_department', 'up_department.id = up_asset.department_id');
        $this->db->join('up_manufacturer', 'up_manufacturer.id = up_asset.manufacturer');
       
        
	$this->db->order_by($order_by);
	$query=$this->db->get($table);
	return $query;
	}
       
       

//	function get($order_by){
//	$table = $this->get_table();
//	$this->db->order_by($order_by,'ASC');
//	$query=$this->db->get($table);
//	return $query;
//	}
	
	function get_where($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$query=$this->db->get($table);
	return $query;
	}
	
	function _insert($data){
	$next_id = $this->get_id();
		
	$table = $this->get_table();
	$this->db->insert($table, $data);
	
	
	
	}
	
	function get_id(){
	$result = mysql_query("SHOW TABLE STATUS LIKE 'up_asset'");
	$row = mysql_fetch_array($result);
	$nextId = $row['Auto_increment']; 
	return $nextId;
	}
	
	function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}
        
        function _delete($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->delete($table);
	}
	
	
	function get_modules_dropdown()
	{
	$this->db->select('id, name');	
	$this->db->order_by('name');
	$dropdowns = $this->db->get('up_asset')->result();
	foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->name;
		}
	if(empty($dropdownlist)){return NULL;}
	$finaldropdown = $dropdownlist;
	return $finaldropdown;
	}
	
	function get_groups_dropdown()
	{
		$this->db->select('id, name');
                $this->db->where('parent_id', '0');
                $this->db->where('status', 'live');
                
		$this->db->order_by('id','DESC');
		$dropdowns = $this->db->get('up_asset')->result();
                $dropdownlist[0]='';
		foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->name;
		}
		if(empty($dropdownlist)){return NULL;}
		$finaldropdown = $dropdownlist;
		return $finaldropdown;
	}

function get_equipment_strings($name){
	$table = $this->get_table();
	$this->db->where('name', $name);
	$query=$this->db->get($table);
	$string = $query->result_array();
	return $string;
	}
}