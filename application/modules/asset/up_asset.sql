-- phpMyAdmin SQL Dump
-- version 3.1.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 16, 2014 at 12:25 PM
-- Server version: 5.1.32
-- PHP Version: 5.2.9-1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `upvedacms`
--

-- --------------------------------------------------------

--
-- Table structure for table `up_asset`
--

CREATE TABLE IF NOT EXISTS `up_asset` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `width` float NOT NULL,
  `height` float NOT NULL,
  `length` float NOT NULL,
  `capacity` float NOT NULL,
  `weight` float NOT NULL,
  `manufacturer` varchar(255) NOT NULL,
  `model_no` varchar(255) NOT NULL,
  `serial_no` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `purchase_date` datetime NOT NULL,
  `cost` float NOT NULL,
  `life_span` float NOT NULL,
  `parent_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `web_link` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `spareparts_string` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `ent_date` datetime NOT NULL,
  `upd_date` datetime DEFAULT NULL,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `up_asset`
--

INSERT INTO `up_asset` (`id`, `name`, `width`, `height`, `length`, `capacity`, `weight`, `manufacturer`, `model_no`, `serial_no`, `location`, `purchase_date`, `cost`, `life_span`, `parent_id`, `description`, `attachment`, `web_link`, `user_id`, `department_id`, `spareparts_string`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'Harry', 5, 5, 5, 5, 5, 'Motorola', '54', '45', 'Kathmandu', '2014-07-16 00:00:00', 45, 0, 0, 'dsagfsfasd\r\n', '0', 'sdaffdsafsadf', 0, 0, ',2,3', 'harry', '2014-07-16 00:00:00', NULL, 'draft');
