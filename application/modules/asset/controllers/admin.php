<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('asset'); //module name is groups here	
	}
	
	function index()
	{	
		$data['query'] = $this->get('id');
		
		$data['view_file'] = "admin/table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['name'] = $row->name;
                        $data['length'] = $row->length;
                        $data['width'] = $row->width;                      
			$data['height'] = $row->height;
			$data['capacity'] = $row->capacity;
                        $data['weight'] = $row->weight;
			$data['manufacturer'] = $row->manufacturer;
			$data['model_no'] = $row->model_no;
			$data['serial_no'] = $row->serial_no;
                        $data['location'] = $row->location;
                        $data['purchase_date'] = $row->purchase_date;
                         $data['cost'] = $row->cost;
                         $data['life_span'] = $row->life_span;
                        $data['parent_id'] = $row->parent_id;
                         $data['description'] = $row->description;
                         
                         $data['attachment'] = $row->attachment;
                         
                        $data['web_link'] = $row->web_link;
                         $data['user_id'] = $row->user_id;
                         $data['department_id'] = $row->department_id;
                      
			$data['ent_date'] = $row->ent_date;
//                       $data['upd_date'] = $row->upd_date;
//                       $data['slug']=$row->row->slug;
			$data['status'] = $row->status;
                        
                        $spareparts_string = $row->spareparts_string;
                       // $doctorstring = $row->equipment_strings;
		}
	/*now converting servicestring to stringnumbers*/
			
			$singchar = explode(',',$spareparts_string);			
			$countsingchar = count($singchar);
			$newnum = array();
			for($i=0;$i<$countsingchar;$i++){					
						$newnum[$i-1] = $singchar[$i];
				}
				unset($newnum[-1]);//unsseting because we don't want this and it contains blank value
			/*end of converting servicestring to stringnumbers*/
			$spareparts_list = $this->get_spareparts();
			$countservices_list = count($spareparts_list);
			/*now converting stringnumbers to numricalnumbers*/
			$countnewnum = count($newnum);
			for($k=0;$k<$countnewnum;$k++){
					$newnum[$k] = intval($newnum[$k]); //converting into integer
					
					for($l=0;$l<$countservices_list;$l++){
						
						if($spareparts_list[$newnum[$k]]){
							
							$newservices[$newnum[$k]] = $spareparts_list[$newnum[$k]];
							
							}					
						
					}
					
				}			
			/*end of converting stringnumbers to numricalnumbers*/
			if(empty($newservices)){
				$data['newservices'] = array();
				}
				else{
				$data['newservices'] = $newservices;
				}
                      
			
			
		
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}	
        
        function get_data_from_post()
	{
		$data['name'] = $this->input->post('name', TRUE);
               
                $data['length'] = $this->input->post('length', TRUE);
                $data['width'] = $this->input->post('width', TRUE);
                $data['height'] = $this->input->post('height', TRUE);
                
                $data['capacity'] = $this->input->post('capacity', TRUE);
                $data['weight'] = $this->input->post('weight', TRUE);
                $data['manufacturer'] = $this->input->post('manufacturer', TRUE);
                $data['model_no'] = $this->input->post('model_no', TRUE);
                $data['serial_no'] = $this->input->post('serial_no', TRUE);
                $data['location'] = $this->input->post('location', TRUE);
		$data['purchase_date'] = $this->input->post('purchase_date', TRUE);	
                $data['cost'] = $this->input->post('cost', TRUE);
                $data['life_span'] = $this->input->post('life_span', TRUE);
                $data['parent_id'] = $this->input->post('parent_id', TRUE);
                $data['description'] = $this->input->post('description', TRUE);
                
                $data['web_link'] = $this->input->post('web_link', TRUE);
                $data['user_id'] = $this->input->post('user_id', TRUE);
                $data['department_id'] = $this->input->post('department_id', TRUE);
                $data['spareparts_string'] = $this->input->post('spareparts_string', TRUE);
                $data['slug'] = strtolower(url_title($data['name']));
                $data['status'] = $this->input->post('status', TRUE);
                /* to make string of services id*/
				$spareparts_id = $this->input->post('toBox', TRUE);
                             
				$spareparts_string=''; //initializing servicestring
				
				if(!empty($spareparts_id)){ 
					foreach($spareparts_id as $singserv){
						$spareparts_string = $spareparts_string.','.$singserv;	//generating servicestring				
					}			
				}
                                
				$data['spareparts_string'] = $spareparts_string;
                                
				/* end of services id*/
	
		$update_id = $this->input->post('update_id', TRUE);
                        if(is_numeric($update_id))
			{
				$attach = $this->get_attachment_from_db($update_id);
				$data['attachment'] = $attach['attachment'];
				$data['upd_date'] = date("Y-m-d");
			}
			else
			{
				$data['attachment'] = $this->input->post('userfile', TRUE);
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
			}
		return $data;
	}
	
	
	function create()
	{
		$update_id = $this->uri->segment(4);
			$submit = $this->input->post('submit', TRUE);
	
			if($submit=="Submit"){
				//person has submitted the form
				$data = $this->get_data_from_post();
			} 
			else {
				if (is_numeric($update_id)){
					$data = $this->get_data_from_db($update_id);
				}
			}
		
			if(!isset($data))
			{
				$data = $this->get_data_from_post();
			}
			
			$data['update_id'] = $update_id;
                        
                       
                        
//                        
//			$data['group_array'] = $this->get_groups();
//                        $data['group_array_asset'] = $this->get_groups_asset();
//                        $data['group_array_department'] = $this->get_groups_department();
//                         $data['group_array_location'] = $this->get_groups_location();
//                        $data['group_array_manufacturer'] = $this->get_groups_manufacturer();
                        //$data['group_array5'] = $this->get_groups5();
			$data['view_file'] = "admin/form";
                       
//                        $data['spareparts'] = $this->get_spareparts();

			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
			
		
	}
	
	
	function submit()
	{	
        
	$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		if(is_numeric($update_id)){
                    
                   
            
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean');
                        
                        $this->form_validation->set_rules('manufacturer', 'Manufacturer', 'required|xss_clean');
                        
                        $this->form_validation->set_rules('model_no', 'Model No', 'required|xss_clean');
                        $this->form_validation->set_rules('serial_no', 'Serial No', 'required|xss_clean');
                        $this->form_validation->set_rules('cost', 'Cost', 'required|xss_clean');
                        $this->form_validation->set_rules('life_span', 'Life Span', 'required|xss_clean');
                       
                        
                        $this->form_validation->set_rules('description', 'Description', 'required|xss_clean');
//                      
                        
			;;//we don't want unique_validation error while editing
		}
		else{
		
                    $this->form_validation->set_rules('name', 'Name', 'required|xss_clean]');
                       $this->form_validation->set_rules('manufacturer', 'Manufacturer', 'required|xss_clean]');
               
                    $this->form_validation->set_rules('model_no', 'Model No', 'required|xss_clean]');
                     $this->form_validation->set_rules('serial_no', 'Serial No', 'required|xss_clean]');
                      $this->form_validation->set_rules('cost', 'Cost', 'required|xss_clean]');
                      
                       $this->form_validation->set_rules('life_span', 'Life Span', 'required|xss_clean]');
                    $this->form_validation->set_rules('description', 'Description', 'required|xss_clean]');
                    
                   
                    
                    
                    //////unique_validation check while creating new
		}
		/*end of validation rule*/
		
		
		if ($this->form_validation->run($this) == FALSE)
		{
			$this->create();
		}
		else
		{
			$data = $this->get_data_from_post();
			
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){				
					
				$attach = $this->get_attachment_from_db($update_id);
				$uploadattachment = $this->do_upload($update_id);
				$data['attachment'] = $uploadattachment['upload_data']['file_name'];
				if(empty($data['attachment'])){
					$data['attachment'] = $attach['attachment'];}
				
				$this->_update($update_id, $data);
			}  else {								
					
				$nextid = $this->get_id();
				$uploadattachment = $this->do_upload($nextid);
				$data['attachment'] = $uploadattachment['upload_data']['file_name'];
				
				$this->_insert($data);	
			}
			
			redirect('admin/asset');
		}
			
	}
        
		function do_upload($id) 
	{ 
	   $config['upload_path']   =   "./uploads/asset/"; 
	   $config['file_name'] = $id;		   
	   $config['overwrite'] = TRUE;
	   $config['allowed_types'] =   "gif|jpg|jpeg|png";  
	   $config['max_size']      =   "20480"; //that's 20MB
	   $config['max_width']     =   "1907"; 
	   $config['max_height']    =   "1280"; 

	   $this->load->library('upload',$config);
 
	   
		if ( ! $this->upload->do_upload())
		{
			//echo 'File cannot be uploaded';
			$datas = array('error' => $this->upload->display_errors());
		}
		else
		{
			echo 'File has been uploaded';
			$datas = array('upload_data' => $this->upload->data());		
			
		}
		
		return $datas;
	}

	function get($order_by){
	$this->load->model('mdl_asset');
	$query = $this->mdl_asset->get($order_by);
	return $query;
	}
	
        function get_groups()
	{
	$this->load->model('users/mdl_users');
	$query = $this->mdl_users->get_groups_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}
        
        function get_attachment_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{			
			$data['attachment'] = $row->attachment;				
		}
			return $data;
	}
        function get_groups_department()
	{
	$this->load->model('department/mdl_department');
	$query = $this->mdl_department->get_groups_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}
        function get_groups_asset()
	{
	$this->load->model('mdl_asset');
	$query = $this->mdl_asset->get_groups_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}
        function get_id(){
	$this->load->model('mdl_asset');
	$id = $this->mdl_asset->get_id();
	return $id;
	}
        function get_groups_manufacturer()
	{
	$this->load->model('manufacturer/mdl_manufacturer');
	$query = $this->mdl_manufacturer->get_groups_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}
        function get_groups_location()
	{
	$this->load->model('location/mdl_location');
	$query = $this->mdl_location->get_groups_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}
        function get_groups5()
	{
	$this->load->model('spareparts/mdl_spareparts');
	$query = $this->mdl_spareparts->get_groups_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}
	function get_where($id){
	$this->load->model('mdl_asset');
	$query = $this->mdl_asset->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_asset');
	$this->mdl_asset->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_asset');
	$this->mdl_asset->_update($id, $data);
	}
        
        function delete()
	{	$this->load->model('mdl_asset');
		$delete_id = $this->uri->segment(4);							
		
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/asset');
			}
		else
		{
			$this->mdl_asset->_delete($delete_id);
			redirect('admin/asset');
		}			
			
	}
        
         function get_spareparts()
	{
		$this->load->model('spareparts/mdl_spareparts');
		$query = $this->mdl_spareparts->get_spareparts_dropdown();
		return $query;
	}
          function get_equipments()
	{
		$this->load->model('equipment/mdl_equipment');
		$query = $this->mdl_equipment->get_equipment_dropdown();
		return $query;
	}
        function get_equipment_strings($name){
	$this->load->model('mdl_asset');
	$query = $this->mdl_asset->get_equipment_strings($name);
		foreach($query as $queryresult){
			$result = $queryresult['equipment_strings'];
		}
		if(empty($query)){return NULL;}
	return $result;
	}
        function get_live_equipment()
	{
		$this->load->model('doctor/mdl_doctor');
		$query = $this->mdl_doctor->get_live_doctor_dropdown();
		return $query;
	}
}