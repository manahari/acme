<?php    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('contactus'); //module name is groups here	
	}
	
		
	function index()
	{	
		$data['query'] = $this->get('id');
		
		$data['view_file'] = "admin/table";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
	
	function get_data_from_post()
	{
		
		$data['name'] = $this->input->post('name', TRUE);
                $data['slug'] = strtolower(url_title($data['name']));
		$data['status'] = $this->input->post('status', TRUE);	
		
		$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$data['upd_date'] = date("Y-m-d H:i:s");
			}
			else
			{
				$data['ent_date'] = date("Y-m-d H:i:s");
				//$data['upd_date'] = NULL;
			}
		
		
			
			
		return $data;
	}
		
		
	function get_data_from_db($update_id)
	{      
		$query = $this->get_where($update_id);
		foreach($query as $row)
		{			
			
			
			$data['name'] = $row->name;
                        $data['slug'] = $row->name;
			$data['status'] = $row->status;
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
	
	
	function create()
	{
		$update_id = $this->uri->segment(4);
		$submit = $this->input->post('submit', TRUE);
	
		if($submit=="Submit"){
			//person has submitted the form
			$data = $this->get_data_from_post();
		} 
		else {
			if (is_numeric($update_id)){
				$data = $this->get_data_from_db($update_id);
			}
		}
		
		if(!isset($data))
		{
			$data = $this->get_data_from_post();
		}
		
		$data['update_id'] = $update_id;
	//	$data['name_array'] = $this->get_membercategory();////////////////
				
		$data['view_file'] = "admin/form";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);
	}

	
	function delete()
	{	$this->load->model('mdl_contactus');
		$delete_id = $this->uri->segment(4);							
		
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/contactus');
			}
		else
		{
			$this->mdl_contactus->_delete($delete_id);
			redirect('admin/contactus');
		}			
			
	}	
	
	
	function submit()
	{		
	
		$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		
		if(is_numeric($update_id)){						
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
			
		}
		else{
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
			
		}
		/*end of validation rule*/
		
		
		
		if ($this->form_validation->run($this) == FALSE)
		{
			$this->create();
		}
		else
		{
				$data = $this->get_data_from_post();
				$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){				
				$this->_update($update_id, $data);
			} else {
				$this->_insert($data);
			}
			
			
			
			redirect('admin/contactus');
		}
			
	}	
	
	
					
	function chpwd()
	{
		$update_id = $this->uri->segment(4);
		if(isset($update_id) && is_numeric($update_id))
			{
				$chpwd_submit = $this->input->post('submit', TRUE);
						
				if($chpwd_submit=="Submit")
					{
					//person has submitted the form
					$chpwd = $this->get_chpwd_from_post();
					} 
						
				if(!isset($chpwd))
					{
					$chpwd = $this->get_data_from_post();
					}
						
				$chdata['update_id'] = $update_id;										
				$chdata['password'] = $chpwd;
				
				
				$chdata['view_file'] = "admin/chpwd";
				$this->load->module('template/admin_template');
				$this->admin_template->admin($chdata);	
				
			}
		else
			{
				redirect('admin/contactus');
			}	
	}


	function chpwd_submit()
	{
				
	$this->load->library('form_validation');

	$this->form_validation->set_rules('chpwd', 'New Password', 'required|xss_clean');

		if ($this->form_validation->run($this) == FALSE)
		{
			$this->chpwd();
		}
		else
		{
			$chdata['password'] = $this->get_chpwd_from_post();
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){		
			
				$pwd = $chdata['password'];
				$chdata['password'] = $this->zenareta_pasaoro($pwd);
				$this->chpwd_update($update_id, $chdata);
			} else {
				redirect('admin/membercategory');
			}
			
			redirect('admin/contactus');
		}
			
	}
		
		
	function get_chpwd_from_post()
	{
		$chpwd = $this->input->post('chpwd', TRUE);
		return $chpwd;
	}

	
	
	
	
	function get($order_by){
	$this->load->model('mdl_contactus');
	$query = $this->mdl_contactus->get($order_by);
	return $query;
	}
	
	function get_membercategory()
	{
	$this->load->model('mdl_contactus');
	$query = $this->mdl_contactus->get_membercategory_dropdown();
	if(empty($query)){return NULL;}
	return $query;
	}
	
	function get_where($id){
            
	$this->load->model('mdl_contactus');
	$query = $this->mdl_contactus->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_contactus');
	$this->mdl_contactus->_insert($data);
	
	
	}

	function _update($id, $data){
	$this->load->model('mdl_contactus');
	$this->mdl_contactus->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_contactus');
	$this->mdl_contactus->_delete($id);
	}
	
	function chpwd_update($id, $chpwd){
	$this->load->model('mdl_contactus');
	$this->mdl_contactus->chpwd_update($id, $chpwd);
	}
	
	function zenareta_pasaoro($pasaoro){		
	$this->load->model('admin_login/mdl_admin_login');
	$query = $this->mdl_admin_login->enc_hash_pwd($pasaoro);
	return $query;		
	}
	
	function sidebar($data){
	echo 'syo';
	}
	
}