<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class contactus extends MX_Controller
{

	function __construct() {
	$this->load->model('contactus/mdl_contactus');
	parent::__construct();
	}

	function index(){
		$data['view_file']="front";
		$this->load->module('template');
		$this->template->front($data);	
	}
    function index2(){
        $this->load->view('front');
    }
        function get_data_from_post()
	{
		$data['name'] = $this->input->post('name', TRUE);			
		$data['slug'] = strtolower(url_title($data['name']));	
		$data['email'] = $this->input->post('email', TRUE);	
		$data['mailsubject'] = $this->input->post('subject', TRUE);
                $data['mailbody'] = $this->input->post('emailbody', TRUE);
		$data['ent_date'] = date("Y-m-d");
		$data['upd_date'] = NULL;
		return $data;
	}
        
        function submit()
	{		
	
	$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //unique_validation check while creating new
		$this->form_validation->set_rules('email', 'Email', 'required|xss_clean');
                $this->form_validation->set_rules('subject', 'Subject', 'required|xss_clean');
                $this->form_validation->set_rules('emailbody', 'Message', 'required|xss_clean');
		/*end of validation rule*/
		
		
		if ($this->form_validation->run($this) == FALSE)
		{
			redirect(base_url().'contactus');
		}
		else
		{
			$data = $this->get_data_from_post();
//            $this->send_email($data);
                        $this->_insert($data);
                        redirect(base_url().'contactus');
		}
			
			redirect($this->index());
                        
        }

    function submit2()
    {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //unique_validation check while creating new
        $this->form_validation->set_rules('email', 'Email', 'required|xss_clean');
        $this->form_validation->set_rules('subject', 'Subject', 'required|xss_clean');
        $this->form_validation->set_rules('emailbody', 'Message', 'required|xss_clean');
        /*end of validation rule*/


        if ($this->form_validation->run($this) == FALSE)
        {
            redirect(base_url().'home');
        }
        else
        {
            $data = $this->get_data_from_post();
            $this->send_email($data);
            $this->_insert($data);
            redirect(base_url().'home');
        }

        redirect($this->index());

    }

    function send_email($data){
        $name = $data['name'] ;
        $email = $data['email'];
        $to = 'roshanigd@gmail.com';
        $subject = $data['mailsubject'];
        $emailbody = $data['mailbody'] ;
        //$emailbody = 'Name='.$data['name'].'<br>Email='.$data['email'].'<br>Project='.$data['project'].'<br>Volunter Duration='.$data['volunteer_duration'].'<br>Fitness Health Condition That We Should Be Aware Of<br>'.$data['fitness'].'<br>Date of Birth='.$data['dob'].'<br>What Do You Want To Take This Experience For<br>'.$data['experience'].'<br>Please Write Something About Yourself<br>'.$data['yourself'].'<br>Country='.$data['country_id'];




//        $config['protocol'] = 'sendmail';
//        $config['mailpath'] = '/usr/sbin/sendmail';
//        $config['charset'] = 'iso-8859-1';
//        $config['wordwrap'] = TRUE;
//        $config['mailtype'] = 'html';

        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://smtp.googlemail.com'; //change this
        $config['smtp_port'] = '465';
        $config['smtp_user'] = 'manahari.sijapati@gmail.com'; //change this
        $config['smtp_pass'] = 'bdr20440231'; //change this
        $config['mailtype'] = 'html';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;
        $config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from($email);
        $this->email->to($to);

//                        $this->email->cc($emailfrom);
        //$this->email->bcc('them@their-example.com');
//$this->email->subject('INSEC SAM'."'".'S:'.$msg_from_name.':status:'.date('y-m-d',$data['ent_date']));
        $this->email->subject('Acme'.':'.$subject);
        $this->email->message('<div style="color:#003366">'.$emailbody.'<br>'.$name.'<div>');

//                        $this->email->subject('INSEC SAM'."'".'S:'.$name.':status:'.date('y-m-d',$data['ent_date']));
//                        $this->email->message($emailbody.'<div style="color:red"><div>');
//


        if (!$this->email->send())
        {
            show_error($this->email->print_debugger());
        }
        else
        {
            echo "Mail sent!";
        }
    }

        function get($order_by){
	$this->load->model('mdl_contactus');
	$query = $this->mdl_contactus->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_contactus');
	$query = $this->mdl_contactus->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_contactus');
	$this->mdl_contactus->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_contactus');
	$this->mdl_contactus->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_contactus');
	$this->mdl_contactus->_delete($id);
	}
        function generation_sitemap(){
		
		$data['view_file']="sitemap";
		$this->load->module('template');
		$this->template->front($data);	
	}
	
}