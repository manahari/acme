<div class="col-lg-12">
    <div class="contact_us_form">
       <h3>Contact Us</h3>
       <p>To contact us please fill out the form below.</p>
            <?php
            echo form_open_multipart('contactus/submit');
            ?>
              <div class="form-group"> 
                <?php echo form_input('name', '', 'style = "" class="form-control" id="exampleInputEmail1" placeholder="Enter name"');?> 
              </div>
              <div class="form-group">
                <?php echo form_input('email', '', 'style = "" class="form-control" id="exampleInputEmail1" placeholder="Enter Email"');?> 
              </div>
              <div class="form-group">
                <?php echo form_input('subject', '', 'style = "" class="form-control" id="exampleInputEmail1" placeholder="Enter subject"');?> 
              </div>
              <div class="form-group">
                <?php
                   $emailbody = array(
                            'name'				=> 'emailbody',
                            'id'				=> 'emailbody',
                            'class'				=> 'form-control',
                            'placeholder'		=> 'Type the text to send'
                            );
                    echo form_textarea($emailbody);
                    ?>
              </div>
              <?php 
                echo form_submit('save','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 

                ?>
                <div class="clearfix"></div>
            <?php echo form_close() ?> 
                <hr/>
    </div>
    <div class=" contact_both">
        <div class="col-lg-12">
            <div class="contact_info">
                <h5>
                    <strong style="text-align:justify; color:#5c8d1c;">
                       Acme Engineering College
                    </strong>
                </h5>
                <p style="font-size:16px;">Sitapaila, Kathmandu</p>


                <p class="author" style="text-align: justify;">
                    <strong>
                    Tel No: 977-01-4282962, 4280445, 4670924, 4670925
                    Fax: 977-01-4282947
                    </strong>
                </p>
                <p>
                    <strong>
                    E-mail: acme@acme.edu.np
                    </strong>
                </p>

            </div>

            <div class="contact_map">
<!--                <iframe frameborder="0" height="300" width="500" marginheight="0" marginwidth="0" scrolling="no" src="http://maps.google.com/maps/ms?msa=0&msid=215847421770814160536.0004ad070aaadde6d7d7c&ie=UTF8&ll=27.697788,85.349876&spn=0,0&t=m&vpsrc=6&output=embed"></iframe><br />-->
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3829.804675199897!2d85.2835443247969!3d27.70830385935579!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1888ee9916f7%3A0x6188d2022654674!2sAcme+Engineering+College!5e0!3m2!1sen!2snp!4v1443154476866" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe><br/>
                <small>
                    <a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3829.804675199897!2d85.2835443247969!3d27.70830385935579!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb1888ee9916f7%3A0x6188d2022654674!2sAcme+Engineering+College!5e0!3m2!1sen!2snp!4v1443154476866" style="color:#0000FF;text-align:left" target="_blank">View Larger Map</a>
                </small>
            </div>
            
        </div>
<!--        <div class="col-lg-6">-->
<!--            <div class="contact_info">-->
<!--                <h5>-->
<!--                    <strong style="text-align:justify; color:#5c8d1c;">-->
<!--                       Ayurveda Health Home(AHH)-->
<!--                    </strong>-->
<!--                </h5>-->
<!--                <p style="font-size:16px;">Pokhara</p> -->
<!---->
<!---->
<!--                <p class="author" style="text-align: justify;">-->
<!--                    <strong>-->
<!--                    Tel No: 01-4111111-->
<!--                    Fax: 01-4111111-->
<!--                    </strong>-->
<!--                </p>-->
<!--                <p>-->
<!--                    <strong>-->
<!--                    E-mail: info@ayurveda.com.np-->
<!--                    </strong>-->
<!--                </p>-->
<!---->
<!--            </div>-->
<!---->
<!--            <div class="contact_map">-->
<!--                <iframe frameborder="0" height="300" width="250" marginheight="0" marginwidth="0" scrolling="no" src="http://maps.google.com/maps/ms?msa=0&msid=215847421770814160536.0004ad070aaadde6d7d7c&ie=UTF8&ll=27.697788,85.349876&spn=0,0&t=m&vpsrc=6&output=embed"></iframe><br />-->
<!--                <small>-->
<!--                    <a href="http://maps.google.com/maps/ms?msa=0&msid=215847421770814160536.0004ad070aaadde6d7d7c&ie=UTF8&ll=27.697788,85.349876&spn=0,0&t=m&vpsrc=6&output=embed" style="color:#0000FF;text-align:left" target="_blank">View Larger Map</a>-->
<!--                </small>-->
<!--            </div>-->
<!--        </div>-->
        <div class="clearfix"></div>
    </div>
</div>