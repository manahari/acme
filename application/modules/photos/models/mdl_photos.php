<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_photos extends CI_Model {

	function __construct() {
	parent::__construct();
	}

	function get_table() {
	$table = "up_photos";
	return $table;
	} 
        function get_front($order_by){
	$table = $this->get_table();
	$this->db->order_by($order_by);
	$query=$this->db->get($table)->result();
	return $query;
	}
        function get_photos($slug){
	$table = $this->get_table();
        $this->db->select('up_photos.*');
	$this->db->select('up_photoscategory.name as category_name');
	$this->db->join('up_photoscategory', 'up_photoscategory.id = up_photos.category_id');
        $this->db->where('up_photoscategory.slug', $slug);
	$query=$this->db->get($table)->result();
	return $query;
	}
	function get($order_by){
	$table = $this->get_table();	
	$this->db->select('up_photos.*');
	$this->db->select('up_photoscategory.name as category_name');
	$this->db->join('up_photoscategory', 'up_photoscategory.id = up_photos.category_id');
	$this->db->order_by($order_by);
	$query=$this->db->get($table);
	return $query;
	}
	
        function get_by($key,$value)
        {
          $table = $this->get_table();
            $this->db->where($key, $value);
            $query=$this->db->get($table);
            return $query;  
        }
	function get_where($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$query=$this->db->get($table);
	return $query;
	}
	
	function _insert($data){
	$table = $this->get_table();
	$this->db->insert($table, $data);
        
	}
	
	function get_id(){
	$result = mysql_query("SHOW TABLE STATUS LIKE 'up_photos'");
	$row = mysql_fetch_array($result);
	$nextId = $row['Auto_increment']; 
	return $nextId;
	}
	
	function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}
        
        function _delete($id){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->delete($table);
	}
	
	
	function get_modules_dropdown()
	{
	$this->db->select('id, title');	
	$this->db->order_by('title');
	$dropdowns = $this->db->get('up_photos')->result();
	foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->title] = $dropdown->title;
		}
	if(empty($dropdownlist)){return NULL;}
	$finaldropdown = $dropdownlist;
	return $finaldropdown;
	}
	
	function get_groups_dropdown()
	{
		$this->db->select('id, name');
                $this->db->where('status', 'live');
		$this->db->order_by('id','DESC');
		$dropdowns = $this->db->get('up_photoscategory')->result();
		foreach ($dropdowns as $dropdown)
		{
		$dropdownlist[$dropdown->id] = $dropdown->name;
		}
		if(empty($dropdownlist)){return NULL;}
		$finaldropdown = $dropdownlist;
		return $finaldropdown;
	}

}