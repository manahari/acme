-- phpMyAdmin SQL Dump
-- version 3.1.3
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 28, 2014 at 09:33 AM
-- Server version: 5.1.32
-- PHP Version: 5.2.9-1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `upvedacms`
--

-- --------------------------------------------------------

--
-- Table structure for table `up_photos`
--

CREATE TABLE IF NOT EXISTS `up_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `category_id` int(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `slug` varchar(255) NOT NULL,
  `ent_date` date NOT NULL,
  `upd_date` date DEFAULT NULL,
  `status` enum('draft','live') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `up_photos`
--

INSERT INTO `up_photos` (`id`, `name`, `category_id`, `attachment`, `description`, `slug`, `ent_date`, `upd_date`, `status`) VALUES
(1, 'Harry', 1, '1.jpg', 'fjhgjhg\r\n', 'harry', '2014-07-08', '2014-07-08', 'draft'),
(2, 'Dhruba kc', 3, '2.png', 'ahfkhsahfhaskfj\r\n', 'dhruba-kc', '2014-07-08', NULL, 'draft'),
(3, 'Love', 1, '3.png', 'this is photo of love\r\n', 'love', '2014-07-28', '2014-07-28', 'live'),
(7, 'Abichar', 1, '7.jpg', 'fdsafsdafsd\r\n', 'abichar', '2014-07-28', NULL, 'live'),
(6, 'Heart', 1, '6.jpg', 'fdsafsdfs\r\n', 'heart', '2014-07-28', '2014-07-28', 'live');
