
<div class=" media col-lg-12">
	<h3 style="color:#5c8d1c;">Photo Gallery</h3>
			<?php
                 $i=0;       
                foreach($photos as $row )
                {
					$color=$i%3;
                    
                    ?>
                     <div class="thumbnail  col-lg-2">
                              
                       <a href="<?php echo base_url()?>photos/details/<?php echo $row->slug;?>"><img src="<?php echo base_url();?>uploads/photos/<?php echo $row->attachment; ?>" class="img-responsive" /></a>
                       <div class="photos_title gallery_title photos_category_bg_<?php echo $color; ?>"><?php echo ucwords(character_limiter($row->name,18)); ?></div>
                      </div>
                    <?php
					$i++;
                }
            ?>
           
</div>
