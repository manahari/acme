<!-- added link for slider gallery-->
<link rel="shortcut icon" href="<?php echo base_url();?>uploads/settings/<?php echo $site_settings['favicon'];?>">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/gallery/blueimp-gallery.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/gallery/bootstrap-image-gallery.min.css">
<link rel="stylesheet" href="<?php echo base_url();?>design/frontend/css/gallery/demo.css">

<script src="<?php echo base_url();?>design/frontend/js/gallery_modal/jquery.blueimp-gallery.min.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/gallery_modal/bootstrap-image-gallery.min.js"></script>
<script src="<?php echo base_url();?>design/frontend/js/gallery_modal/demo.js"></script>

<!-- added script for slider gallery & For Responsive Photo Gallery-->
<div class="col-lg-12">
	
			<?php
                $i=1;       
                foreach($picture as $row )
                {
					if($i==1)
					{
						echo '<h3 style="color:#5e7e95;">'.$row->category_name.'</h3>';
					}
					//print_r($row);
					?>
                    
                    <div class="col-lg-2 thumbnail" id="links"> 
                       <a href="<?php echo base_url();?>uploads/photos/<?php echo $row->attachment; ?>" title="<?php echo $row->name; ?>" data-gallery="blueimp-gallery" ><img src="<?php echo base_url();?>uploads/photos/<?php echo $row->attachment; ?>" class="img-responsive"/></a>
                    </div>
                    <?php
					$i++;
                }
            ?>
                   
</div>

<div id="blueimp-gallery" class="blueimp-gallery"> 
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h5 class="title"></h5>
    <a class="prev">‹</a> <a class="next">›</a> <a class="close">×</a> <a class="play-pause"></a>
    <ol class="indicator">
    </ol>
 <!--The modal dialog, which will be used to wrap the lightbox content -->
    <div class="modal fade">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" aria-hidden="true">&times;</button>
            <h4 class="modal-title"></h4>
          </div>
          <div class="modal-body next"></div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left prev">  Previous </button>
            <button type="button" class="btn btn-primary next"> Next  </button>
          </div>
        </div>
      </div>
    </div>
</div>