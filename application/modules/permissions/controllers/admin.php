<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('permissions'); //module name is permissions here	
	}
	
	function index()
	{	
		$data['query'] = $this->get_groups('id');
		
		$data['view_file'] = "admin/index";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);		
	}
	
	function get_groups($order_by){
	$this->load->model('groups/mdl_groups');
	$query = $this->mdl_groups->get($order_by);
	return $query;
	}
	
	function group(){		
		$group_id = $this->uri->segment(4);
		if(!empty($group_id)){
			$data['permissions'] = $this->unserialize_role_array($group_id);
			$data['ids'] = $this->get_module_ids();
			$data['query'] = $this->get_modules('id');
			$data['view_file'] = "admin/group";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
		}
		else{
			$this->session->set_flashdata('error', 'The group id provided was not valid.');				
			redirect('admin/permissions');
		}
	}
	
	function get_module_ids(){
	$this->load->model('modules/mdl_moduleslist');
	$query = $this->mdl_moduleslist->get_module_ids();
	return $query;		
	}
	
	function get_modules($order_by){
	$this->load->model('modules/mdl_moduleslist');
	$query = $this->mdl_moduleslist->get($order_by);
	return $query;
	}
	
	
	function get_data_from_post()
	{		
		$this->load->model('modules/mdl_moduleslist');
		$query = $this->mdl_moduleslist->get_module_ids();
		foreach ($query as $row){
			$data[$row] = $this->input->post($row, TRUE);
			if($data[$row] != 'on'){ unset($data[$row]);}
			else{$data[$row] = $row;}
		}
		$data['group_id'] = $this->input->post('group_id', TRUE);
		return $data;
	}
	
	function submit(){
	$data = $this->get_data_from_post();
	$group_id = $data['group_id'];
	unset($data['group_id']);
	//echo $group_id;
	$permission_status = $this->check_permission_existence($group_id);	//$permission_status only returns either TRUE or FALSE 
																		//by checking if permission hase been set before
		if($permission_status == TRUE){
			$this->_update($group_id,$data);	
		}else {			
			$this->_insert($group_id,$data);
		}
	redirect('admin/permissions');	
	}	
	
	
	function check_permission_existence($group_id){
	$this->load->model('mdl_permissions');
	$query = $this->mdl_permissions->check_permission_existence($group_id);
	return $query;		
	}

	function _update($group_id,$data){
	$this->load->model('mdl_permissions');
	$this->mdl_permissions->_update($group_id,$data);
	}

	function _insert($group_id,$data){
	$this->load->model('mdl_permissions');
	$this->mdl_permissions->_insert($group_id,$data);
	}
	
	function unserialize_role_array($group_id){		
	$this->load->model('mdl_permissions');
	$array = $this->mdl_permissions->unserialize_role_array($group_id);
	return $array;	
	}
	
}