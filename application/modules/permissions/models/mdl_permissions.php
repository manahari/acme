<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mdl_permissions extends CI_Model 
{
	
	function __construct() {
	parent::__construct();
	}	
	
	function get_table() {
	$table = "up_permissions";
	return $table;
	}
	
	function get_permissions(){
	$table = $this->get_table();
	$query=$this->db->get($table);
	return $query;
	}
	
	function get_where($name){
	$table = $this->get_table();
	$this->db->select($name);
	$this->db->where('id', 1);
	$query=$this->db->get($table);
	return $query;
	}
	
	/*function _update($id, $data){
	$table = $this->get_table();
	$this->db->where('id', $id);
	$this->db->update($table, $data);
	}*/	
	
	function check_permission_existence($group_id){
	$table = $this->get_table();
	$this->db->select('group_id');	
	$this->db->where('group_id', $group_id);	
	$result=$this->db->get($table)->result();	
	if(empty($result)){return FALSE;}
	else {return TRUE;}	
	}
	
	function _insert($group_id,$data){
	$array_string=serialize($data);
	$insertarray['group_id'] = $group_id;
	$insertarray['roles'] = $array_string;
	$table = $this->get_table();
	$this->db->insert($table, $insertarray);
	}
	
	function _update($group_id,$data){		
	$array_string=serialize($data);
	$updateroles['roles'] = $array_string;

	$table = $this->get_table();
	$this->db->where('group_id', $group_id);
	$this->db->update($table, $updateroles);	
	}
	
	function unserialize_role_array($group_id){	
	$table = $this->get_table();
	$this->db->where('group_id', $group_id);		
	$query_results=$this->db->get($table)->result();
	if(empty($query_results)){return NULL;}
	$result = $query_results[0]->roles;	
	$roles_array = unserialize($result);
	return $roles_array;
	}
	
}