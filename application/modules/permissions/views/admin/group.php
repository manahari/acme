<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i> Set Permissions</h4> 
            </div> 
            <div class="widget-content">
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
					echo form_open_multipart('admin/permissions/submit', 'class="form-horizontal row-border" id="validate-1"');			
				?>        
                
                	<table class="sort table table-striped table-bordered table-hover table-checkable table-responsive datatable"> 
                        <thead> 
                            <tr> 
                                <th data-class="expand" colspan="2">Group</th> 
                            </tr> 
                        </thead> 
                        <tbody> 
                           <?php foreach($query->result() as $row){?>
                        
                            <tr> 
                                <td class="checkbox-column">
                                	<input type='checkbox' name='<?php echo $row->id;?>' <?php if(isset($permissions[$row->id])){ echo "checked";}?> type="checkbox" class="uniform"/>
                                </td> 
                                <td><?php echo Ucfirst($row->title);?></td>
                            </tr> 
                            
                            <?php }	?>                
                        </tbody> 
                    </table> 
                
                    
                    <div class="form-actions"> 
						<?php 							
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
							$group_id = $this->uri->segment(4);
							if (!empty($group_id)){echo form_hidden('group_id',$group_id);}	
						?>
					</div>                 
                    
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>