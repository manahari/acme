<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i> Student</h4> 
            </div> 
            <div class="widget-content">
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
					echo form_open_multipart('admin/Student/submit', 'class="form-horizontal row-border" id="validate-1"');				
				?>                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Title <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('name', $name, 'class="form-control required"');?>
						</div> 
                    </div>
                    
                    <div class="form-actions"> 
						<?php 							
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
							if (!empty($update_id)){echo form_hidden('update_id',$update_id);}	
						?>
					</div>                 
                    
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>