<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		
		$this->load->module('admin_login/admin_login');
		$this->admin_login->check_session_and_permission('student'); //module name is student here	
	}
	
	function index()
	{	
            $modulename = $this->uri->segment(2);
            $data['module_id']=$this->get_id_from_modulename($modulename);
            $group_id = $this->session->userdata['group_id'];//to set the permession of user group
            $data['permissions'] = $this->unserialize_role_array($group_id);
            $data['query'] = $this->get('id');
            $data['view_file'] = "admin/table";
            $this->load->module('template/admin_template');
            $this->admin_template->admin($data);		
	}
	
	
	function get_data_from_post()
	{
		$data['name'] = $this->input->post('name', TRUE);			
		$data['slug'] = strtolower(url_title($data['name']));
		
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id))
			{
				$data['upd_date'] = date("Y-m-d");
			}
			else
			{
				$data['ent_date'] = date("Y-m-d");
				$data['upd_date'] = NULL;
			}
		return $data;
	}
		
	function get_data_from_db($update_id)
	{
		$query = $this->get_where($update_id);
		foreach($query->result() as $row)
		{
			$data['name'] = $row->name;
		}
	
		if(!isset($data))
		{
			$data = "";
		}
		return $data;
	}
	
	
	function create()
	{       
            $group_id = $this->session->userdata['group_id'];//to set the permession of user group
            $update_id = $this->uri->segment(4);
            
		$submit = $this->input->post('submit', TRUE);
		if($submit=="Submit")
                    {
			//person has submitted the form
			$data = $this->get_data_from_post();
                    }
			if (is_numeric($update_id))
                            {
                                if($group_id!=1)
                                {
                                    $permissions = $this->unserialize_role_array($group_id);
                                    $modulename = $this->uri->segment(2);
                                    $module_id=$this->get_id_from_modulename($modulename);
                                    $mystring = implode(" ",$permissions);
                                    if((strpos($mystring, 'e'.$module_id))==false)
                                    {
                                        redirect('admin/dashboard');  
                                    }
                                    else
                                    {
                                      $data = $this->get_data_from_db($update_id);  
                                    }
                                }
                                else
                                {
                                  $data = $this->get_data_from_db($update_id);  
                                }
                            }
                    
                    else
                    {
                        if($group_id!=1)
                        {
                            $permissions = $this->unserialize_role_array($group_id);
                            $modulename = $this->uri->segment(2);
                            $module_id=$this->get_id_from_modulename($modulename);
                            $mystring = implode(" ",$permissions);
                            if((strpos($mystring, 'a'.$module_id))==false)
                            {
                                redirect('admin/dashboard');  
                            }
                        }
                    //$data = $this->get_data_from_db($update_id);
                    }
		
            
		if(!isset($data))
		{
			$data = $this->get_data_from_post();
		}
		$data['update_id'] = $update_id;
		$data['view_file'] = "admin/form";
		$this->load->module('template/admin_template');
		$this->admin_template->admin($data);
	}

	
	function delete()
	{	$this->load->model('mdl_student');
		$delete_id = $this->uri->segment(4);
                $group_id = $this->session->userdata['group_id'];//to set the permession of user group
			
		if(!isset($delete_id) || !is_numeric($delete_id))
			{
				unset($delete_id);
				redirect('admin/student');
			}
		else
		{
                    if($group_id!=1)
                    {
                    $permissions = $this->unserialize_role_array($group_id);
                    $modulename = $this->uri->segment(2);
                    $module_id=$this->get_id_from_modulename($modulename);
                    $mystring = implode(" ",$permissions);
                    if((strpos($mystring, 'd'.$module_id))==false)
                    {
                        redirect('admin/dashboard');  
                    }
                    else
                    {
                      	$this->mdl_student->_delete($delete_id);
			redirect('admin/student');  
                    }
                    }
			$this->mdl_student->_delete($delete_id);
			redirect('admin/student');
		}				
	}
	function view()
        {
            $group_id = $this->session->userdata['group_id'];//to set the permession of user group
		$view_id = $this->uri->segment(4);
		if($group_id != 1){
				if (is_numeric($view_id))
                                    {
                                        $permissions = $this->unserialize_role_array($group_id);
                                        $modulename = $this->uri->segment(2);
                                        $module_id=$this->get_id_from_modulename($modulename);
                                        $mystring = implode(" ",$permissions);
                                        
                                        if((strpos($mystring, 'v'.$module_id))==false)
                                        {
                                            redirect('admin/dashboard');  
                                        }
                                        else
                                        {
                                         $data = $this->get_data_from_db($view_id);   
                                        }
                                      }
                                    }
                        $data = $this->get_data_from_db($view_id);
			$data['view_id'] = $view_id;
			$data['view_file'] = "admin/view";
			$this->load->module('template/admin_template');
			$this->admin_template->admin($data);
                }
	
	function submit()
	{		
	
	$this->load->library('form_validation');
		/*setting validation rule*/
		$update_id = $this->input->post('update_id', TRUE);
		if(is_numeric($update_id)){						
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //we don't want unique_validation error while editing
		}
		else{
		$this->form_validation->set_rules('name', 'Name', 'required|xss_clean'); //unique_validation check while creating new
		}
		/*end of validation rule*/
		
		
		if ($this->form_validation->run($this) == FALSE)
		{
			$this->create();
		}
		else
		{
			$data = $this->get_data_from_post();
			
			$update_id = $this->input->post('update_id', TRUE);
			if(is_numeric($update_id)){				
				$this->_update($update_id, $data);
			} else {
				$this->_insert($data);
			}
			
			redirect('admin/student');
		}
			
	}
		

	function get($order_by){
	$this->load->model('mdl_student');
	$query = $this->mdl_student->get($order_by);
	return $query;
	}
	
	function get_where($id){
	$this->load->model('mdl_student');
	$query = $this->mdl_student->get_where($id);
	return $query;
	}
	
	function _insert($data){
	$this->load->model('mdl_student');
	$this->mdl_student->_insert($data);
	}

	function _update($id, $data){
	$this->load->model('mdl_student');
	$this->mdl_student->_update($id, $data);
	}
	
	function _delete($id){
	$this->load->model('mdl_student');
	$this->mdl_student->_delete($id);
	}
        function unserialize_role_array($group_id){		
	$this->load->model('permissions/mdl_permissions');
	$array = $this->mdl_permissions->unserialize_role_array($group_id);
	return $array;	
	}
        function get_id_from_modulename($modulename){
	$this->load->model('modules/mdl_moduleslist');
	$query = $this->mdl_moduleslist->get_id_from_modulename($modulename);
	return $query;
        }
}