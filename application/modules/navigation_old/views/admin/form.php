<script>
$(window).load(function() {
	
    if (document.getElementById('moduleCheck').checked) {
        document.getElementById('ifModule').style.display = 'block';
		document.getElementById('ifPage').style.display = 'none';		
		document.getElementById('ifUri').style.display = 'none';
		document.getElementById('ifUrl').style.display = 'none';
    }
	else if (document.getElementById('pageCheck').checked) {
        document.getElementById('ifPage').style.display = 'block';
        document.getElementById('ifModule').style.display = 'none';		
		document.getElementById('ifUri').style.display = 'none';
		document.getElementById('ifUrl').style.display = 'none';
    }
    else if (document.getElementById('uriCheck').checked) {			
		document.getElementById('ifUri').style.display = 'block';
        document.getElementById('ifModule').style.display = 'none';
        document.getElementById('ifPage').style.display = 'none';	       
		document.getElementById('ifUrl').style.display = 'none';	
		}
    else if(document.getElementById('urlCheck').checked) {					
		document.getElementById('ifUrl').style.display = 'block';
        document.getElementById('ifModule').style.display = 'none';			
        document.getElementById('ifPage').style.display = 'none';			
		document.getElementById('ifUri').style.display = 'none';
		} 
    else {				
		document.getElementById('ifUrl').style.display = 'none';
        document.getElementById('ifModule').style.display = 'none';			
        document.getElementById('ifPage').style.display = 'none';			
		document.getElementById('ifUri').style.display = 'none';
		} 
		
});
</script>

<div class="row"> 
	<div class="col-md-12"> 
        <div class="widget box"> 
            <div class="widget-header"> 
            	<h4><i class="icon-reorder"></i> Add Navigation</h4> 
            </div> 
            <div class="widget-content">
            	<?php
					echo validation_errors('<p style="color: red;">', '</p>');
					echo form_open_multipart('admin/navigation/submit', 'class="form-horizontal row-border" id="validate-1"');				
				?>            
                                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Title <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('title', $title, 'class="form-control required"');?>
						</div> 
                    </div>
                        
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Parent </label> 
                    	<div class="col-md-10"> 
                        	<?php $selected = $parent_id;$options = $parent;
				echo form_dropdown('parent_id', $options, $selected, 'class="form-control"');?>
						</div> 
                    </div>  

					<script type="text/javascript">
                    
                    function navtypeCheck() {
                        if (document.getElementById('moduleCheck').checked) {
                            document.getElementById('ifModule').style.display = 'block';
                            document.getElementById('ifPage').style.display = 'none';		
                            document.getElementById('ifUri').style.display = 'none';
                            document.getElementById('ifUrl').style.display = 'none';
                        }
                        else if (document.getElementById('pageCheck').checked) {
                            document.getElementById('ifPage').style.display = 'block';
                            document.getElementById('ifModule').style.display = 'none';		
                            document.getElementById('ifUri').style.display = 'none';
                            document.getElementById('ifUrl').style.display = 'none';
                        }
                        else if (document.getElementById('uriCheck').checked) {			
                            document.getElementById('ifUri').style.display = 'block';
                            document.getElementById('ifModule').style.display = 'none';
                            document.getElementById('ifPage').style.display = 'none';	       
                            document.getElementById('ifUrl').style.display = 'none';	
                            }
                        else {				
                            document.getElementById('ifUrl').style.display = 'block';
                            document.getElementById('ifModule').style.display = 'none';			
                            document.getElementById('ifPage').style.display = 'none';			
                            document.getElementById('ifUri').style.display = 'none';
                            }
                    
                    }
                    
                    </script>    
                                
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Link Type <span class="required">*</span></label> 
                    	<div class="col-md-10"> 
                        	<?php 
								echo '<label class="radio">' . form_radio('navtype', 'Module', ((isset($post->navtype) && $post->navtype == 'Module' ? TRUE : FALSE) || (isset($navtype) && $navtype == 'Module' ? TRUE : FALSE)),'onclick = "javascript:navtypeCheck();" id="moduleCheck" style="margin-right:30px;" class="required"'). ' Module</label>' ;
								echo '<label class="radio">' . form_radio('navtype', 'Page', ((isset($post->navtype) && $post->navtype == 'Page' ? TRUE : FALSE) || (isset($navtype) && $navtype == 'Page' ? TRUE : FALSE)),'onclick = "javascript:navtypeCheck();" id="pageCheck" style="margin-right:30px;"'). ' Page</label>' ;
								echo '<label class="radio">' . form_radio('navtype', 'URI', ((isset($post->navtype) && $post->navtype == 'URI' ? TRUE : FALSE) || (isset($navtype) && $navtype == 'URI' ? TRUE : FALSE)),'onclick = "javascript:navtypeCheck();" id="uriCheck" style="margin-right:30px;"'). ' Site Link(URI)</label>'  ;
								echo '<label class="radio">' . form_radio('navtype', 'URL', ((isset($post->navtype) && $post->navtype == 'URL' ? TRUE : FALSE) || (isset($navtype) && $navtype == 'URL' ? TRUE : FALSE)),'onclick = "javascript:navtypeCheck();" id="urlCheck" style="margin-right:30px;"'). ' URL' ;
							?>
                            <label for="navtype" class="has-error help-block" generated="true" style="display:none;"></label>
						</div> 
                    </div>  
                                
                    <div id="ifModule" style="display:none;" class="form-group"> 
                    	<label class="col-md-2 control-label">Module</label> 
                    	<div class="col-md-10"> 
                        	<?php $selected = $module_id;$options = $modulelist;//moduleslist array has moduleslists from tbl_moduleslists
							echo form_dropdown('module_id', $options, $selected, 'class="form-control"');?>
						</div> 
                    </div>
                                
                    <div id="ifPage" style="display:none;" class="form-group"> 
                    	<label class="col-md-2 control-label">Page</label> 
                    	<div class="col-md-10"> 
                        	<?php $selected = $page_id;$options = $pagelist;//moduleslist array has moduleslists from tbl_moduleslists
							echo form_dropdown('page_id', $options, $selected, 'class="form-control"');?>
						</div> 
                    </div>
                                
                    <div id="ifUri" style="display:none;" class="form-group"> 
                    	<label class="col-md-2 control-label">Site Link(URI)</label> 
                    	<div class="col-md-10"> 
                        	<?php echo form_input('site_uri', $site_uri, 'class="form-control"' );?>
						</div> 
                    </div>
                                
                    <div id="ifUrl" style="display:none;" class="form-group"> 
                    	<label class="col-md-2 control-label">Link URL</label> 
                    	<div class="col-md-10">                         	      
							<?php empty($link_url) ? $link_url='http://' : $link_url ?>
                            <?php echo form_input('link_url', $link_url, 'class="form-control"');?>
						</div> 
                    </div> 
                         
                    <div class="form-group"> 
                    	<label class="col-md-2 control-label">Status</label> 
                    	<div class="col-md-10"> 
                            <?php $selected = $status;$options = array(
                              'draft'  => 'draft',
                              'live'    => 'live',
                            );                            
                            echo form_dropdown('status', $options, $selected,'class="form-control"');?>
						</div> 
                    </div>   
                    
                    <div class="form-actions"> 
						<?php 			
							$group_id = $this->uri->segment(4); echo form_hidden('group_id',$group_id);				
							echo form_submit('submit','Submit','class="btn btn-primary pull-right"'); //name,value...type is default submit 
							if (!empty($update_id)){echo form_hidden('update_id',$update_id);}	
						?>
					</div>                 
                    
                <?php echo form_close(); ?>                
            </div> 
        </div> 
    </div>
</div>