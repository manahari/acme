/*jQuery (1.4.2) gets confused if you have any form elements named "action". You can get around this by using the DOM attribute methods or simply avoid having form elements named "action".

<form action="foo">
  <button name="action" value="bar">Go</button>
</form>

<script type="text/javascript">
  $('form').attr('action', 'baz'); //this fails silently
  $('form').get(0).setAttribute('action', 'baz'); //this works
</script>
*/

function bulkaction(frmname,action)
{
    //var checkedlength = $("input[type=checkbox]").length; // it gives number of checkbox
    var checkedlength = $("input[type=checkbox]:checked").length;
    //var checkedlength = $(":checkbox:checked").length; // this also works
    //alert(checkedlength);
    if(checkedlength > 0)
    {
        if(confirm('Are you sure you want to perform the selected operation??'))
        {
            $("#"+frmname).attr('action', base_url + 'index.php/' + action).submit(); 
        }
    }
    else
    {
        alert("No record(s) selected.");
    }
}

$(function(){
    //alert('a');
    $('input').checkBox();
    $('#toggle-all').click(function(){
     $('#toggle-all').toggleClass('toggle-checked');
    $('#frmcheckbulk input[type=checkbox]').checkBox('toggle');
    return false;
    });
    
    
});

